onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.data
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.data
add wave -noupdate /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMDelay(0)
add wave -noupdate /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMDelay(1)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(0).outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(1).outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.packet_received
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.received_signal(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.primitiveID(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.finetime(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.received_signal(1)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.primitiveID(1)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.finetime(1)
add wave -noupdate -radix hexadecimal -childformat {{/ethlinksimulation/ethlink_inst/outputs.timestamp(0) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(1) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(2) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(3) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(4) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(5) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(6) -radix hexadecimal}} -subitemconfig {/ethlinksimulation/ethlink_inst/outputs.timestamp(0) {-height 16 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(1) {-height 16 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(2) {-height 16 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(3) {-height 16 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(4) {-height 16 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(5) {-height 16 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(6) {-height 16 -radix hexadecimal}} /ethlinksimulation/ethlink_inst/outputs.timestamp
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allnets.REFERENCEFIFO.inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allnets.REFERENCEFIFO.inputs.data
add wave -noupdate -radix decimal /ethlinksimulation/trigger_inst/allcmps.REFERENCEFIFO.outputs.rdusedw
add wave -noupdate -radix decimal /ethlinksimulation/trigger_inst/allnets.CONTROLFIFO.inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allnets.CONTROLFIFO.inputs.data
add wave -noupdate /ethlinksimulation/trigger_inst/allnets.FIFOMTPNUMREF.inputs.wrreq
add wave -noupdate /ethlinksimulation/trigger_inst/allnets.FIFOMTPNUMCONTROL.inputs.wrreq
add wave -noupdate -radix decimal /ethlinksimulation/trigger_inst/allnets.FIFOMTPNUMREF.inputs.data
add wave -noupdate -radix decimal /ethlinksimulation/trigger_inst/allnets.FIFOMTPNUMCONTROL.inputs.data
add wave -noupdate /ethlinksimulation/trigger_inst/allnets.FIFOMTPNUMREF.inputs.rdreq
add wave -noupdate /ethlinksimulation/trigger_inst/allnets.FIFOMTPNUMCONTROL.inputs.rdreq
add wave -noupdate /ethlinksimulation/trigger_inst/allregs.dout.clk125.nprimitivecontrol
add wave -noupdate /ethlinksimulation/trigger_inst/allregs.dout.clk125.nprimitiveref
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allcmps.CONTROLFIFO.outputs.q
add wave -noupdate /ethlinksimulation/trigger_inst/allregs.dout.clk125.nprimitivereffinish
add wave -noupdate /ethlinksimulation/trigger_inst/allregs.dout.clk125.nprimitivecontrolfinish
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/REFERENCEFIFO/inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allcmps.REFERENCEFIFO.outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/CONTROLFIFO/inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/CONTROLFIFO/outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/inputs.data
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/alignRAM(0)/alignRAM/outputs.q_b
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/alignRAM(1)/alignRAM/outputs.q_b
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/wena
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/control_detector
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_ref_in
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/timestamp_in
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_in0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_in1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_in2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_outputenable
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_wait0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_wait1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_wait2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/FSM
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_decoding
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/rdready
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/timestamp_out
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_out0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_out1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_out2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID_t_out0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID_t_out1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID_t_out2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/n_of_trigger
add wave -noupdate -radix binary /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/control_detector_out
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.trigger_signal
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.control_signal
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.triggerflag
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.tmpdatatype
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.address_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.data_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.wren_a
add wave -noupdate -radix decimal -childformat {{/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(31) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(30) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(29) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(28) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(27) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(26) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(25) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(24) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(23) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(22) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(21) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(20) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(19) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(18) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(17) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(16) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(15) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(14) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(13) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(12) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(11) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(10) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(9) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(8) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(7) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(6) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(5) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(4) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(3) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(2) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(1) -radix decimal} {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(0) -radix decimal}} -subitemconfig {/ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(31) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(30) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(29) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(28) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(27) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(26) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(25) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(24) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(23) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(22) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(21) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(20) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(19) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(18) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(17) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(16) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(15) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(14) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(13) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(12) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(11) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(10) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(9) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(8) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(7) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(6) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(5) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(4) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(3) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(2) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(1) {-height 16 -radix decimal} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)(0) {-height 16 -radix decimal}} /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(0)
add wave -noupdate /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(1)
add wave -noupdate /ethlinksimulation/trigger_inst/allregs.dout.clk125.downscaling(2)
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {0 ps} 0} {{Cursor 3} {18028890 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 550
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1000
configure wave -griddelta 80
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 ps} {60416 ps}
