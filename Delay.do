onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 22 {CHOD DETECTOR}
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.fifodelay_set(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMDelay(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.fifodelay(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.data
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(0).outputs.wrusedw
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.start_latency
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(0).outputs.q
add wave -noupdate -divider -height 22 {MUV3 DETECTOR}
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.fifodelay_set(1)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMDelay(1)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.fifodelay(1)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.data
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(1).outputs.wrusedw
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.start_latency
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(1).outputs.q
add wave -noupdate -divider -height 22 {PACKET FLOW CHECKS}
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.packet_received
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.received_signal(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.received_signal(1)
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7210000 ps} 0} {{Cursor 2} {12002000 ps} 0} {{Cursor 3} {10410000 ps} 0} {{Cursor 4} {14586000 ps} 0}
quietly wave cursor active 4
configure wave -namecolwidth 377
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 80
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {6784741 ps} {25954021 ps}
