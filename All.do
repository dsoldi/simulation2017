onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ethlinksimulation/MACready
add wave -noupdate -radix hexadecimal /ethlinksimulation/FSMDataCHOD
add wave -noupdate -radix hexadecimal /ethlinksimulation/FSMDataMUV
add wave -noupdate -radix hexadecimal /ethlinksimulation/MACoutput(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/MACoutput(1)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.data
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.data
add wave -noupdate /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(0).outputs.q
add wave -noupdate /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(1).inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(1).outputs.q
add wave -noupdate /ethlinksimulation/ethlink_inst/outputs.packet_received
add wave -noupdate /ethlinksimulation/trigger_inst/allregs.dout.clk125.FSMReadRam
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/REFERENCEFIFO/inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/CONTROLFIFO/inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/CONTROLFIFO/outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/inputs.data
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/MERGEDFIFO/outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/alignRAM(0)/alignRAM/outputs.q_b
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/alignRAM(1)/alignRAM/outputs.q_b
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/wena
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/control_detector
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_ref_in
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/timestamp_in
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_in0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_in1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_in2
add wave -noupdate /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_wait0
add wave -noupdate /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_wait1
add wave -noupdate /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_wait2
add wave -noupdate /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/s_outputenable
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/rdready
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/timestamp_out
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_out0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_out1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/finetime_out2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID_t_out0
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID_t_out1
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/primitiveID_t_out2
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/n_of_trigger
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/LUT/altTriggerLUT_inst/control_detector_out
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.trigger_signal
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.control_signal
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.triggerflag
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.tmpdatatype
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.address_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.data_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.wren_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/SENDRAM_inst/altSENDRAM_inst/data_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/SENDRAM_inst/altSENDRAM_inst/wren_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/SENDRAM_inst/altSENDRAM_inst/address_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMoutputdata
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.rden_b
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/outputs.q_b
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {11366000 ps} 1} {{Cursor 2} {13052706 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 557
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 80
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {10154496 ps} {16924160 ps}
