library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
use IEEE.NUMERIC_BIT.all;
use work.globals.all;
use work.userlib.all;

entity altTTC is
	port (clk40             : in std_logic; --40 MHZ FROM LTU
			clk125             : in std_logic; --40 MHZ FROM LTU
			reset           : in std_logic;
			STARTSOB        : in std_logic;
			BCRST           : in std_logic;
			RUN40           : out std_logic;
			RUN125          : out std_logic;

			ECRST           : in std_logic;
			led1            : out std_logic;
			led3            : out std_logic;
			number_of_burst : out std_logic_vector(31 downto 0);
			
			CHOKE0      : in std_logic;
      	CHOKE1      : in std_logic;
      	CHOKE2      : in std_logic;
      	CHOKE3      : in std_logic;
      	CHOKE4      : in std_logic;
      	CHOKE5      : in std_logic;
      	CHOKE6      : in std_logic;
      	CHOKE7      : in std_logic;
      	CHOKE8      : in std_logic;
      	CHOKE9      : in std_logic;
      	CHOKE10     : in std_logic;
      	CHOKE11     : in std_logic;
      	CHOKE12     : in std_logic;
      	CHOKE13     : in std_logic;
      	
      	ERROR0      : in std_logic;
      	ERROR1      : in std_logic;
      	ERROR2      : in std_logic;
      	ERROR3      : in std_logic;
      	ERROR4      : in std_logic;
      	ERROR5      : in std_logic;
      	ERROR6      : in std_logic;
      	ERROR7      : in std_logic;
      	ERROR8      : in std_logic;
      	ERROR9      : in std_logic;
      	ERROR10     : in std_logic;
      	ERROR11     : in std_logic;
      	ERROR12     : in std_logic;
      	ERROR13     : in std_logic;
			
			CHOKE_ON        : out std_logic;   
			CHOKE_OFF       : out std_logic;  
			ERROR_ON   		 : out std_logic;	 
			ERROR_OFF   	 : out std_logic;		 
			CHOKE_signal    : out std_logic_vector(13 downto 0);
			ERROR_signal    : out std_logic_vector(13 downto 0);
			activateCHOKE   : in std_logic;
			activateERROR   : in std_logic
			);

end altTTC;

architecture rtl of altTTC is

signal s_number_of_burst : std_logic_vector(31 downto 0);
signal s_n_of_choke      : std_logic_vector(31 downto 0);
signal s_n_of_error      : std_logic_vector(31 downto 0);
signal s_RUN40           : std_logic;
signal s_RUN125          : std_logic;
signal s_CHOKE_OFF       : std_logic;
signal s_CHOKE_ON        : std_logic;
signal s_ERROR_OFF       : std_logic;
signal s_ERROR_ON        : std_logic;
signal CHOKE             : std_logic_vector(0 to 13);
signal ERROR             : std_logic_vector(0 to 13);
signal CHOKE_s1          : std_logic_vector(0 to 13);
signal CHOKE_s2          : std_logic_vector(0 to 13);
signal ERROR_s1          : std_logic_vector(0 to 13);
signal ERROR_s2          : std_logic_vector(0 to 13);

begin

CHOKE(0)  <= CHOKE0;
CHOKE(1)  <= CHOKE1;
CHOKE(2)  <= CHOKE2;
CHOKE(3)  <= CHOKE3;
CHOKE(4)  <= CHOKE4;
CHOKE(5)  <= CHOKE5;
CHOKE(6)  <= CHOKE6;
CHOKE(7)  <= CHOKE7;
CHOKE(8)  <= CHOKE8;
CHOKE(9)  <= CHOKE9;
CHOKE(10) <= CHOKE10;
CHOKE(11) <= CHOKE11;
CHOKE(12) <= CHOKE12;
CHOKE(13) <= CHOKE13;


ERROR(0)  <= ERROR0;
ERROR(1)  <= ERROR1;
ERROR(2)  <= ERROR2;
ERROR(3)  <= ERROR3;
ERROR(4)  <= ERROR4;
ERROR(5)  <= ERROR5;
ERROR(6)  <= ERROR6;
ERROR(7)  <= ERROR7;
ERROR(8)  <= ERROR8;
ERROR(9)  <= ERROR9;
ERROR(10) <= ERROR10;
ERROR(11) <= ERROR11;
ERROR(12) <= ERROR12;
ERROR(13) <= ERROR13;


process(clk125, reset) is
begin
  if(rising_edge(clk125)) then
    if(reset = '1') then
      CHOKE_s1 <= (others=>'0');
      CHOKE_s2 <= (others=>'0');
    else
      CHOKE_s1 <= CHOKE;
      CHOKE_s2 <= CHOKE_s1;
    end if;
  end if;
end process;


process(clk125, reset) is
begin
  if(rising_edge(clk125)) then
    if(reset = '1') then
      ERROR_s1 <= (others=>'0');
      ERROR_s2 <= (others=>'0');
    else
      ERROR_s1 <= ERROR;
      ERROR_s2 <= ERROR_s1;
    end if;
  end if;
end process;



P1: PROCESS(reset,clk40,ECRST,BCRST,startSOB)
begin
if reset = '1' then
  s_RUN40<='0';
	elsif(clk40='1' and clk40'event)  then
		if startSOB ='1' then --FROM USB
			if(ECRST='1' and BCRST='1')then --SOB
				s_RUN40<='1';
			elsif (BCRST='0' and ECRST='1')  then
				s_RUN40 <='0';
			else
				s_RUN40 <=s_RUN40;
			end if;
		end if;
	end if;	
end PROCESS;

P2: PROCESS(reset,clk125,ECRST,BCRST,startSOB)
begin
if reset = '1' then
  s_RUN125<='0';
	s_number_of_burst<=(others=>'0');  
	elsif(clk125='1' and clk125'event)  then
		if startSOB ='1' then --FROM USB
			if(ECRST='1' and BCRST='1')then --SOB
				s_RUN125<='1';
				s_number_of_burst<=SLV(UINT(s_number_of_burst)+1,32);
			elsif (BCRST='0' and ECRST='1')  then
				s_RUN125 <='0';
				s_number_of_burst<=s_number_of_burst;
			else
				s_RUN125 <=s_RUN125;
				s_number_of_burst<=(others=>'0');
			end if;
		end if;
	end if;	
end PROCESS;



--P2: PROCESS(reset,clk125,ECRST,BCRST,startSOB)
--begin
--if reset = '1' then
--  s_RUN125<='0';
--	s_number_of_burst<=(others=>'0');  
--	elsif(clk125='1' and clk125'event)  then
--		if startSOB ='1' then --FROM USB
--			if(ECRST='1' and BCRST='1')then --SOB
--				s_number_of_burst<=SLV(UINT(s_number_of_burst)+1,32);
--				s_RUN125<='1';
--			elsif (BCRST='0' and ECRST='1')  then
--				s_RUN125 <='0';
--				s_number_of_burst<=s_number_of_burst;
--			else
--				s_RUN125 <=s_RUN125;
--			  s_number_of_burst<=s_number_of_burst;
--			end if;
--		end if;
--	end if;	
	
--end PROCESS;



CHOKE_P: PROCESS(reset,clk125,CHOKE,activateCHOKE)
begin	
if reset = '1' then
	s_CHOKE_OFF <='1';
	s_CHOKE_ON  <='0';
elsif(clk125='1' and clk125'event)  then
	if startSOB ='1' then --FROM USB
		if activateCHOKE ='1' then
			if CHOKE_s2 = SLV(0,14)  then
				s_CHOKE_OFF <='1';
				s_CHOKE_ON  <='0';
			else 
				s_CHOKE_OFF <='0';
				s_CHOKE_ON <= '1';
			end if;

		else
			s_CHOKE_OFF <='1';
			s_CHOKE_ON  <='0';
		end if;
   else
		s_CHOKE_OFF <='1';
		s_CHOKE_ON  <='0';
	end if;
end if;
end process;


ERROR_P: PROCESS(reset,clk125,ERROR,activateERROR) 
begin	
if reset = '1' then
	s_ERROR_OFF <='1';
	s_ERROR_ON  <='0';
elsif(clk125='1' and clk125'event)  then
	if startSOB ='1' then --FROM USB
		if activateERROR ='1' then
			if ERROR_s2 = SLV(0,14)  then
				s_ERROR_OFF <='1';
				s_ERROR_ON  <='0';
			else 
				s_ERROR_OFF <='0';
				s_ERROR_ON <= '1';
			end if;

		else
			s_ERROR_OFF <='1';
			s_ERROR_ON  <='0';
		end if;
   else
		s_ERROR_OFF <='1';
		s_ERROR_ON  <='0';
	end if;
end if;
end process;



ERROR_ON       <= s_ERROR_ON       ;
ERROR_OFF      <= s_ERROR_OFF      ;
CHOKE_ON       <= s_CHOKE_ON       ;
CHOKE_OFF      <= s_CHOKE_OFF      ;
RUN40  		   <= s_RUN40          ;
RUN125  		   <= s_RUN125         ;
Led1   		   <= s_RUN40          ;
Led3    			<= not(s_RUN40)     ; --Led of EOB
number_of_burst<= s_number_of_burst;
CHOKE_signal   <= CHOKE            ;
ERROR_signal   <= ERROR            ;



END RTL;