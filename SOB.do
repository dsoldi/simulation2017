onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ethlinksimulation/s_clk40
add wave -noupdate -radix hexadecimal /ethlinksimulation/s_clk125
add wave -noupdate /ethlinksimulation/PLL40_inst/locked
add wave -noupdate /ethlinksimulation/PLL125_inst/locked
add wave -noupdate -radix hexadecimal /ethlinksimulation/CTSTMP/internal_timestamp
add wave -noupdate -radix hexadecimal /ethlinksimulation/CTSTMP/internal_timestamp125
add wave -noupdate -radix hexadecimal /ethlinksimulation/s_clk125
add wave -noupdate /ethlinksimulation/ALTTTC_inst/BCRST
add wave -noupdate /ethlinksimulation/ALTTTC_inst/ECRST
add wave -noupdate /ethlinksimulation/ALTTTC_inst/BURST
configure wave -namecolwidth 465
configure wave -valuecolwidth 114
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
run 300 ns
