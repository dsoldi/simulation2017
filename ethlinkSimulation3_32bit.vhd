library ieee;
use ieee.std_logic_1164.all;
use work.component_ethlink.all;
use work.component_trigger.all;
use work.globals.all;
use work.mac_globals.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use std.textio.all;  --include package textio.vhd
use ieee.std_logic_textio.all;
use work.runcontrol.all;

--use ieee.numeric_std.all;

--Test bench used in Modelsim version 10.1. All the firmware has been simulated
--except for the ethernet part. This simulation takes primitives from files .bin
--and simulates the delivery of them via ethernet. One byte per clock hit.
--Parameters to select run control conditions are in RunControlParameters.vhd.

--Note: November 21 2018: Update to read directly the primitive files 

entity ethlinkSimulation is
end ethlinkSimulation;

architecture rtl of ethlinkSimulation is

   constant clkin_50       : time := 20000 ps; --Internal oscillator
   constant clkin_40       : time := 25000 ps; --External oscillator

----DEBUG SIGNALS----------------------------------------------
   signal tryc0            : std_logic;
   signal trylocked        : std_logic;
-------CLOCK SIGNALS-------------------------------------------
   signal clock50MHz    : std_logic:='0';
   signal clock_125MHz  : std_logic:='0';
   signal clock40MHz    : std_logic:='0';


-------MAC SIGNALS---------------------------------------------
   signal MACoutput:vector8bit_t(0 to 3) := (others =>"00000000");
   signal s_rdMAC    : std_logic_vector(0 to 2) := (others=>'0');
   signal MACready :std_logic_vector(0 TO 2) :=(others=>'0');
   signal s_reoframe :std_logic_vector(0 TO 2) :=(others=>'0');
-----------FILE SIGNALS----------------------------------------
   type infile is file of character;



--   file CHODfile0: infile open read_mode is "cases/testMergedRAM_IRC_mod.bin";
  -- file MUVfile0:  infile open read_mode is "cases/testMergedRAM_LKr_mod2.bin";

   file CHODfile0: infile open read_mode is "/home/na62torino/Data/rawPrimitiveFiles/run9315/chod_run09315_burst00100_mod.bin";
   file MUVfile0:  infile open read_mode is "/home/na62torino/Data/rawPrimitiveFiles/run9315/rich_run09315_burst00100_mod.bin";

--   file CHODfile0: infile open read_mode is "/home/na62torino/Data/simulation2017/cases/irc_run08764_burst00500_case3.bin";
--   file MUVfile0: infile open read_mode is "/home/na62torino/Data/simulation2017/cases/lkr_run08764_burst00500_case3.bin";

   
   file file_RESULTS : text;    
   
--TOP SIGNALS
   signal external_clock                    : std_logic;
--PLL 125 MHz signals:
   signal s_clk125                          : std_logic;
   signal s_locked                          : std_logic;
-------------------------------------
--PLL 40 MHz signals:
   signal s_clk40                           : std_logic;
   signal s_locked40                        : std_logic;
-------------------------------------
--reset
   signal s_rst125                         : std_LOGIC;
   signal s_rst50                          : std_LOGIC;
   signal s_rst40                          : std_LOGIC;
   signal s_resetn                         : std_logic;
-------------------------------------
--StartSoB, internal timestamp clock signals
   signal s_RUN40                          : std_logic;
   signal s_BURST                          : std_logic;
   signal s_StartSOB                       : std_logic;
   signal s_number_of_burst                : std_logic_vector (31 downto 0);		
   signal s_internal_timestamp             : std_logic_vector(29 downto 0);
   signal s_internal_timestamp125          : std_logic_vector(29 downto 0);
------------------------------------------------------------------------
--counters of the number of triggers after LUT, before any downscaling
   
   signal s_countertriggers_NIM            : vector32bit_t(0 to 6);

   signal s_ntriggers_predownscaling           : vector32bit_t(0 to nmask-1);
   signal s_ntriggers_predownscaling_control  : std_logic_vector(31 downto 0);
--after downscaling
   signal s_ntriggers_postdownscaling                : vector32bit_t(0 to nmask-1);
   signal s_ntriggers_postdownscaling_control        : std_logic_vector(31 downto 0);
   
------------------------------------------------------------------------
--counter data sent to LTU (outoutput to the detectors
   signal s_CounterLTU                     : std_logic_vector(31 downto 0);
------------------------------------------------------------------------
--Random trigger counter
   signal s_periodicrandomtriggercounter   : std_logic_vector(31 downto 0);
   signal s_randomtriggercounter           : std_logic_vector(31 downto 0);
------------------------------------------------------------------------
   signal s_MEPNum			        : std_logic_vector (31 downto 0);
-----------counters of primitives---------------------------------------
   signal s_number_of_primitives	        : vector32bit_t(0 to ethlink_NODES-2);
--Number of nim
   signal s_number_of_NIM                  : vector32bit_t(0 to NIM_NODES-1);
------------------------------------------------------------------------
--monitoring signals
   signal s_TRIGGERERROR		        :std_logic_vector (31 downto 0);                   
   signal s_ETHLINKERROR		        :std_logic_vector (31 downto 0);
------------------------------------------------------------------------
--CHOKE/ERROR signals
   signal s_enaCHOKE_and_ERROR   	        : std_logic;
   signal s_CHOKE_OFF                      : std_LOGIC;			
   signal s_CHOKE_ON                       : std_LOGIC;			
   signal s_ERROR_OFF                      : std_LOGIC;
   signal s_ERROR_ON                       : std_LOGIC;
   signal s_CHOKE_signal                   : std_logic_vector(13 downto 0); --which detector is in ch/er
   signal s_ERROR_signal                   : std_logic_vector(13 downto 0); 
   signal s_n_of_choke                     : std_logic_vector(31 downto 0);
   signal s_n_of_error                     : std_logic_vector(31 downto 0);
------------------------------------------------------------------------
-- timestamp received NIM via NIM interface
   signal s_timestamp_fromNIM              : vector32bit_t(0 to NIM_NODES-1);

-- timestamp out to trigger into ethlink module (in order to send the information @ right time)
   signal s_timestamp_out      	        : std_logic_vector(31 downto 0);
   signal s_timestamp_calib_out            : vector32bit_t(0 to ethlink_NODES - 2);     
   signal s_timestamp_random_out      	: std_logic_vector(31 downto 0);
   signal s_timestamp_out_NIM              : std_logic_vector(31 downto 0);

-- finetime out to trigger into ethlink module (in order to send the information @ right time)
   signal s_finetime_ref_out            : std_logic_vector(7 downto 0);
   signal s_finetime0_out      		: vector8bit_t(0 to ethlink_NODES - 2);
   signal s_finetime1_out      		: vector8bit_t(0 to ethlink_NODES - 2);
   signal s_finetime2_out      		: vector8bit_t(0 to ethlink_NODES - 2);
   signal s_finetime_calib_out          : vector8bit_t(0 to ethlink_NODES - 2);
---------------------------------------------------------------------------------
--ethlink says to trigger that it has received someting 
   signal s_received_signal                : std_logic_vector(0 to ethlink_NODES-2);
   signal s_received_signal_NIM            :  std_logic_vector(0 to NIM_NODES -1);
   signal s_random_signal                  : std_logic;
   signal s_periodic_signal0               : std_logic;
   signal s_periodic_signal1               : std_logic;
   signal s_calib_signal                   : std_logic_vector(6 downto 0);
   signal s_trigger_signal                 : std_logic;
   signal s_control_signal                 : std_logic;
   signal s_trigger_signal_NIM             : std_logic;
   signal s_synch_signal                   : std_logic;
   signal s_packet_received                : std_logic;
------------------------------------------------------------------------------
--primitive data output of ethlink into trigger
   signal s_primitiveID                    : vector16bit_t(0 to ethlink_NODES - 2);
   signal s_reserved    	   	   : vector8bit_t(0 to ethlink_NODES - 2);
   signal s_finetime    	 	   : vector8bit_t(0 to ethlink_NODES - 2);
   signal s_timestamp   	 	   : vector32bit_t(0 to ethlink_NODES - 2);
   signal s_MTPNUMREF                      : vector8bit_t(0 to ethlink_NODES - 2);
   signal s_MTPTimestamp                : vector24bit_t(0 to ethlink_NODES - 2);
------------------------------------------------------------------------
--Parameter set via USB
   signal s_primitive_In_mep               : std_logic_vector (31 downto 0);
   signal s_number_of_write                : vector32bit_t(0 to NIM_NODES-1);
   signal s_offset_NIM                     : vector32bit_t(0 to NIM_NODES-1);
---------------------------------------------------------------------------------
   signal s_Fixed_Latency                  : std_logic_vector(31 downto 0);
   signal s_Fixed_Latency_o                : std_logic_vector(31 downto 0);
------------------------------------------------------------------------------
--Activate Signals
   signal s_activateperiodictrigger        : std_logic;
   signal s_activaterandomtrigger          : std_logic;
   signal s_activate_NIM                   : std_logic;
   signal s_activate_primitives            : std_logic;
   signal s_activatecalibtrigger           : std_logic;
   signal s_activatesynchtrigger           : std_logic;
   signal s_activate_clock20MHz	        : std_logic;
------------------------------------------------------------------------------
-- signals of the mask in the LUT (inside trigger module) 
   signal s_enable_mask_NIM                : std_logic_vector(7 downto 0);
   signal s_enable_mask                    : std_logic_vector(nmask-1 downto 0);
   signal s_enable_control                 : std_logic;
   signal s_control_mask                   : std_logic_vector(15 downto 0);
   signal s_control_dontcare               : std_logic_vector(15 downto 0);
   signal s_mask                           : mem;
   signal s_dontcare                       : mem;
   signal s_mask_NIM                       : mem_NIM;
   signal s_dontcare_NIM                   : mem_NIM;
------------------------------------------------------------------------
--triggerword signals out from trigger into ethlink for sending info
   signal s_triggerword_NIM                : std_logic_vector(5 downto 0);
   signal s_triggerword        		: std_logic_vector(5 downto 0);
   signal s_triggerword_calib       	: std_logic_vector(5 downto 0);
   signal s_triggerflag                    : std_logic_vector(15 downto 0);
------------------------------------------------------------------------
--downscaling signal (one for each trigger type)
   signal s_downscaling                    : vector32bit_t(0 to nmask-1);
   signal s_downscaling_NIM                :  vector32bit_t(0 to 6);
------------------------------------------------------------------------
--Reference detector signal
   signal s_reference_detector             : std_logic_vector(31 downto 0);
   signal s_control_detector               : std_logic_vector(31 downto 0);
   signal s_reference_detector_NIM         : std_logic_vector(31 downto 0);
------------------------------------------------------------------------
--periodic triggerword set via usb
   signal s_periodic_triggerword0_in       : std_logic_vector(5 downto 0);
   signal s_periodic_triggerword1_in       : std_logic_vector(5 downto 0);
   signal s_periodic_triggerword0_out      : std_logic_vector(5 downto 0);
   signal s_periodic_triggerword1_out      : std_logic_vector(5 downto 0);

   signal s_periodictime                   : std_logic_vector(31 downto 0);
------------------------------------------------------------------------
--random triggerword set via usb
   signal s_random_triggerword_in          : std_logic_vector(5 downto 0);
   signal s_random_triggerword_out         : std_logic_vector(5 downto 0);
   signal s_randomtime                     : std_logic_vector(31 downto 0);
------------------------------------------------------------------------
-- position and primitive ID (t=trigger, c= calib) out of trigger, into ethlink for sending info
   signal s_primitive0_t                    : vector16bit_t(0 to ethlink_NODES-2);
   signal s_primitive1_t                    : vector16bit_t(0 to ethlink_NODES-2);
   signal s_primitive2_t                    : vector16bit_t(0 to ethlink_NODES-2);

------------------------------------
   signal s_primitive_c                    : vector16bit_t(0 to ethlink_NODES-2);

   signal s_LTU0 :std_logic;
   signal s_LTU1 :std_logic;
   signal s_LTU2 :std_logic;
   signal s_LTU3 :std_logic;
   signal s_LTU4 :std_logic;
   signal s_LTU5 :std_logic;
   signal s_LTU_TRIGGER :std_logic;
------------------------------------------------------------------------
-- calibration via NIM interface
   signal s_calibration_NIM                : std_logic;
   signal s_calib_latency                  : std_logic_vector(31 downto 0);
   signal s_calib_direction                : std_logic_vector(31 downto 0);

   signal s_delaydeliveryoutput :std_logic_vector(31 downto 0);

--FAKE DE4--
   signal s_fakereset      : std_logic:='0';
   signal counter_RUN      : unsigned(31 downto 0);
   signal counter_INTERRUN : unsigned(31 downto 0);

   type FSM_tstmp is (idle,sob,run,eob);
   type FSMData_t is (idle,Read0,Read1,Read2,Read3,Read4,Read5,Read6,Read7,ReadData,RemoveEmpty,EndPrimitive,Latency);
   signal FSMDataCHOD      :FSMData_t;
   signal FSMDataLAV       :FSMData_t;
   signal FSMDataMUV       :FSMData_t;
   signal FSMDataRICH      :FSMData_t;

   signal state            : FSM_tstmp;
   signal ECRST_fake       : std_logic:='0'; 				 
   signal BCRST_fake       : std_logic:='0';     
   signal s_led1           : std_logic:='1';
   signal s_led3           : std_logic:='1';
   signal s_FAKECHOKE      : std_logic_vector(13 downto 0):=(others=>'0');
   signal s_FAKEERROR      : std_logic_vector(13 downto 0):=(others=>'0');
-------------------------------------

   signal s_sendtimestamp : vector24bit_t(0 to 2):=(others=>X"000000");
   signal s_sourceID      : vector8bit_t(0 to  2):=(others=>X"00");
   signal s_subsourceID   : vector8bit_t(0 to  2):=(others=>X"00");
   signal s_PrimNumber    : vector8bit_t(0 to  2):=(others=>X"00");
   signal s_MTPlength     : vector16bit_t(0 to 2):=(others=>X"0000");
   signal s_WasEmpty      : std_logic_vector(0 to 1):="00";

--debug
   signal s_debug : std_logic_vector(703 downto 0);
   signal s_debug_ready : std_logic;





--Component declarations:

   component altpll_refclk2
      port(
	 areset		: IN STD_LOGIC  := '0';
	 inclk0		: IN STD_LOGIC;
	 c0		: OUT STD_LOGIC ;
	 locked		: OUT STD_LOGIC 
	 );
   end component;

----------
   component altpll40MHz
      port(
	 areset	   : IN STD_LOGIC  := '0';
	 inclk0	   : IN STD_LOGIC;
	 c0   	   : OUT STD_LOGIC ;
	 locked	   : OUT STD_LOGIC 
	 );
   end component;
----------

   
   component altcountertimestamp is 
      port(
	 clock40               : in STD_LOGIC;
	 clock125              : in STD_LOGIC;
	 BURST                 : in STD_LOGIC;
	 internal_timestamp    : out STD_LOGIC_VECTOR(29 downto 0);
	 internal_timestamp125 : out STD_LOGIC_VECTOR(29 downto 0)
	 );
   end component;

   
   component NIMInterface is
      port(
	 reset                      : in std_LOGIC;
	 CalibNimIn                 : in std_logic;  --40 MHz
	 clkB                       : in std_logic;  --125 MHz
	 CalibNimOut                : out std_logic;  --125 MHz
	 -------------------------
	 activateCalib              : in std_logic;
	 --activate_NIM               : in std_logic;
	 --DetectorfromNIM            : in std_logic_vector(0 to NIM_NODES-1);
	 --received_signal_NIM        : out std_logic_vector(0 to NIM_NODES-1);
	 BURST                      : in STD_LOGIC
	 --delay                      : in std_logic_vector(31 downto 0)
	 );
   end component;

   
   component altTTC
      port (
	 clk40           : in std_logic;
	 reset           : in std_logic;
	 
	 startRUN        : in std_logic;
	 activateCHOKE   : in std_logic; 	
	 activateERROR   : in std_logic; 	
	 
	 BCRST           : in std_logic;
	 ECRST           : in std_logic;	
	 CHOKE           : in std_logic_vector(13 downto 0);
	 ERROR           : in std_logic_vector(13 downto 0);
	 CHOKEMASK       : in std_logic_vector(13 downto 0); 
	 ERRORMASK       : in std_logic_vector(13 downto 0);
         FAKECHOKE       : in std_logic_vector(13 downto 0);
         FAKEERROR       : in std_logic_vector(13 downto 0);
	 BURST           : out std_logic;
	 led1            : out std_logic;
	 led3            : out std_logic;

	 CHOKE_signal    : out std_logic_vector(13 downto 0);
	 ERROR_signal    : out std_logic_vector(13 downto 0);
	 CHOKE_ON        : out std_logic;
	 ERROR_ON        : out std_logic;
	 CHOKE_OFF       : out std_logic;
	 ERROR_OFF       : out std_logic        
	 );end component;

   

   component pulser 
      port (
	 BURST125                  : in std_logic;
	 clk125                    : in std_logic; --40 MHZ FROM LTU
	 activateperiodictrigger0  : in std_logic;
	 periodicstarttime0        : in std_logic_vector(31 downto 0);
	 periodicendtime0          : in std_logic_vector(31 downto 0);
	 periodictime0             : in std_logic_vector(31 downto 0);
	 periodic_triggerword0_in  : in std_logic_vector(5 downto 0);
	 periodic_triggerword1_in  : in std_logic_vector(5 downto 0);
	 
	 activateperiodictrigger1  : in std_logic;
	 periodicstarttime1        : in std_logic_vector(31 downto 0);
	 periodicendtime1          : in std_logic_vector(31 downto 0);
	 periodictime1             : in std_logic_vector(31 downto 0);
	 
	 internal_timestamp        : in std_logic_vector(31 downto 0);
	 periodic_signal0          : out std_logic;
	 periodic_signal1          : out std_logic;
	 periodic_triggerword0_out  : out std_logic_vector(5 downto 0);
	 periodic_triggerword1_out  : out std_logic_vector(5 downto 0)
	 );

   end component;


   component generator 
      port (
	 clk                      : in std_logic; --40 MHZ FROM LTU
	 activaterandomtrigger    : in std_logic;
	 randomstarttime          : in std_logic_vector(31 downto 0);
	 randomendtime            : in std_logic_vector(31 downto 0);
	 randomtime               : in std_logic_vector(31 downto 0);
	 random_triggerword_in    : in std_logic_vector(5 downto 0);
	 internal_timestamp       : in std_logic_vector(31 downto 0);
	 random_signal            : out std_logic;
	 random_triggerword_out   : out std_logic_vector(5 downto 0)
	 
	 );

   end component;






begin
--
-- component port map
--
  file_open(file_RESULTS, "output_results.txt", write_mode);

  
   pulser_inst : pulser port map(

      clk125                            => s_clk125,
      BURST125                          => s_BURST,
      activateperiodictrigger0          => p_activateperiodictrigger0,
      periodicstarttime0                => p_periodic_start_time0,
      periodicendtime0                  => p_periodic_end_time0,
      periodictime0                     => p_periodictime0,
      periodic_triggerword0_in          => p_periodic_triggerword0,
      
      activateperiodictrigger1          => p_activateperiodictrigger1,
      periodicstarttime1                => p_periodic_start_time1,
      periodicendtime1                  => p_periodic_end_time1,
      periodictime1                     => p_periodictime1,
      periodic_triggerword1_in          => p_periodic_triggerword1,
      
      internal_timestamp(31 downto 30)  => (others=>'0'),
      internal_timestamp(29 downto 0)   => s_internal_timestamp,
      periodic_signal0                  => s_periodic_signal0,
      periodic_signal1                  => s_periodic_signal1,
      periodic_triggerword0_out         => s_periodic_triggerword0_out,
      periodic_triggerword1_out         => s_periodic_triggerword1_out

      );

   random_inst : generator port map(

      clk                              => s_clk125,
      activaterandomtrigger            => p_activaterandomtrigger,
      randomstarttime                  => p_random_start_time,
      randomendtime                    => p_random_end_time,
      randomtime                       => p_randomtime,
      random_triggerword_in            => p_random_triggerword,
      internal_timestamp(31 downto 30) => (others=>'0'),
      internal_timestamp(29 downto 0)  => s_internal_timestamp,
      random_signal                    => s_random_signal,
      random_triggerword_out           => s_random_triggerword_out

      );


   CTSTMP: altcountertimestamp port map 
      (
	 clock40                   => s_clk40,
	 clock125                  => s_clk125,
	 BURST                     => s_BURST,
	 internal_timestamp        => s_internal_timestamp,
	 internal_timestamp125     => s_internal_timestamp125  
	 );
   

   PLL125_inst : altpll_refclk2 port map(
      inclk0   => clock50MHz, --from DE4
      areset   => '0',
      Locked   => s_locked,
      c0       => s_clk125
      );

   PLL40_inst : altpll40MHz port map(
      inclk0   => clock40MHz,
      areset   => '0',
      Locked   => s_locked40,
      c0       => s_clk40
      );


   ethlink_inst : ethlink port map
      (

-------------------------------------------------------------------------
	 inputs.clkin_50                            => clock50MHz,        --
	 inputs.clkin_40                            => s_clk40,          --
	 inputs.clkin_125                           => s_clk125,         --
	 inputs.cpu_resetn                          => s_fakereset,      --
	 inputs.USER_DIPSW                          => "00010000",--
	 inputs.BURST                               => s_BURST,
	 inputs.primitive_In_Mep                    => p_Primitive_in_Mep , 
	 inputs.activate_clock20MHz                 => p_activateclock20MHz,
	 inputs.activate_synch                      => p_activatesynchtrigger,
	 inputs.calib_latency                       => p_calib_latency,
	 inputs.calib_direction                     => p_calib_direction(0),
	 inputs.internal_timestamp(29 downto 0)     => s_internal_timestamp(29 downto 0),
	 inputs.internal_timestamp125(29 downto 0)  => s_internal_timestamp125(29 downto 0),
	 inputs.internal_timestamp(31 downto 30)    => "00",
	 inputs.internal_timestamp125(31 downto 30) => "00",
	 inputs.timestamp_physics                   => s_timestamp_out       ,
	 inputs.timestamp_calib                     => s_timestamp_calib_out ,
	 inputs.finetime_physics_ref                => s_finetime_ref_out    ,
	 inputs.finetime_physics0                   => s_finetime0_out        ,
	 inputs.finetime_physics1                   => s_finetime1_out        ,
	 inputs.finetime_physics2                   => s_finetime2_out        ,
	 
	 inputs.finetime_calib                      => s_finetime_calib_out  ,
	 inputs.primitiveID_t0                      => s_primitive0_t,     
	 inputs.primitiveID_t1                      => s_primitive1_t,     
	 inputs.primitiveID_t2                      => s_primitive2_t,     

	 inputs.primitiveID_c                       => s_primitive_c    ,     
	 inputs.periodic_triggerword0               => s_periodic_triggerword0_out,
	 inputs.periodic_triggerword1               => s_periodic_triggerword1_out,
	 inputs.random_triggerword                  => s_random_triggerword_out,
	 inputs.triggerword                         => s_triggerword,
	 inputs.triggerword_calib                   => p_calib_triggerword(5 downto 0) ,
	 inputs.calibration_NIM                     => s_calibration_NIM     ,
	 inputs.trigger_signal                      => s_trigger_signal,---
	 inputs.random_signal                       => s_random_signal,	  
	 inputs.periodic_signal0                    => s_periodic_signal0,	
	 inputs.periodic_signal1                    => s_periodic_signal1,	
	 
	 inputs.calib_signal                        => s_calib_signal        ,
	 inputs.synch_signal                        => '0',
	 inputs.CHOKE_signal                        => s_CHOKE_signal,--
	 inputs.ERROR_signal                        => s_ERROR_signal,--
	 inputs.control_signal                      => s_control_signal,		
	 inputs.CHOKE_OFF                           => s_CHOKE_OFF,
	 inputs.CHOKE_ON                            => s_CHOKE_ON,
	 inputs.ERROR_ON                            => S_ERROR_ON,
	 inputs.ERROR_OFF                           => s_ERROR_OFF,
	 inputs.Fixed_Latency                       => p_Fixed_Latency,	
	 inputs.activate_primitives                 => p_activateprimitivetrigger,
	 
	 inputs.enable_mask                         => p_enable_mask        ,
	 inputs.downscaling                         => p_downscaling_set    ,
         
         inputs.ntriggers_predownscaling          => s_ntriggers_predownscaling,
--inputs.countertriggers                     => s_countertriggers    ,
         inputs.ntriggers_postdownscaling         => s_ntriggers_postdownscaling,

         inputs.ntriggers_postdownscaling_control => s_ntriggers_postdownscaling_control,
         inputs.ntriggers_predownscaling_control  => s_ntriggers_predownscaling_control,
         --inputs.number_of_trigger                   => s_number_of_trigger  ,
	-- inputs.control_trigger_counter             => s_control_trigger_counter ,

         inputs.mask                                => p_mask               ,	
	 inputs.dontcare                            => p_dontcare           ,
	 inputs.triggerflag                         => s_triggerflag         ,
	 inputs.reference_detector                  => p_reference_detector ,
	 inputs.MACoutput(0)         	            => MACoutput(0)               ,
	 inputs.MACoutput(1)                        => MACoutput(1)               ,
	 inputs.MACoutput(2)                        => MACoutput(2)               ,
	 inputs.MACoutput(3)                        => (others=>'0')              ,
	 inputs.MACoutput(4)                        => (others=>'0')              ,
	 inputs.MACoutput(5)                        => (others=>'0')              ,
	 inputs.MACoutput(6)                        => (others=>'0')              ,
	 inputs.MACReady(0)                         => MACReady(0)                ,
	 inputs.MACReady(1)                         => MACReady(1)                ,
	 inputs.MACReady(2)                         => MACReady(2)                ,
	 inputs.reoframe(0)                         => s_reoframe(0)              ,
	 inputs.reoframe(1)                         => s_reoframe(1)              ,
	 inputs.reoframe(2)                         => s_reoframe(2)              ,
	 inputs.delay_set                           => p_offset                   ,
	 inputs.maximum_delay_detector              => p_maximum_delay_detector,

	 outputs.resetn                             => s_resetn,
	 outputs.LTU0 		               => s_LTU0,
	 outputs.LTU1 		               => s_LTU1,
	 outputs.LTU2 		               => s_LTU2,
	 outputs.LTU3 		               => s_LTU3,
	 outputs.LTU4 		               => s_LTU4,
	 outputs.LTU5                  	       => s_LTU5,
	 outputs.LTU_TRIGGER                        => s_LTU_TRIGGER,
	 outputs.MEPNum                             => s_MEPNum                   ,
	 outputs.number_of_primitives               => s_number_of_primitives,
	 outputs.ETHLINKERROR   		    => s_ETHLINKERROR,
	 outputs.number_of_CHOKE                    => s_n_of_choke,
	 outputs.number_of_ERROR                    => s_n_of_error,
	 outputs.received_signal                    => s_received_signal   ,	 
	 outputs.primitiveID 	               => s_primitiveID 	   , 
	 outputs.MTPTimestamp                  => s_MTPTimestamp           ,
	 outputs.reserved    	               => s_reserved    	   ,
	 outputs.finetime    	               => s_finetime    	   ,  
	 
	 outputs.timestamp                          => s_timestamp         ,
	 outputs.Fixed_Latency_o                    => s_Fixed_Latency_o,
	 outputs.CounterLTU                         => s_CounterLTU,  
	 outputs.periodicrandomtriggercounter       => s_periodicrandomtriggercounter,
	 outputs.randomtriggercounter               => s_randomtriggercounter,
	 outputs.rst125                             => s_rst125            ,
	 outputs.rst40                              => s_rst40             ,
	 outputs.rst50                              => s_rst50             ,
	 outputs.packet_received                    => s_packet_received,
	 outputs.MTPNUMREF                          => s_MTPNUMREF ,                  
	 outputs.rdMAC                              => s_rdMAC,
         outputs.debug                              => s_debug,
         outputs.debug_ready                        => s_debug_ready
         
	 );

   
   trigger_inst : trigger port map
      (
	 
	 inputs.clkin_125                             => s_clk125                     ,
	 inputs.clkin_40                              => s_clk40                      ,

	 inputs.rst125                                => s_rst125		       ,
	 inputs.rst40                                 => s_rst40		       ,
         
         inputs.RUN                                   => '1',  
	 inputs.BURST                                 => s_BURST                      ,

         --FROM ETHLINK
         inputs.received_signal                       => s_received_signal            ,
	 inputs.primitiveID 	                       => s_primitiveID 	       ,
	 inputs.reserved    	                       => s_reserved    	       ,
	 inputs.finetime    	                       => s_finetime    	       ,
	 inputs.timestamp   	                       => s_timestamp   	       ,
	 inputs.packet_received                       => s_packet_received            ,
	 inputs.MTPNUMREF                             => s_MTPNUMREF                 ,

         inputs.internal_timestamp125(29 downto 0)    => s_internal_timestamp125      ,
	 inputs.internal_timestamp125(31 downto 30)   => "00"                         ,
         inputs.internal_timestamp(29 downto 0)    => s_internal_timestamp      ,
	 inputs.internal_timestamp(31 downto 30)   => "00"                         ,
	 


	 --FROM USB
	 inputs.mask                                  => p_mask,    
	 inputs.control_mask                          => p_control_mask               ,
	 inputs.control_dontcare                      => p_control_dontcare           ,
	 inputs.activatesynchtrigger                  => p_activatesynchtrigger       ,
         inputs.activatecalibtrigger                   => '0',

	 inputs.downscaling_set                       => p_downscaling_set            ,
         inputs.downscaling_reset                     => (others=>'0'),        
	 inputs.dontcare                              => p_dontcare                   ,
	 inputs.activatecontroltrigger                => p_activatecontroltrigger     ,
	 inputs.enable_mask                           => p_enable_mask                ,
	 inputs.calib_triggerword                     => p_calib_triggerword          , --Calib triggerword
	 inputs.bit_finetime                          => p_bit_finetime               , -- #bit finetime
	 inputs.deltapacket                           => p_deltapacket                , 
	 inputs.reference_detector                    => p_reference_detector         ,
	 inputs.control_detector                      => p_control_detector           ,
	 inputs.control_downscaling_set               => p_control_detector_downcaling,
	 inputs.timecut                               => p_timecut                    ,
	 inputs.delaydeliveryprimitive                => p_delaydeliveryprimitive     ,
	
	 -- OUTPUTS
	 outputs.timestamp_out                        => s_timestamp_out              ,
	 outputs.finetime_ref_out                     => s_finetime_ref_out           ,
	 outputs.finetime0_out                        => s_finetime0_out              ,
	 outputs.finetime1_out                        => s_finetime1_out              ,
	 outputs.finetime2_out                        => s_finetime2_out              ,

	 outputs.triggerword                          => s_triggerword                ,
	 outputs.trigger_signal                       => s_trigger_signal             ,
	 outputs.synch_signal                         => s_synch_signal               ,
	 outputs.calib_signal                         => s_calib_signal               ,
	 outputs.triggerword_calib                    => s_triggerword_calib          ,
	 outputs.finetime_calib_out                   => s_finetime_calib_out         ,
	 outputs.timestamp_calib_out                  => s_timestamp_calib_out        ,       
	 outputs.primitiveID0_t                       => s_primitive0_t                ,             
	 outputs.primitiveID1_t                       => s_primitive1_t                ,             
	 outputs.primitiveID2_t                       => s_primitive2_t                ,             


         outputs.ntriggers_predownscaling          => s_ntriggers_predownscaling,
         outputs.ntriggers_predownscaling_control  => s_ntriggers_predownscaling_control,
         outputs.ntriggers_postdownscaling         => s_ntriggers_postdownscaling(0 to nmask-1),
         outputs.ntriggers_postdownscaling_control => s_ntriggers_postdownscaling_control,

         
	 outputs.primitiveID_c                        => s_primitive_c                ,
	
         
	 outputs.TRIGGERERROR                         => s_TRIGGERERROR,
	 outputs.control_signal                       => s_control_signal             ,
	 outputs.triggerflag                          => s_triggerflag               ,
	 outputs.delaydeliveryoutput                  => s_delaydeliveryoutput
	 ); --END TRIGGER

   CDC_inst2 : NIMInterface port map (
      reset          	        => '0'                  ,
      CalibNimIn           	=> '0'                  ,
      clkB                        => s_clk125             ,
      CalibNimOut       	        => s_calibration_NIM    ,
--      delay                       => (others =>'0')       , --DELAY OF 0,1,2, NIM, not 3 4
      activateCalib               => p_activateCalib_NIM  ,
    --  activate_NIM                => p_activateNIMtrigger ,     
    --  DetectorfromNIM(0)          => '0'                  ,
    --  DetectorfromNIM(1)          => '0'                  ,
    --  DetectorfromNIM(2)          => '0'                  ,
    --  DetectorfromNIM(3)          => '0'                  ,
    --  DetectorfromNIM(4)          => '0'                  ,
  --    received_signal_NIM         => s_received_signal_NIM,
      BURST                       => s_BURST          
      ); --END NIM INTERVACE

   ALTTTC_inst : altTTC port map(
      clk40     		   => s_clk40, --to sample SOB/EOB TTCrx
      reset 		   => '0',
      ECRST                => ECRST_FAKE,
      BCRST 	       	   => BCRST_FAKE,
      startRUN 	      	   => '1',
      BURST   	   	   => s_BURST,
     
      Led1 		   => s_Led1,
      Led3  		   => s_Led3,
      FAKECHOKE            => s_FAKECHOKE,
      
      CHOKE(0)               => '1' ,
      CHOKE(1)               => '0' ,
      CHOKE(2)               => '1' ,
      CHOKE(3)               => '0' ,
      CHOKE(4)               => '0' ,
      CHOKE(5)               => '0' ,
      CHOKE(6)               => '0' ,
      CHOKE(7)               => '0' ,
      CHOKE(8)               => '0' ,
      CHOKE(9)               => '0' ,
      CHOKE(10)              => '0' ,
      CHOKE(11)              => '0' ,
      CHOKE(12)              => '0' ,
      CHOKE(13)              => '0' ,

      FAKEERROR           => s_FAKEERROR,
      ERROR(0)               => '0' ,
      ERROR(1)               => '0' ,
      ERROR(2)               => '0',
      ERROR(3)               => '0' ,
      ERROR(4)               => '0' ,
      ERROR(5)               => '0' ,
      ERROR(6)               => '0' ,
      ERROR(7)               => '0' ,
      ERROR(8)               => '0' ,
      ERROR(9)               => '0',
      ERROR(10)              => '0' ,
      ERROR(11)              => '0' ,
      ERROR(12)              => '0' ,
      ERROR(13)              => '0' ,
      
      activateCHOKE         => p_activateCHOKE,
      activateERROR         => p_activateERROR,
      CHOKE_signal          => s_CHOKE_signal ,
      ERROR_signal          => s_ERROR_signal ,
      CHOKE_ON              => s_CHOKE_ON     , 
      CHOKE_OFF             => s_CHOKE_OFF    ,
      ERROR_ON              => s_ERROR_ON     , 
      ERROR_OFF             => s_ERROR_OFF    ,
      CHOKEMASK             => p_CHOKEMASK    ,
      ERRORMASK             => p_ERRORMASK			 
      );

   
   
----CLOCK GENERATION-----------------------------------------------
   clock:process  
   begin
      clock50MHz <='0';
      wait for clkin_50/2;
      
      clock50MHz <='1';
      wait for clkin_50/2;   
   end process;
   
   clock40:process  
   begin
      clock40MHz <='0';
      wait for clkin_40/2;
      
      clock40MHz <='1';
      wait for clkin_40/2;   
   end process;
   
   
   
   -------------CHOD DATA GENERATION:-----------------------------------
   datiCHOD: process(s_clk125)
      variable delayCHOD : integer :=0;
      variable interpacketCHOD : integer :=800;
      variable indata0: character;
      variable ipacketCHOD : integer :=0;
      variable empty : integer :=0;
   begin
      if(s_clk125 ='1') then
	 case FSMDataCHOD is				
	    when idle=>	
	       s_reoframe(0)<='0';
	       if delayCHOD > 0 then
		  MACready(0) <='0';
		  delayCHOD := delayCHOD -1;
		  FSMDataCHOD<=Idle;
                  s_sendtimestamp(0) <= x"000000";
	       else
		  MACready(0) <='1';
		  FSMDataCHOD <=Read0;  
	       end if;


---------HEADER: 64 bit: 8 clocks to extact
         -- Source ID 8 bit
         -- Send Timestamp 24 bit
         -- + 
         -- Sub Source ID 8 bit
         -- Number of Primitives 8 bit
         -- Length 16 bit
         
               
           when Read0=> 
	       interpacketCHOD := interpacketCHOD -1;
	       if s_rdMAC(0)='1' then
		  if not endfile(CHODfile0) then  
		     read(CHODfile0,indata0);
		     MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                     s_sendtimestamp(0)(7 downto 0)<= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);       
		     FSMDataCHOD<=Read1;
		  else
--		     file_close(CHODfile0);
		     s_reoframe(0)<='1';
		     FSMDataCHOD<=Latency;
		  end if;
		  
	       else
		  MACready(0) <='0';
                  s_sendtimestamp(0) <= x"000000";
		  FSMDataCHOD<=EndPrimitive;
	       end if;  

	       
	       
	       
	    when Read1=> 
	       interpacketCHOD := interpacketCHOD -1;
	       if s_rdMAC(0)='1' then
		  if not endfile(CHODfile0) then
		     read(CHODfile0,indata0);
		     MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                     s_sendtimestamp(0)(15 downto 8)<= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
		     FSMDataCHOD<=Read2;
		  else
		     file_close(CHODfile0);
		     s_reoframe(0)<='1';
		     FSMDataCHOD<=Latency;
		  end if;
	       else
		  MACready(0) <='0';
		  FSMDataCHOD<=EndPrimitive;
	       end if;
	       

           when Read2=>
              interpacketCHOD := interpacketCHOD -1;
              if s_rdMAC(0)='1' then
                if not endfile(CHODfile0) then
                  read(CHODfile0,indata0);
                  MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_sendtimestamp(0)(23 downto 16)<= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  FSMDataCHOD<=Read3;
                else
                  file_close(CHODfile0);
                  s_reoframe(0)<='1';
                  FSMDataCHOD<=Latency;
                end if;
              else
                MACready(0) <='0';
                FSMDataCHOD<=EndPrimitive;
              end  if;

              
           when Read3=>
              interpacketCHOD := interpacketCHOD -1;
              if s_rdMAC(0)='1' then
                if not endfile(CHODfile0) then
                  read(CHODfile0,indata0);
                  MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_sourceID(0) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataCHOD<=Read4;
                else
                  file_close(CHODfile0);
                  s_reoframe(0)<='1';
                  FSMDataCHOD<=Latency;
                end if;
              else
                MACready(0) <='0';
                FSMDataCHOD<=Read3;
              end  if;

              when Read4=>
              interpacketCHOD := interpacketCHOD -1;
              if s_rdMAC(0)='1' then
                if not endfile(CHODfile0) then
                  read(CHODfile0,indata0);
                  MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_MTPlength(0)(7 downto 0) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataCHOD<=Read5;
                else
                  file_close(CHODfile0);
                  s_reoframe(0)<='1';
                  FSMDataCHOD<=Latency;
                end if;
              else
                MACready(0) <='0';
                FSMDataCHOD<=Read4;
             end if;


              when Read5=>
              interpacketCHOD := interpacketCHOD -1;
              if s_rdMAC(0)='1' then
                if not endfile(CHODfile0) then
                  read(CHODfile0,indata0);
                  MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_MTPlength(0)(15 downto 8) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataCHOD<=Read6;
                else
                  file_close(CHODfile0);
                  s_reoframe(0)<='1';
                  FSMDataCHOD<=Latency;
                end if;
              else
                MACready(0) <='0';
                FSMDataCHOD<=Read5;
              end if;


           when Read6=>
              interpacketCHOD := interpacketCHOD -1;
              if s_rdMAC(0)='1' then
                if not endfile(CHODfile0) then
                  read(CHODfile0,indata0);
                  MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_PrimNumber(0) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataCHOD<=Read7;
                else
                  file_close(CHODfile0);
                  s_reoframe(0)<='1';
                  FSMDataCHOD<=Latency;
                end if;
              else
                MACready(0) <='0';
                FSMDataCHOD<=Read6;
              end  if;


              when Read7=>
              interpacketCHOD := interpacketCHOD -1;
              if s_rdMAC(0)='1' then
                if not endfile(CHODfile0) then
                  read(CHODfile0,indata0);
                  MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_subsourceID(0) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  if(s_MTPlength(0)/=X"08") then
                    s_WasEmpty(0)<='0';
                    FSMDataCHOD<=ReadData;
                  else
                    s_WasEmpty(1)<='1';                  
                    FSMDataCHOD<=RemoveEmpty;
                  end if; 
                else
                  file_close(CHODfile0);
                  s_reoframe(0)<='1';
                  FSMDataCHOD<=Latency;
                end if;
              else
                MACready(0) <='0';
                FSMDataCHOD<=EndPrimitive;                                                                                                                                                         end  if;

           when RemoveEmpty=>
              --Events with 0 primitives have 1 byte at 0 to reach the minimum
              --UDP payload dimension. I remove it.
              interpacketCHOD := interpacketCHOD -1;              
                if not endfile(CHODfile0) then                
                  if(empty<8) then
                    read(CHODfile0,indata0);
                    MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                    FSMDataCHOD <= RemoveEmpty;
                    empty:=empty+1;
                  else
		    s_reoframe(0)<='1';	
                    FSMDataCHOD<= Latency;
                  end if;
                else
                  file_close(CHODfile0);
                  FSMDataCHOD<=Latency;
                end if;
                           

---------END HEADER
              --Now I extract Data:
              -- TimeStamp Word (0x0000)
              -- Primitive TimeStamp High 24 bit
              -- Primitive ID 16 bit
              -- TimeStamp Low 8 bit
              -- FineTime 8 bit

              -- MTPlength = 8: emtpy MTP
           when ReadData=>
              
              interpacketCHOD := interpacketCHOD -1;
              if s_rdMAC(0)='1' then
                if not endfile(CHODfile0) then
                  if s_MTPlength(0) /= X"08" then 
                  read(CHODfile0,indata0);                  
                  MACoutput(0) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_MTPlength(0) <= s_MTPlength(0)(15 downto 0) - X"01";
                  FSMDataCHOD<=ReadData;                                                                                                                                              
                else
                  s_reoframe(0)<='1';
                  FSMDataCHOD<=Latency;
                end if;
              else
              --  file_close(CHODfile0);
                s_reoframe(0)<='1';
                FSMDataCHOD<=Latency;
              end if;
              else
                FSMDataCHOD<=ReadData;
              end  if;
              when EndPrimitive =>
	       interpacketCHOD := interpacketCHOD -1;
               if not endfile(CHODfile0) then
		  FSMDataCHOD<=Read0;
		  MACready(0) <='1';
	       else
		  file_close(CHODfile0);
		  s_reoframe(0)<='1';
		  MACready(0) <='0';
		  FSMDataCHOD<=Latency;
	       end if;



	    when Latency =>
	       if interpacketCHOD > 0 then
		  MACready(0) <='0';
		  interpacketCHOD := interpacketCHOD -1;
		  FSMDataCHOD<=Latency;
	       else
		  MACready(0) <='1';
                  s_reoframe(0)<='0';
		  interpacketCHOD  :=800;
		  FSMDataCHOD <=Read0;
                  s_MTPlength(0)<=X"0000";
                  s_WasEmpty(0)<='0';
                  empty:=0;
		  end if; 
	       

	 end case;

      end if;

   end process;



    -------------MUV DATA GENERATION:-----------------------------------
   datiMUV: process(s_clk125)
      variable delayMUV : integer :=0;
      variable interpacketMUV : integer :=800;
      variable indata0: character;
      variable ipacketMUV : integer :=0;
      variable empty : integer :=0;
   begin
      if(s_clk125 ='1') then
	 case FSMDataMUV is				
	    when idle=>	
	       s_reoframe(1)<='0';
	       if delayMUV > 0 then
		  MACready(1) <='0';
		  delayMUV := delayMUV -1;
		  FSMDataMUV<=Idle;
                  s_sendtimestamp(1) <= x"000000";
	       else
		  MACready(1) <='1';
		  FSMDataMUV <=Read0;  
	       end if;


---------HEADER: 64 bit: 8 clocks to extact
         -- Source ID 8 bit
         -- Send Timestamp 24 bit
         -- + 
         -- Sub Source ID 8 bit
         -- Number of Primitives 8 bit
         -- Length 16 bit
         
               
           when Read0=> 
	       interpacketMUV := interpacketMUV -1;
	       if s_rdMAC(1)='1' then
		  if not endfile(MUVfile0) then  
		     read(MUVfile0,indata0);
		     MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                     s_sendtimestamp(1)(7 downto 0)<= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);       
		     FSMDataMUV<=Read1;
		  else
		     file_close(MUVfile0);
		     s_reoframe(1)<='1';
		     FSMDataMUV<=Latency;
		  end if;
		  
	       else
		  MACready(1) <='0';
                  s_sendtimestamp(1) <= x"000000";
		  FSMDataMUV<=EndPrimitive;
	       end if;  

	       
	       
	       
	    when Read1=> 
	       interpacketMUV := interpacketMUV -1;
	       if s_rdMAC(1)='1' then
		  if not endfile(MUVfile0) then
		     read(MUVfile0,indata0);
		     MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                     s_sendtimestamp(1)(15 downto 8)<= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
		     FSMDataMUV<=Read2;
		  else
		     file_close(MUVfile0);
		     s_reoframe(1)<='1';
		     FSMDataMUV<=Latency;
		  end if;
	       else
		  MACready(1) <='0';
		  FSMDataMUV<=EndPrimitive;
	       end if;
	       

           when Read2=>
              interpacketMUV := interpacketMUV -1;
              if s_rdMAC(1)='1' then
                if not endfile(MUVfile0) then
                  read(MUVfile0,indata0);
                  MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_sendtimestamp(1)(23 downto 16)<= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  FSMDataMUV<=Read3;
                else
                  file_close(MUVfile0);
                  s_reoframe(1)<='1';
                  FSMDataMUV<=Latency;
                end if;
              else
                MACready(1) <='0';
                FSMDataMUV<=EndPrimitive;                                                                                                                                                         end  if;


           when Read3=>
              interpacketMUV := interpacketMUV -1;
              if s_rdMAC(1)='1' then
                if not endfile(MUVfile0) then
                  read(MUVfile0,indata0);
                  MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_sourceID(1) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataMUV<=Read4;
                else
                  file_close(MUVfile0);
                  s_reoframe(1)<='1';
                  FSMDataMUV<=Latency;
                end if;
              else
                MACready(1) <='0';
                FSMDataMUV<=Read3;
              end  if;

              when Read4=>
              interpacketMUV := interpacketMUV -1;
              if s_rdMAC(1)='1' then
                if not endfile(MUVfile0) then
                  read(MUVfile0,indata0);
                  MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_MTPlength(1)(7 downto 0) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataMUV<=Read5;
                else
                  file_close(MUVfile0);
                  s_reoframe(1)<='1';
                  FSMDataMUV<=Latency;
                end if;
              else
                MACready(1) <='0';
                FSMDataMUV<=Read4;
             end if;


              when Read5=>
              interpacketMUV := interpacketMUV -1;
              if s_rdMAC(1)='1' then
                if not endfile(MUVfile0) then
                  read(MUVfile0,indata0);
                  MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_MTPlength(1)(15 downto 8) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataMUV<=Read6;
                else
                  file_close(MUVfile0);
                  s_reoframe(1)<='1';
                  FSMDataMUV<=Latency;
                end if;
              else
                MACready(1) <='0';
                FSMDataMUV<=Read5;
              end if;


           when Read6=>
              interpacketMUV := interpacketMUV -1;
              if s_rdMAC(1)='1' then
                if not endfile(MUVfile0) then
                  read(MUVfile0,indata0);
                  MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_PrimNumber(1) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);                  
                  FSMDataMUV<=Read7;
                else
                  file_close(MUVfile0);
                  s_reoframe(1)<='1';
                  FSMDataMUV<=Latency;
                end if;
              else
                MACready(1) <='0';
                FSMDataMUV<=Read6;
              end  if;


              when Read7=>
              interpacketMUV := interpacketMUV -1;
              if s_rdMAC(1)='1' then
                if not endfile(MUVfile0) then
                  read(MUVfile0,indata0);
                  MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_subsourceID(1) <= CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  if(s_MTPlength(1)/=X"08") then
                    s_WasEmpty(1)<='0';
                    FSMDataMUV<=ReadData;
                  else
                    s_WasEmpty(1)<='1';
                    
                    FSMDataMUV<=RemoveEmpty;
                  end if; 
                else
                  file_close(MUVfile0);
                  s_reoframe(1)<='1';
                  FSMDataMUV<=Latency;
                end if;
              else
                MACready(1) <='0';
                FSMDataMUV<=EndPrimitive;                                                                                                                                                         end  if;

              when RemoveEmpty=>
              interpacketMUV := interpacketMUV -1;              
                if not endfile(MUVfile0) then
                  
                  if(empty<8) then
                    read(MUVfile0,indata0);
                    MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                    FSMDataMUV <= RemoveEmpty;
                    empty:=empty+1;
                  else
		     s_reoframe(1)<='1';
                    FSMDataMUV<= Latency;
                  end if;
                else
                  file_close(MUVfile0);
                  FSMDataMUV<=Latency;
                end if;
                           

---------END HEADER
              --Now I extract Data:
              -- TimeStamp Word (0x0000)
              -- Primitive TimeStamp High 24 bit
              -- Primitive ID 16 bit
              -- TimeStamp Low 8 bit
              -- FineTime 8 bit

              -- MTPlength = 8: emtpy MTP
           when ReadData=>
              
              interpacketMUV := interpacketMUV -1;
              if s_rdMAC(1)='1' then
                if not endfile(MUVfile0) then
                  if s_MTPlength(1) /= X"08" then 
                  read(MUVfile0,indata0);                  
                  MACoutput(1) <=  CONV_STD_LOGIC_VECTOR(character'pos(indata0), 8);
                  s_MTPlength(1) <= s_MTPlength(1)(15 downto 0) - X"01";
                  FSMDataMUV<=ReadData;                                                                                                                                              
                else
                  --file_close(MUVfile0);
                  s_reoframe(1)<='1';
                  FSMDataMUV<=Latency;
                 end if;
               else
               file_close(MUVfile0);
               s_reoframe(1)<='1';
               FSMDataMUV<=Latency;
               end if;
              else
                FSMDataMUV<=ReadData;                                                                                                                                                               end  if;
              
               
	    when EndPrimitive =>
	       interpacketMUV := interpacketMUV -1;
	       if not endfile(MUVfile0) then
		  FSMDataMUV<=Read0;
		  MACready(1) <='1';
	       else
		  file_close(MUVfile0);
		  s_reoframe(1)<='1';
		  MACready(1) <='0';
		  FSMDataMUV<=Latency;
	       end if;



	    when Latency =>
	       if interpacketMUV > 0 then
		  MACready(1) <='0';
		  interpacketMUV := interpacketMUV -1;
		  FSMDataMUV<=Latency;
	       else
		  MACready(1) <='1';
                  s_reoframe(1)<='0';
		  interpacketMUV  :=800;
		  FSMDataMUV <=Read0;
                  s_MTPlength(1)<=X"0000";
                  s_WasEmpty(1)<='0';
                  empty:=0;
		  end if; 
	       

	 end case;

      end if;

   end process;  







--**************************************************************
--**************************************************************

---------------------
   process(s_clk40)
   begin
      if rising_edge(s_clk40) then
	 case state is				
	    when idle=>				
	       counter_RUN <=(others=>'0');
	       if counter_INTERRUN < 400000000 then --10 sec
		  counter_INTERRUN <=counter_INTERRUN +1;
		  state <=idle;
	       else 
		  state <=sob;
	       end if;
	       
	    when sob=>
	       counter_INTERRUN <=(others =>'0');
	       state <=run;
	       
	    when run =>
	       if counter_RUN < (199999999) then --5 sec
		  counter_RUN <= counter_RUN +1;
		  state <= run;
	       else
		  state <= eob;
	       end if;
	       
	    when eob =>
	       counter_RUN <=(others=>'0');
	       state <= idle;
	 end case;
      end if;
   end process;
--
--
-- Output depends solely on the current state
   process (state)
   begin
      case state is
	 when idle =>
	    BCRST_fake <= '0';
	    ECRST_fake <= '0';
            s_fakereset<='0';
	 when sob =>
	    ECRST_fake <='1';
	    BCRST_fake <='1';
            s_fakereset<='1';
	 when run =>
	    BCRST_fake <= '0';
	    ECRST_fake <= '0';
            s_fakereset<='1';
	 when eob =>
	    ECRST_fake <='1';
	    BCRST_fake <='0';
            s_fakereset<='1';
      end case;
   end process;



   
 outputfiles: process(s_clk125)
    variable v_OLINE     : line;

 begin
   if(s_clk125 ='1') then
     if s_debug_ready = '1' then
        write(v_OLINE, s_debug);
        writeline(file_RESULTS, v_OLINE); 
     end if;
   end if;

 end process;

end rtl;

