onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.wrreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/FSMDataCHOD
add wave -noupdate -radix hexadecimal /ethlinksimulation/MACoutput(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/s_reoframe(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMReceive(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.headermode(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.headerreceived(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.data0(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.data1(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.data2(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.data3(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.MTPTimestampH(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.MTPNUMREF(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.data
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMDelay(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allnets.FIFODELAY(0).inputs.rdreq
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allcmps.FIFODELAY(0).outputs.q
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.packet_received
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.received_signal(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.primitiveID(0)
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/outputs.finetime(0)
add wave -noupdate -radix hexadecimal -childformat {{/ethlinksimulation/ethlink_inst/outputs.timestamp(0) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(1) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(2) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(3) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(4) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(5) -radix hexadecimal} {/ethlinksimulation/ethlink_inst/outputs.timestamp(6) -radix hexadecimal}} -subitemconfig {/ethlinksimulation/ethlink_inst/outputs.timestamp(0) {-height 17 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(1) {-height 17 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(2) {-height 17 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(3) {-height 17 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(4) {-height 17 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(5) {-height 17 -radix hexadecimal} /ethlinksimulation/ethlink_inst/outputs.timestamp(6) {-height 17 -radix hexadecimal}} /ethlinksimulation/ethlink_inst/outputs.timestamp
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.nprimitivecontrol
add wave -noupdate -radix hexadecimal /ethlinksimulation/trigger_inst/allregs.dout.clk125.nprimitiveref
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {152138000 ps} 0} {{Cursor 2} {8210000 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 510
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1000
configure wave -griddelta 80
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {8312130 ps} {8334346 ps}
