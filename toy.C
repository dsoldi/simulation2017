#include <vector>

//15 Jan 2019:
//Emulation of  procedure SubReadFifo of the L0TP.
//Due to the FSM implementation in L0TP, the last datum
//from the first FIFO reaching the end of frame is not
//handled, but lost.

vector<unsigned long long> Merging(vector<unsigned long long> ReferenceFIFO,vector<unsigned long long> ControlFIFO){

  int MTPRef=ReferenceFIFO.size(); 
  int MTPCntrl=ControlFIFO.size();

  int indexC=0;//index of the FIFO
  int indexR=0;//index of the FIFO


  unsigned long long ValueR=0;//Register of the FIFO
  unsigned long long ValueC=0;//Register of the FIFO

  unsigned long long Datum=0;
  
  vector<unsigned long long> MergedFifo;

  //STATE: WaitForPackets: Both the boolean for the reading are set to 1
    
  bool ReadReference = 1;
  bool ReadControl   = 1;
  bool PrimitiveFinish=0;
  bool PrimitiveControlFinish=0;

  while(1){

    Datum=0;
    
    //STATE: ReadFIFO:
    if(indexC == MTPCntrl) PrimitiveControlFinish=1;
    if(indexR == MTPRef)   PrimitiveFinish=1;

    if(PrimitiveControlFinish==1 && PrimitiveFinish==1) break; //Come back to wait for packet
  
    //Read request. I cannot use Primitive Control Finish in the same state, because it is not yet set
    if(indexR <= MTPRef && ReadReference == 1) {
      ValueR = ReferenceFIFO.at(indexR);
      indexR++;
    }
      
    if(indexC <= MTPCntrl && ReadControl == 1) {
      ValueC = ControlFIFO.at(indexC);
      indexC++;
    }

    if(PrimitiveFinish==0 && PrimitiveControlFinish==0) {

      if((ValueR>>7) < (ValueC>>7)) { // comparing TSTMP + 1 bit of FT (rejecting last 7 bits)

	Datum = ((unsigned long long)0x1)<<38 | ValueR;
	MergedFifo.push_back(Datum);
	ReadReference=1;
	ReadControl=0;	  
      }

      else if((ValueC>>7) < (ValueR>>7)) {
	Datum = ((unsigned long long)0x10)<<38 | ValueC;	
	MergedFifo.push_back(Datum);
	ReadReference=0;
	ReadControl=1;	  
      }

      else if((ValueC>>7) == (ValueR>>7)) {
	Datum = ((unsigned long long)0x11)<<38 | ValueR;	
	MergedFifo.push_back(Datum);
	ReadReference=1;
	ReadControl=1;	  
      }
    }
	
    else if(PrimitiveFinish==0 && PrimitiveControlFinish==1) {
      Datum = ((unsigned long long)0x1)<<38 | ValueR;
      MergedFifo.push_back(Datum);
      ReadReference=1;
      ReadControl=0;	     
    }

    else if(PrimitiveFinish==1 && PrimitiveControlFinish==0) {
      Datum = ((unsigned long long)0x10)<<38 | ValueC;
      MergedFifo.push_back(Datum);
      ReadReference=0;
      ReadControl=1;	     
    }

	
  }//end while 1

  return MergedFifo;
}


unsigned int GetDataType(unsigned long long i){
  //1 is physics
  //16 is control
  //17 is both
return i >> 38;
}

unsigned int GetTime(unsigned long long i){
  //32 bit timestamp + 8 bit finetime
return i & (unsigned int)0xFFFFFFFFFF;
}

void toy(){

  vector<unsigned long long> ReferenceFIFO;
  vector<unsigned long long> ControlFIFO;
  
  ReferenceFIFO.push_back(0x29c87);//insert time 40 bit
  ReferenceFIFO.push_back(0x2a087);
  ReferenceFIFO.push_back(0x2a587);
  ReferenceFIFO.push_back(0x2aa87);
  ReferenceFIFO.push_back(0x2b087);
     
  for (auto i = ReferenceFIFO.begin(); i != ReferenceFIFO.end(); ++i) {
    std::cout<<"Reference Detector: " <<hex<< *i <<endl;
  }

  ControlFIFO.push_back(0x2ae78);//insert time 40 bit
  ControlFIFO.push_back(0x2b378);

  for (auto i = ControlFIFO.begin(); i != ControlFIFO.end(); ++i) {
    std::cout<<"Control Detector: " <<hex<< *i <<endl;
  }

  vector<unsigned long long> MergedFifo;

  MergedFifo=Merging(ReferenceFIFO,ControlFIFO);

  unsigned long long outputMergedFifo=0;
  unsigned int DataTtype=0;
  unsigned int Time=0;
  
  for (auto i = MergedFifo.begin(); i != MergedFifo.end(); ++i) {

    outputMergedFifo = *i;

    DataType = GetDataType(outputMergedFifo);
    Time = GetTime(outputMergedFifo);
    
    std::cout<<"Merged Fifo: " << outputMergedFifo <<endl;
    std::cout<<"Time:        "<< Time    <<endl;
    std::cout<<"DataType:    "<<DataType <<endl;
    
  }
  
}
