--RUN CONTROL PARAMETERS--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.userlib.all;
use work.globals.all;
package runcontrol is
   
   constant p_activaterandomtrigger     : std_logic                       := '0';
   constant p_activatecalibtrigger      : std_logic                       := '0';    
   constant p_activateperiodictrigger0  : std_logic                       := '1';
   constant p_activateperiodictrigger1  : std_logic                       := '1';
   constant p_activatecontroltrigger    : std_logic                       := '1';
   constant p_activatesynchtrigger      : std_logic                       := '0';
   constant p_activateclock20MHz        : std_logic                       := '0';
   constant p_activateprimitivetrigger  : std_logic                       := '1';
   constant p_activateCalib_Nim         : std_logic                       := '0';
   constant p_activateNIMtrigger        : std_logic                       := '0';
   constant p_activateCHOKE             : std_logic                       := '0';
   constant p_activateERROR             : std_logic                       := '0';
   
   constant p_Primitive_in_Mep            : std_logic_vector(31 downto 0) := X"00000008";
   constant p_calib_latency               : std_logic_vector(31 downto 0) := X"00000000";
   constant p_calib_direction             : std_logic_vector(31 downto 0) := X"00000000";
   
   constant p_Fixed_Latency               : std_logic_vector(31 downto 0) := X"00000F00";
   constant p_enable_mask                 : std_logic_vector(15 downto 0)  := X"0003";
   constant p_enable_mask_NIM             : std_logic_vector(7 downto 0)  := X"00";
   
   constant p_reference_detector          : std_logic_vector(31 downto 0) := X"00000000";
   constant p_control_detector            : std_logic_vector(31 downto 0) := X"00000001";
   constant p_reference_detector_NIM      : std_logic_vector(31 downto 0) := X"00000000";
   constant p_finetime_bits               : std_logic_vector(31 downto 0) := X"00000001";
   
   constant p_nimDT                       : std_logic_vector(31 downto 0) := X"00000003";
   constant p_primitiveDT                 : std_logic_vector(31 downto 0) := X"00000003";
   constant p_calib_triggerword           : std_logic_vector(7 downto 0) := X"07";
   constant p_bit_finetime                : std_logic_vector(31 downto 0) := X"00000001";
   constant p_deltapacket                 : std_logic_vector(31 downto 0) := X"00000004";
   constant p_maximum_delay_detector      : std_logic_vector(31 downto 0) := X"00000001";
   
   constant p_control_detector_downcaling : std_logic_vector(31 downto 0) := X"00000000";
   
   constant p_periodictime0                : std_logic_vector(31 downto 0) := X"00000080";
   constant p_periodic_start_time0         : std_logic_vector(31 downto 0) := X"00000100";
   constant p_periodic_end_time0           : std_logic_vector(31 downto 0) := X"02625A00";
   constant p_periodic_triggerword0        : std_logic_vector(5 downto 0)  := "110001";
   
   constant p_periodictime1                : std_logic_vector(31 downto 0) := X"00000100";
   constant p_periodic_start_time1         : std_logic_vector(31 downto 0) := X"02625A00";
   constant p_periodic_end_time1           : std_logic_vector(31 downto 0) := X"0ABA9500";
   constant p_periodic_triggerword1        : std_logic_vector(5 downto 0)  := "110010";
   
   constant p_randomtime                  : std_logic_vector(31 downto 0) := X"0000000a";
   constant p_random_start_time           : std_logic_vector(31 downto 0) := X"00000010";
   constant p_random_end_time             : std_logic_vector(31 downto 0) := X"00040000";
   constant p_random_triggerword          : std_logic_vector(5 downto 0)  := "000010";
   
   constant p_delaydeliveryprimitive      : std_logic_vector(31 downto 0) :=X"00000000";
   constant p_controlreduction_set        : std_logic_vector(31 downto 0) :=X"00000000";
   
   
   constant p_CHOKEMASK                   : std_logic_vector(13 downto 0) :="00000000000000";
   constant p_ERRORMASK                   : std_logic_vector(13 downto 0) :="00000000000000";

   constant p_timecut: vector16bit_t(0 to 6):=(
      X"0040", --0
      X"0040", --1
      X"0040", --2
      X"0040", --3
      X"0040", --4
      X"0040", --5
      X"0040"  --6
      ); 


   constant p_offset: vector32bit_t(0 to 6):=(
      X"00000000", --0
      X"00000003", --1
      X"00000000", --2
      X"00000000", --3
      X"00000000", --4
      X"00000000", --5
      X"00000000");--6

   constant p_control_mask : std_logic_vector(111 downto 0) :=X"7fff7fff00000000000000000000";
   constant p_control_dontcare:std_logic_vector(111 downto 0) :=X"7fff7BFF00000000000000000000";

   constant p_mask: mem:=(
--A --B --C --D --E --F --G
      X"7fff7fff00000000000000000000", --0
      X"7fff7fff00000000000000000000", --1
      X"00007fff00000000000000000000", --2
      X"0000000200000000000000000000", --3
      X"0000000200000000000000000000", --4 
      X"0000000200000000000000000000", --5
      X"0000000200000000000000000000", --6
      X"0000000200000000000000000000", --7
      X"0000000200000000000000000000", --8
      X"0000000200000000000000000000", --9
      X"0000000200000000000000000000", --10
      X"0000000200000000000000000000", --11
      X"0000000200000000000000000000", --12
      X"0000000200000000000000000000", --13
      X"0000000200000000000000000000", --14
      X"0000000200000000000000000000"); --15
      

   constant p_dontcare:mem:=(
      X"3fff7fff00000000000000000000", --0
      X"7BFF7fff00000000000000000000", --1
      X"00003fff00000000000000000000", --2
      X"0000000000000000000000000000", --3ru
      X"0000000000000000000000000000", --4 
      X"0000000000000000000000000000", --5
      X"0000000000000000000000000000", --6
      X"0000000000000000000000000000", --7
      X"0000000000000000000000000000", --8
      X"0000000000000000000000000000", --9
      X"0000000000000000000000000000", --10
      X"0000000000000000000000000000", --11
      X"0000000000000000000000000000", --12
      X"0000000000000000000000000000", --13
      X"0000000000000000000000000000", --14
      X"0000000000000000000000000000"); --15

   constant p_downscaling_set: vector32bit_t(0 to nmask-1):=(
      X"00000004",  --0
      X"00000000",  --1
      X"00000000",  --2
      X"00000000",  --3
      X"00000000",  --4
      X"00000000",  --5
      X"00000000",  --6
      X"00000000",  --7
      X"00000000",  --8
      X"00000000",  --9
      X"00000000",  --10
      X"00000000",  --11
      X"00000000",  --12
      X"00000000",  --13
      X"00000000",  --14
      X"00000000"); --15

   constant p_downscaling_set_NIM: vector32bit_t(0 to 6):=(
      X"00000000",  --0
      X"00000000",  --1
      X"00000000",  --2
      X"00000000",  --3
      X"00000000",  --4
      X"00000000",  --5
      X"00000000");  --6




   constant p_mask_nim :mem_NIM:=(
      "00000",  --0
      "00000",  --1
      "00000",  --2
      "00000",  --3
      "00000",  --4
      "00000",  --5
      "00000",  --6
      "00000"); --7

   constant p_dontcare_nim :mem_NIM:=(
      "00000",  --0
      "00000",  --1
      "00000",  --2
      "00000",  --3
      "00000",  --4
      "00000",  --5
      "00000",  --6
      "00000"); --7

end runcontrol; 

