onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ethlinksimulation/s_clk40
add wave -noupdate -radix hexadecimal /ethlinksimulation/s_clk125
add wave -noupdate /ethlinksimulation/PLL40_inst/locked
add wave -noupdate /ethlinksimulation/PLL125_inst/locked
add wave -noupdate -radix hexadecimal /ethlinksimulation/CTSTMP/internal_timestamp
add wave -noupdate -radix hexadecimal /ethlinksimulation/CTSTMP/internal_timestamp125
add wave -noupdate -radix hexadecimal /ethlinksimulation/s_clk125
add wave -noupdate /ethlinksimulation/ALTTTC_inst/BCRST
add wave -noupdate /ethlinksimulation/ALTTTC_inst/ECRST
add wave -noupdate /ethlinksimulation/ALTTTC_inst/BURST
