onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodicstarttime0
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodicendtime0
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodictime0
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodicstarttime1
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodicendtime1
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodictime1
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/internal_timestamp
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/s_periodiccounter0
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/s_periodiccounter1
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodic_signal0
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodic_signal1
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodic_triggerword0_out
add wave -noupdate -radix hexadecimal /ethlinksimulation/pulser_inst/periodic_triggerword1_out
add wave -noupdate /ethlinksimulation/ethlink_inst/allregs.dout.clk125.periodic_signal0
add wave -noupdate /ethlinksimulation/ethlink_inst/allregs.dout.clk125.periodic_signal1
add wave -noupdate /ethlinksimulation/ethlink_inst/allregs.dout.clk125.periodic_triggerword0
add wave -noupdate /ethlinksimulation/ethlink_inst/allregs.dout.clk125.periodic_triggerword1
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.address_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.data_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.wren_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/SENDRAM_inst/altSENDRAM_inst/data_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/SENDRAM_inst/altSENDRAM_inst/wren_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/SENDRAM_inst/altSENDRAM_inst/address_a
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/allregs.dout.clk125.FSMoutputdata
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/inputs.rden_b
add wave -noupdate -radix hexadecimal /ethlinksimulation/ethlink_inst/LatencyRam_inst/outputs.q_b
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3933115 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 472
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 80
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {5519488 ps}
