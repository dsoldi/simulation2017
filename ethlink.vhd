library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.globals.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

--Simulation module of ethlink. All the procedure are identical to the true
--module, except for the receiving logic: MAC is simulated, it receives bytes
--of primitives coming from ethlinksimulation test bench. Output ethernet logic
--is not implemented. you can just see SENDRAM and LATENCYRAM that go full.


package component_ethlink is

   constant SGMII_NODES : natural := 4; -- 1;
   constant RGMII_NODES : natural := 4; -- 1;

   file CHODprim : TEXT open WRITE_MODE is "CHODprimitives.out";
   file MUVprim : TEXT open WRITE_MODE is "MUVprimitives.out";

--
-- ethlink inputs (edit)
--
   type ethlink_inputs_t is record
--------------------------------- CLOCK DOMAINS
      clkin_40      : std_logic;--
      clkin_50      : std_logic;--
      clkin_125     : std_logic;--
      -- reset list------------------
      cpu_resetn    : std_logic;--

      -- USER dip switch (SW6, CMOS inputs)
      USER_DIPSW    : std_logic_vector(7 downto 0);--


      BURST  	    : std_logic; --FROM CROSS DOMAIN--

      --------------------------------- USB input                         
      primitive_In_Mep       : std_logic_vector(31 downto 0)         ;--
      activate_clock20MHz    : std_logic                             ;--
      activate_synch         : std_logic                             ;--
      
      calib_latency           : std_logic_vector(31 downto 0)        ;--
      calib_direction         : std_logic;--
      -------------------------------- TIMESTAMP
      internal_timestamp      : std_logic_vector(31 downto 0)        ; --FROM TTC--
      internal_timestamp125   : std_logic_vector(31 downto 0)        ; --FROM TTC--
      timestamp_physics       : std_logic_vector(31 downto 0)        ; --FROM ETHERNET--
      timestamp_calib         : vector32bit_t(0 to ethlink_NODES - 2); --    
      
      ----------------FINETIME                                                 
      finetime_physics_ref    : std_logic_vector(7 downto 0)         ;--
      finetime_physics0       : vector8bit_t(0 to ethlink_NODES - 2) ;--	
      finetime_physics1       : vector8bit_t(0 to ethlink_NODES - 2) ;--	
      finetime_physics2       : vector8bit_t(0 to ethlink_NODES - 2) ;--	
      
      finetime_calib          : vector8bit_t(0 to ethlink_NODES - 2) ;--
      ---trigger primitives                                                  
      primitiveID_t0           : vector16bit_t(0 to ethlink_NODES - 2) ;--
      primitiveID_t1           : vector16bit_t(0 to ethlink_NODES - 2) ;--
      primitiveID_t2          : vector16bit_t(0 to ethlink_NODES - 2) ;--

      --calib primitives                                                   
      primitiveID_c           : vector16bit_t(0 to ethlink_NODES - 2) ;--
      -----------------TRIGGERWORD        
      periodic_triggerword0   : std_logic_vector(5 downto 0)         ;--	
      periodic_triggerword1   : std_logic_vector(5 downto 0)         ;--	
      random_triggerword      : std_logic_vector(5 downto 0)         ;--	
      triggerword        	   : std_logic_vector(5 downto 0)    ;  -- FROM TRIGGER MODULE - OUT OF LUT-- 
      triggerword_calib       : std_logic_vector (5 downto 0)        ;--      
      calibration_nim         : std_logic                            ;--        
      --------------------SIGNAL EVENT HAPPENED        
      trigger_signal     	   : std_logic                       ;	-- FROM TRIGGER         MODULE--
      control_signal     	   : std_logic                       ;	-- FROM TRIGGER         MODULE--
      random_signal           : std_logic                            ;      
      periodic_signal0        : std_logic                            ;	-- FROM TRIGGER         MODULE--
      periodic_signal1        : std_logic                            ;	-- FROM TRIGGER         MODULE--
      calib_signal            : std_logic_vector (6 downto 0)        ;--
      synch_signal            : std_logic                            ;--		        
      CHOKE_signal            : std_logic_vector(13 downto 0)        ;--
      ERROR_signal            : std_logic_vector(13 downto 0)        ;--	
      
      CHOKE_OFF               : std_logic                            ;--
      CHOKE_ON                : std_logic                            ;--
      ERROR_ON                : std_logic                            ;--
      ERROR_OFF               : std_logic                            ;--
      
      Fixed_Latency           : std_logic_vector(31 downto 0)        ;--
-----------------------------------------------------------                  
      
      activate_primitives     : std_logic                            ;--
      enable_mask             : std_logic_vector(nmask-1 downto 0);
      downscaling     		   : vector32bit_t(0 to nmask-1);
      countertriggers 		   : vector32bit_t(0 to nmask-1);
      number_of_trigger 		: vector32bit_t(0 to nmask-1);
      control_trigger_counter : std_logic_vector(31 downto 0);
      mask                    : mem;
      dontcare                : mem;
      triggerflag              : std_logic_vector(15 downto 0);

      reference_detector      : std_logic_vector(31 downto 0);

---------------------------
      MACoutput : vector8bit_t(0 to ethlink_NODES-2);
      MACReady  : std_logic_vector(0 to 2);
      reoframe  : std_logic_vector(0 to 2);
      delay_set               : vector32bit_t(0 to 6);
      maximum_delay_detector  : std_logic_vector(31 downto 0);	

   end record;

--
-- ethlink outputs (edit)
--
   type ethlink_outputs_t is record

      -- sgmii phy reset
      resetn : std_logic;
      

      -----------------------LTU PIN OUT
      LTU0 			: std_logic;
      LTU1 			: std_logic;
      LTU2 			: std_logic;
      LTU3 			: std_logic;
      LTU4 			: std_logic;
      LTU5 			: std_logic;
      LTU_TRIGGER 		: std_logic;
      ------------------------ COUNTERS
      MEPNum                      : std_logic_vector(31 downto 0);
      number_of_primitives        : vector32bit_t(0 to ethlink_NODES-2);
      SENDFIFOFULL                : std_logic_vector(31 downto 0);
      number_of_CHOKE             : std_logic_vector(31 downto 0);
      number_of_ERROR             : std_logic_vector(31 downto 0);
      ---------------------- RECEPTION OUTPUT
      received_signal             : std_logic_vector(0 to ethlink_NODES-2);
      primitiveID 	          : vector16bit_t(0 to ethlink_NODES - 2);
      reserved    	          : vector8bit_t(0 to ethlink_NODES - 2);
      finetime    	          : vector8bit_t(0 to ethlink_NODES - 2);
      timestamp   	          : vector32bit_t(0 to ethlink_NODES - 2);
      Fixed_Latency_o             : std_logic_vector(31 downto 0);
      CounterLTU                  : std_logic_vector(31 downto 0);
      periodicrandomtriggercounter: std_logic_vector(31 downto 0);
      randomtriggercounter        : std_logic_vector(31 downto 0);
      rst40                       : std_logic;
      rst50                       : std_logic;
      rst125                      : std_logic;
       
      packet_received             : std_logic;
      MTPNUMREF                   : vector8bit_t(0 to ethlink_NODES-2);
      rdMAC                       :std_logic_vector(0 to 2);

   end record;

   type ethlink_t is record
      inputs : ethlink_inputs_t;
      outputs : ethlink_outputs_t;
   end record;

   type ethlink_vector_t is array(NATURAL RANGE <>) of ethlink_t;

   component ethlink
      port (
	 inputs : in ethlink_inputs_t;
	 outputs : out ethlink_outputs_t
	 );
   end component;

   signal component_ethlink : ethlink_t;

end component_ethlink;


-- libraries (edit)
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.userlib.all;
use work.mac_globals.all;
use work.globals.all;
library std;
use IEEE.std_logic_textio.all;
use STD.textio.all;


----
use work.component_ethlink.all;
use work.component_syncrst1.all;
use work.component_sendram.all;
use work.component_fifopackets.all;
use work.component_latencyram_2.all;
use work.component_fifodelay.all;
use work.component_fifodetectorfarm.all;


-- ethlink entity (constant)
entity ethlink is
   port (
      inputs : in ethlink_inputs_t;
      outputs : out ethlink_outputs_t
      );
end ethlink;

architecture rtl of ethlink is

   type FSMReceive_t is (S0,S1, S2, S3_1,S3_2,S3_3,S3_4,S3_5,S3_6,S3_7,S3_8,S3_loop,S4,S5_header,S5_write_0);
   type FSMReceive_vector_t is array(NATURAL RANGE <>) of FSMReceive_t;
   type FSMDelay_t is (skipdata,waitdata,waitcontrol,waitfinish,endofframe,readdata,waitoutput);
   type FSMDelay_vector_t is array(NATURAL RANGE <>) of FSMDelay_t; 
   type FSMPackets_t is (Idle,WriteEvent,SetFarmAddress,Deadtime_0,Deadtime_1,Deadtime_2);
   type FSMtxtrigger_t is (S0, S1, S2, S3_0, S3_1);
   type FSMtxtrigger_vector_t is array(NATURAL RANGE <>) of FSMtxtrigger_t; 
   type FSMResetCounters_t is (s0,s1);
   type FSMoutputdata_t is (Idle,SetFarmAddress, WriteEvent);
   type FSMSend_t is (Idle,PacketHeader,PacketHeaderEOB,EventHeader,
		      Data_0,Data_1,Data_2,Data_3,Data_4,Data_5,Data_6,Data_7,Data_8, --common data
		      Data_9,Data_10,Data_11,Data_12,Data_13,Data_14,  --mask 0
		      Data_15,Data_16,Data_17,Data_18,Data_19,Data_20, --mask 1
		      Data_21,Data_22,Data_23,Data_24,Data_25,Data_26, --mask 2
		      Data_27,Data_28,Data_29,Data_30,Data_31,Data_32, --mask 3
		      Data_33,Data_34,Data_35,Data_36,Data_37,Data_38, --mask 4
		      Data_39,Data_40,Data_41,Data_42,Data_43,Data_44, --mask 5
		      Data_45,Data_46,Data_47,Data_48,Data_49,Data_50, --mask 6
		      Data_51,Data_52,Data_53,Data_54,Data_55,Data_56  --mask 7
		      );

   type FSMSendDetectors_t is (S0_0,S0,S1,S2,S3);

   type reglist_clk40_t is record
      LTU0 			: std_logic;
      LTU1 			: std_logic;
      LTU2 			: std_logic;
      LTU3 			: std_logic;
      LTU4 			: std_logic;
      LTU5 			: std_logic;
      LTU_TRIGGER 		: std_logic;
      div2 			: std_logic;
      FSMSendDetectors          : FSMSendDetectors_t;
      latencyreadaddressb       :  signed(14 downto 0);
      oldlatencyreadaddressb    :  signed(14 downto 0);
      sumoldlatencyreadaddressb :  signed(14 downto 0);
      latencycounter            :  integer;
      ClockDiv  	        :  natural range 0 to 1;
      EOB 			:  std_logic;
      firstdata			: std_logic;
      Fixed_Latency             : std_logic_vector(31 downto 0);
      counterLTU                : std_logic_vector(31 downto 0);
      TriggerSTOP               : std_logic;
      activate_synch            : std_logic;
      write_ev40                : std_logic;
      sob_ev40                  : std_logic;
      eob_ev40                  : std_logic;
      ecounter40                : std_logic_vector(15 downto 0);
      BURST40                   : std_logic;
      synch_signal              : std_logic;
      activate_clock20MHz       : std_logic;
      internal_timestamp        : std_logic_vector(31 downto 0);

      CHOKE_ON                  : std_logic;
      CHOKE_OFF                 : std_logic;
      ERROR_ON                  : std_logic;
      ERROR_OFF                 : std_logic;
      sentCHOKE_ON              : std_logic;
      sentCHOKE_OFF             : std_logic;
      sentERROR_ON              : std_logic;
      sentERROR_OFF             : std_logic;
      chokeOn_ev40              : std_logic;
      chokeOff_ev40             : std_logic;
      errorOn_ev40              : std_logic;
      errorOff_ev40             : std_logic;
      counterCHOKE              : std_logic_vector(31 downto 0);
      counterERROR              : std_logic_vector(31 downto 0);              
   end record;

   constant reglist_clk40_default : reglist_clk40_t :=
      ( 
	 LTU0 			=> '0'            ,
	 LTU1 			=> '0'            ,
	 LTU2 			=> '0'            ,
	 LTU3 			=> '0'            ,
	 LTU4 			=> '0'            ,
	 LTU5 			=> '0'            ,
	 LTU_TRIGGER 		=> '0'            ,
	 div2                  	=> '0'            ,
	 ClockDiv              	=> 0              ,
	 FSMSendDetectors      	=>S0_0            ,
	 latencyreadaddressb   	=>(others=>'0')   ,
	 oldlatencyreadaddressb	=>(others=>'0')   ,
	 sumoldlatencyreadaddressb=>(others=>'0') ,
	 latencycounter        	=>0  		  ,
	 EOB 	     	        =>'0'		  ,
	 firstdata 	        =>'0'             ,
	 FIxed_Latency          =>(others =>'0') ,
	 counterLTU             =>(others =>'0') ,
	 TriggerSTOP 		=>'0'            ,
	 activate_synch         =>'0'            ,
	 write_ev40             => '0'            ,
	 ecounter40             => (others =>'0') ,
	 sob_ev40               =>'0'             ,
	 eob_ev40               =>'0'             ,
	 BURST40                =>'0'	          ,
	 synch_signal           => '0'            ,
	 activate_clock20MHz    => '0'            ,
	 internal_timestamp  => (others=>'0')     ,
	 CHOKE_OFF               => '0'           ,
	 CHOKE_ON                => '0'           ,
	 ERROR_OFF               => '0'           ,
	 ERROR_ON                => '0'           ,
	 sentCHOKE_OFF           => '1'           ,
	 sentCHOKE_ON            => '0'           ,
	 sentERROR_ON            => '0'           ,
	 sentERROR_OFF           => '1'           ,
	 chokeOn_ev40            => '0'           ,
	 chokeOff_ev40           => '0'           ,
	 errorOn_ev40            => '0'           ,
	 errorOff_ev40           => '0'		 ,
	 counterCHOKE            => (others=> '0'),
	 counterERROR            => (others=> '0') 	
	 );

--
-- clock domain clk50
--
   type reglist_clk50_t is record

      -- register list
      div2 : std_logic;
      wena : std_logic_vector(0 to SGMII_NODES - 1);
      rena : std_logic_vector(0 to SGMII_NODES - 1);

      -- end of list
      eol : std_logic;

   end record;
   constant reglist_clk50_default : reglist_clk50_t :=
      (
	 div2 => '0',
	 wena => (others => '0'),
	 rena => (others => '0'),
	 eol => '0'
	 );


   type reglist_clk125_t is record
      Fixed_Latency             : std_logic_vector(31 downto 0);
      fifodelay                 : vector32bit_t(0 to ethlink_NODES -2);
      fifodelay_set             : vector32bit_t(0 to ethlink_NODES -2);
      maximum_delay_detector    : std_logic_vector(31 downto 0);
      start_latency              : std_logic_vector(31 downto 0);
      primitive_In_Mep           : std_logic_vector(15 downto 0);
      detector_in_run            : std_logic_vector(6 downto 0);
      readdetector               : std_logic_vector(6 downto 0);
-- FSM list--------------------------------------------------------------------------------
      FSMReceive                : FSMReceive_vector_t(0 to ethlink_NODES-2);
      FSMSend                   : FSMsend_t;
      FSMDelay                 : FSMDelay_vector_t(0 to ethlink_NODES-2);
      FSMResetCounters         : FSMResetCounters_t;
      FSMoutputdata            : FSMoutputdata_t;
      enet_FSMReceive           : FSMreceive_vector_t(0 to RGMII_NODES- 1);

      -- register list
      --
      -- RGMII interface IMPORTANTE!!!!
      enet_rena : std_logic_vector(0 to RGMII_NODES - 1);

      enet_data0 : vector8bit_t(0 to RGMII_NODES - 1);
      enet_data1 : vector8bit_t(0 to RGMII_NODES - 1);
      enet_data2 : vector8bit_t(0 to RGMII_NODES - 1);
      enet_data3 : vector8bit_t(0 to RGMII_NODES - 1);
      enet_data4 : vector8bit_t(0 to RGMII_NODES - 1);
      enet_data5 : vector8bit_t(0 to RGMII_NODES - 1);
      enet_data6 : vector8bit_t(0 to RGMII_NODES - 1);
      enet_data7 : vector8bit_t(0 to RGMII_NODES - 1);
      
      --MTP header RGMII
      enet_headermode      : std_logic_vector(0 to RGMII_NODES-1);
      enet_MTPSourceID     : vector8bit_t(0 to     RGMII_NODES-1);
      enet_MTPPrimitiveNum : vector24bit_t(0 to    RGMII_NODES-1);
      enet_MTPSourceSubID  : vector8bit_t(0 to     RGMII_NODES-1);
      enet_MTPNUMREF          : vector8bit_t(0 to     RGMII_NODES-1);
      enet_MTPLen          : vector16bit_t(0 to    RGMII_NODES-1);
      enet_pcounter        : vector8bit_t(0 to     RGMII_NODES-1); -- for counting how many primitives should be extracted
      --MTP header SGMII
      headermode           : std_logic_vector(0 to ethlink_NODES-2);
      MTPSourceID          : vector8bit_t(0 to ethlink_NODES-2);
      MTPPrimitiveNum      : vector24bit_t(0 to ethlink_NODES-2);
      MTPSourceSubID       : vector8bit_t(0 to ethlink_NODES-2);
      MTPNUMREF               : vector8bit_t(0 to ethlink_NODES-2);
      MTPLen               : vector16bit_t(0 to ethlink_NODES-2);
      pcounter             : vector8bit_t(0 to ethlink_NODES-2); -- for counting how many primitives should be extracted
      ----------------------------------------------------------------------------------------
      data0                : vector8bit_t(0 to ethlink_NODES-2); -- mac output data registers
      data1                : vector8bit_t(0 to ethlink_NODES-2);
      data2                : vector8bit_t(0 to ethlink_NODES-2);
      data3                : vector8bit_t(0 to ethlink_NODES-2);
      data4                : vector8bit_t(0 to ethlink_NODES-2);
      data5                : vector8bit_t(0 to ethlink_NODES-2);
      data6                : vector8bit_t(0 to ethlink_NODES-2);
      data7                : vector8bit_t(0 to ethlink_NODES-2);
      ---------------------------------------------------------------------   	
      --MEP header
      MEPeventNum 		      : std_logic_vector(23 downto 0);
      MEPheader125          : std_logic;
      MEPsourceID 		      : std_logic_vector(7 downto 0);
      MEPsourceSubID 	      : std_logic_vector(7 downto 0);
      MEPnum 			      : std_logic_vector(31 downto 0);
      MEPLen 			      : std_logic_vector(15 downto 0);
------------------------------------------------------------------------------
      wena                 : std_logic_vector(0 to ethlink_NODES-1); -- Setting for MAC
      rena                 : std_logic_vector(0 to ethlink_NODES-1);
      hwaddress            : std_logic_vector(7 downto 0);
      eol                  : std_logic;
      timer 				   : integer; --before reset
--counter registers------------------------------------------------------------
      number_of_primitives : vector32bit_t(0 to ethlink_NODES -2);
      number_of_CHOKE       : std_logic_vector(31 downto 0);
      number_of_ERROR      : std_logic_vector(31 downto 0);
      -----------------------------------------------------------
      sendSOB              : std_logic;
      sendEOB              : std_logic;
      SENDFIFOFULL         : std_logic_vector(31 downto 0);
      sendCHOKE_OFF        : std_logic; 
      sendCHOKE_ON			: std_logic;
      sendERROR_OFF		   : std_logic;
      sendERROR_ON		   : std_logic;
      -----------------------------------------------------------
      old_timestamp            : std_logic_vector(31 downto 0);
      old_tw                   : std_logic_vector(5 downto 0);
      periodicrandomtriggercounter: std_logic_vector(31 downto 0);
      randomtriggercounter     : std_logic_vector(31 downto 0);
      calib_direction          : std_logic;
      calib_latency            : std_logic_vector(31 downto 0);
      periodic_triggerword0    : std_logic_vector(5 downto 0);
      periodic_triggerword1    : std_logic_vector(5 downto 0);
      random_triggerword       : std_logic_vector(5 downto 0);
      activate_primitives      : std_logic;
      sob_data125              : std_logic_vector(191 downto 0);
      data_send                : std_logic;
      sob_data_send            : std_logic;
      eob_data_send            : std_logic;
      eob_data125              : std_logic_vector(191 downto 0);
      ecounter125              : std_logic_vector(15 downto 0);
      primitive_In_Mep125      : std_logic_vector(15 downto 0);
      sob_ev125                : std_logic;
      eob_ev125                : std_logic;
      write_ev125              : std_logic;
      chokeOn_ev125            : std_logic;
      chokeOff_ev125           : std_logic;
      errorOn_ev125            : std_logic;
      errorOff_ev125           : std_logic;
      sent                     : std_logic;
      CHOKE_signal            : std_logic_vector(13 downto 0)        ;
      ERROR_signal            : std_logic_vector(13 downto 0)        ;
      
      CHOKE_ON                : std_logic; 
      CHOKE_OFF               : std_logic;  
      ERROR_ON                : std_logic;
      ERROR_OFF               : std_logic; 
      
      chokeOn_data125             : std_logic_vector(191 downto 0)   ;
      chokeOff_data125            : std_logic_vector(191 downto 0)   ;
      errorOn_data125             : std_logic_vector(191 downto 0)   ;
      errorOff_data125            : std_logic_vector(191 downto 0)   ;	
      chokeOn_data_send           : std_logic                        ;
      chokeOff_data_send          : std_logic                        ;
      errorOn_data_send           : std_logic                        ;
      errorOff_data_send          : std_logic                        ;
      
      BURST125                  : std_logic;
      internal_timestamp      : std_logic_vector(31 downto 0);
      
      ---------------------------------------------------------------
      --INPUT 125 MHZ
      --------------------------------- USB input                     
      -------------------------------- TIMESTAMP
      tmptimestamp            : std_logic_vector(31 downto 0)        ;
      timestamp_physics       : std_logic_vector(31 downto 0)        ;
      timestamp_calib         : vector32bit_t(0 to ethlink_NODES - 2);
      ----------------FINETIME                                        
      tmpfinetime0             : vector8bit_t(0 to ethlink_NODES - 2) ;
      tmpfinetime1             : vector8bit_t(0 to ethlink_NODES - 2) ;
      tmpfinetime2             : vector8bit_t(0 to ethlink_NODES - 2) ;
      
      tmpfinetime_ref         : std_logic_vector(7 downto 0)         ;
      finetime_physics_ref    : std_logic_vector(7 downto 0)         ;
      finetime_physics0        : vector8bit_t(0 to ethlink_NODES - 2) ;
      finetime_physics1        : vector8bit_t(0 to ethlink_NODES - 2) ;
      finetime_physics2        : vector8bit_t(0 to ethlink_NODES - 2) ;

      finetime_calib          : vector8bit_t(0 to ethlink_NODES - 2) ;
      ---trigger primitives                                           
      primitiveID_t0           : vector16bit_t(0 to ethlink_NODES - 2) ;
      primitiveID_t1           : vector16bit_t(0 to ethlink_NODES - 2) ;
      primitiveID_t2           : vector16bit_t(0 to ethlink_NODES - 2) ;
      
      tmpprimitiveID0          : vector16bit_t(0 to ethlink_NODES - 2) ;
      tmpprimitiveID1          : vector16bit_t(0 to ethlink_NODES - 2) ;
      tmpprimitiveID2          : vector16bit_t(0 to ethlink_NODES - 2) ;
      --calib primitives                                              
      primitiveID_c           : vector16bit_t(0 to ethlink_NODES - 2);
      ------------------TRIGGERWORD        
      triggerword        	   : std_logic_vector(5 downto 0)         ;
      tmptriggerword          : std_logic_vector(5 downto 0)         ;
      triggerword_calib       : std_logic_vector (5 downto 0)        ;     
      calibration_nim         : std_logic                            ;
      tmpdatatype             : std_logic_vector (7 downto 0)        ;     
      tmptriggerflag            : std_logic_vector (15 downto 0)      ;
      --------------------SIGNAL EVENT HAPPENED        
      trigger_signal     	   : std_logic                            ;
      periodic_signal0        : std_logic                            ;
      periodic_signal1        : std_logic                            ;
      calib_signal            : std_logic_vector (6 downto 0)        ;
      control_signal          : std_logic                            ;
      random_signal           : std_logic                            ;
      -------EOB-------------------------------------------------
      internal_timestamp_eob  : std_logic_vector(31 downto 0)        ;	
      downscaling     		   : vector32bit_t(0 to nmask-1);
      enable_mask             : std_logic_vector(nmask-1 downto 0);
      countertriggers 		   : vector32bit_t(0 to nmask-1);
      number_of_trigger 		: vector32bit_t(0 to nmask-1);
      control_trigger_counter : std_logic_vector(31 downto 0);
      mask_0                  : std_logic_vector(15 downto 0); 
      mask_1                  : std_logic_vector(15 downto 0); 
      mask_2                  : std_logic_vector(15 downto 0); 
      mask_3                  : std_logic_vector(15 downto 0); 
      mask_4                  : std_logic_vector(15 downto 0); 
      mask_5                  : std_logic_vector(15 downto 0); 
      mask_6                  : std_logic_vector(15 downto 0); 
      mask_7                  : std_logic_vector(15 downto 0); 
      calibration_trigger     : std_logic_vector(31 downto 0);
      mask                    : mem;
      dontcare                : mem;
      triggerflag              : std_logic_vector(15 downto 0);
      reference_detector      : std_logic_vector(31 downto 0);
      fixed_Latency125        : std_logic_vector(31 downto 0);
      MEPEventRead            : std_logic_vector(7 downto 0);
      MEPEventWritten         : std_logic_vector(7 downto 0);
      FSMPackets              : FSMPackets_t;
      framelen               : std_logic_vector(10 downto 0);
      addressfarm             : signed(10 downto 0); 
   end record;
   
   
   constant reglist_clk125_default : reglist_clk125_t :=
      (
	 Fixed_Latency=>(others=>'0'),
	 fifODELAY =>(others=>"00000000000000000000000000000000"),
	 fifODELAY_set =>(others=>"00000000000000000000000000000000"),
	 maximum_delay_detector=>(others=>'0'),
	 start_latency =>(others=>'0'),
	 detector_in_run=>(others=>'0'),
	 readdetector   =>(others=>'0'),
	 primitive_In_Mep         =>(others =>'0'),

-- FSM list--------------------------------------------------------------------------------	
	 FSMPackets             =>Idle,
	 FSMReceive             =>(others =>S0),
	 enet_FSMReceive        =>(others =>S0),
	 FSMDelay               =>(others=>skipdata),
	 FSMSend	        => Idle,
	 FSMResetCounters       => S0,
	 FSMoutputdata          => Idle,

	 --------------------------------------                  
	 data0                  =>(others=>"00000000"),
	 data1                  =>(others=>"00000000"),
	 data2                  =>(others=>"00000000"),
	 data3                  =>(others=>"00000000"),
	 data4                  =>(others=>"00000000"),
	 data5                  =>(others=>"00000000"),
	 data6                  =>(others=>"00000000"),
	 data7                  =>(others=>"00000000"),
	 --MTP header                  
	 headermode             => (others =>'0'),
	 MTPSourceID            => (others=>"00000000"),
	 MTPPrimitiveNum        => (others=>"000000000000000000000000") ,
	 MTPSourceSubID         => (others=>"00000000"),
	 MTPNUMREF              => (others=>"00000000"),
	 MTPLen                 => (others=>"0000000000000000"),
	 pcounter               => (others=>"00000000"),
	 --------------------------------------  
	 enet_data0             =>(others=>"00000000"),
	 enet_data1             =>(others=>"00000000"),
	 enet_data2             =>(others=>"00000000"),
	 enet_data3             =>(others=>"00000000"),
	 enet_data4             =>(others=>"00000000"),
	 enet_data5             =>(others=>"00000000"),
	 enet_data6             =>(others=>"00000000"),
	 enet_data7             =>(others=>"00000000"),
	 --MTP header 
	 enet_headermode        => (others =>'0'),
	 enet_MTPSourceID       => (others=>"00000000"),
	 enet_MTPPrimitiveNum   => (others=>"000000000000000000000000") ,
	 enet_MTPSourceSubID    => (others=>"00000000"),
	 enet_MTPNUMREF            => (others=>"00000000"),
	 enet_MTPLen            => (others=>"0000000000000000"),
	 enet_pcounter          => (others=>"00000000"),
	 --MEP header 
	 timer 		        =>0,
	 MEPeventNum 	        => (others=>'0'),
	 MEPheader125 	        =>'1',
	 MEPsourceID            =>(others=>'0'),
	 MEPsourceSubID         =>(others=>'0'),
	 MEPnum 	        =>(others=>'0'),
	 MEPLen 	        =>(others=>'0'),
	 --------------------------------------  
	 wena                   => (others => '0'),
	 rena                   => (others => '0'),	
	 enet_rena              => (others => '0'),	
	 
	 hwaddress              => "00000000",
	 eol                    => '0',
	 --------------------------------------- 
	 
	 number_of_primitives =>(others=>"00000000000000000000000000000000"),
	 
	 SENDFIFOFULL             =>(others=>'0'),
	 primitive_In_Mep125      =>(others =>'0'),
	 number_of_CHOKE          =>(others =>'0'),
	 number_of_ERROR          =>(others =>'0'),
	 old_tw                   =>(others=>'0'),
	 old_timestamp            =>(others=>'0'),
	 sendEOB                  =>'0',
	 periodicrandomtriggercounter =>(others =>'0'),
	 randomtriggercounter =>(others =>'0'),
	 calib_direction          => '0',
	 calib_latency           => (others =>'0'),
	 periodic_triggerword0    =>(others=>'0'),
	 periodic_triggerword1    =>(others=>'0'),

	 random_triggerword    =>(others=>'0'),
	 sendSOB                 => '1',
	 activate_primitives     => '0',
	 sob_data_send           => '0',
	 data_send               => '0',
	 eob_data_send           => '0',
	 chokeOn_data_send       => '0',
	 chokeOff_data_send      => '0',
	 errorOn_data_send       => '0',
	 errorOff_data_send      => '0',
	 
	 sob_data125             =>(others=>'0'),
	 eob_data125             => (others=>'0'),
	 write_ev125             => '0',
	 ecounter125             =>  (others=>'0') ,
	 
	 sendCHOKE_OFF           => '0',
	 sendCHOKE_ON            => '1',
	 sendERROR_ON            => '1',
	 sendERROR_OFF           => '0',
	 chokeOn_data125         => (others=>'0'),
	 chokeOff_data125        => (others=>'0'),
	 errorOn_data125         => (others=>'0'),
	 errorOff_data125        => (others=>'0'),

	 sob_ev125               =>'0',
	 eob_ev125               =>'0',
	 chokeOn_ev125           =>'0',
	 chokeOff_ev125          =>'0',
	 errorOn_ev125           =>'0',
	 errorOff_ev125          =>'0',
	 sent                    =>'1',

	 internal_timestamp      => (OTHERS=>'0'),
	 BURST125              	=> '0',
	 ----------------------------------INPUT 125
	 -------------------------------- TIMESTAMP
	 timestamp_physics        => (OTHERS=>'0'),
	 tmptimestamp             => (OTHERS=>'0'),
	 timestamp_calib          => (OTHERS=>"00000000000000000000000000000000"),
	 ----------------FINETIME                                        
	 finetime_physics0         => (OTHERS=>"00000000"),
	 finetime_physics1         => (OTHERS=>"00000000"),
	 finetime_physics2         => (OTHERS=>"00000000"),

	 tmpfinetime0              => (OTHERS=>"00000000"),
	 tmpfinetime1              => (OTHERS=>"00000000"),
	 tmpfinetime2              => (OTHERS=>"00000000"),

	 tmpfinetime_ref          => (OTHERS=>'0'),
	 finetime_physics_ref     => (OTHERS=>'0'),	
	 finetime_calib           => (OTHERS=>"00000000"),
	 ---trigger primitives                                          
	 primitiveID_t0         => (others => "0000000000000000"),
	 primitiveID_t1         => (others => "0000000000000000"),
	 primitiveID_t2         => (others => "0000000000000000"),
	 
	 tmpprimitiveID0         => (others => "0000000000000000"),
	 tmpprimitiveID1         => (others => "0000000000000000"),
	 tmpprimitiveID2         => (others => "0000000000000000"),
	 --calib primitives                                             
	 primitiveID_c           => (others => "0000000000000000"),
	 ------------------TRIGGERWORD       
	 triggerword        	   => (others => '0'),
	 triggerword_calib       => (others => '0'),
	 tmptriggerword          => (others => '0'),
	 tmpdatatype             => (others => '0'),
	 tmptriggerflag           => (others => '0'),
	 
	 calibration_nim         => '0',
	 --------------------SIGNAL EVENT HAPPENED       
	 trigger_signal     	    => '0',
	 periodic_signal0         => '0',
	 periodic_signal1         => '0',

	 random_signal            => '0',
	 control_signal           => '0',
	 calib_signal             => (others => '0'),
	 CHOKE_signal             => (others => '0'),
	 ERROR_signal             => (others => '0'),
	 CHOKE_OFF                => '0',
	 CHOKE_ON                 => '0',
	 ERROR_ON                 => '0',
	 ERROR_OFF                => '0',
	 	 --------------------------------------------------------       	
	 internal_timestamp_eob    => (others => '0'),
	 countertriggers          => (others => "00000000000000000000000000000000"),	
	 downscaling              => (others => "00000000000000000000000000000000"),	
	 enable_mask              => (others =>'0')                                , 
	 number_of_trigger         => (others => "00000000000000000000000000000000"),
	 control_trigger_counter   => (others=>'0'),
	 mask_0                   => (others=>'0'),	
	 mask_1                   => (others=>'0'),	
	 mask_2                   => (others=>'0'),	
	 mask_3                   => (others=>'0'),	
	 mask_4                   => (others=>'0'),	
	 mask_5                   => (others=>'0'),	
	 mask_6                   => (others=>'0'),	
	 mask_7                   => (others=>'0'),
	 calibration_trigger      => (others =>'0'),
	 mask                     => (others =>"0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"),
	 dontcare                 => (others =>"0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"),
	 triggerflag               => (others =>'0'),
	 reference_detector       => (others =>'0'),
	 fixed_Latency125         => (others=>'0'),

	 MEPEventRead             =>(others=>'0'),
	 MEPEventWritten          =>(others=>'0'),
	 framelen                 =>(others=>'0'),
	 addressfarm              =>(others=>'0')

	 );

   type reglist_t is record
      clk40 : reglist_clk40_t;
      clk50 : reglist_clk50_t;
      clk125 : reglist_clk125_t;
   end record;

--
-- all local resets (edit)
--
-- Notes: one record-element for each clock domain
--
   type resetlist_t is record
      main : std_logic;
      clk40 : std_logic;
      clk50 : std_logic;
      clk125 : std_logic;   
   end record;

--
-- all local nets (edit)
--
   type netlist_t is record

      -- internal clocks
      clk125 : std_logic;
      clk40  : std_logic;
      -- resets (all clock domains)
      rst       : resetlist_t;
      SyncRST   : syncrst1_t;
      SENDRAM    : sendram_t; -- Fifo in which triggerwords are stored before sending them back
      FIFOPACKETS  : fifopackets_t; -- Fifo in which triggerwords are stored before sending them back
      LATENCYRAM : LATENCYRAM_2_t;
      FIFODETECTORFARM : fifodetectorfarm_t;
      FIFODELAY : FIFODELAY_vector_t(0  to ethlink_NODES - 2);

   end record;

   subtype inputs_t is ethlink_inputs_t;
   subtype outputs_t is ethlink_outputs_t;

--
-- all local registers (constant)
--
   type allregs_t is record
      din : reglist_t;
      dout : reglist_t;
   end record;
   signal allregs : allregs_t;

--
-- all local nets (constant)
--
   signal allnets : netlist_t;
   signal allcmps : netlist_t;

--
-- outputs driver (internal signal for read access) (constant)
--
   signal allouts : outputs_t;

begin


   SyncRST : syncrst1 port map
      (
	 inputs => allnets.SyncRST.inputs,
	 outputs => allcmps.SyncRST.outputs
	 );

   
   FIFOPACKETS_inst: fifopackets port map
      (
	 inputs=>allnets.FIFOPACKETS.inputs,
	 outputs=>allcmps.FIFOPACKETS.outputs
	 ); 
   
   LatencyRam_inst: latencyram_2 port map
      (
	 inputs=>allnets.LATENCYRAM.inputs,
	 outputs=>allcmps.LATENCYRAM.outputs
	 );
   

   fifodelay_inst : FOR index IN 0 TO ethlink_NODES - 2 GENERATE
      fifoDelay_inst : fifoDELAY port map
	 (
	    inputs => allnets.FIFODELAY(index).inputs,
	    outputs => allcmps.FIFODELAY(index).outputs
	    );
   END GENERATE;


   SENDRAM_inst: sendram port map
      (
	 inputs=>allnets.SENDRAM.inputs,
	 outputs=>allcmps.SENDRAM.outputs
	 ); 
   
   FIFODETECTORFARM_inst: fifodetectorfarm port map
      (
	 inputs=>allnets.FIFODETECTORFARM.inputs,
	 outputs=>allcmps.FIFODETECTORFARM.outputs
	 ); 

   ----------------------------------------------------------------------
   process (inputs.clkin_50, allnets.rst.clk50)
   begin
      if (allnets.rst.clk50 = '1') then
	 allregs.dout.clk50 <= reglist_clk50_default;
      elsif rising_edge(inputs.clkin_50) then
	 allregs.dout.clk50 <= allregs.din.clk50;
      end if;
   end process;

   process (allnets.clk40, allnets.rst.clk40)
   begin
      if (allnets.rst.clk40 = '1') then
	 allregs.dout.clk40 <= reglist_clk40_default;
      elsif rising_edge(allnets.clk40) then
	 allregs.dout.clk40 <= allregs.din.clk40;
      end if;
   end process;

   process (allnets.clk125, allnets.rst.clk125)
   begin
      if (allnets.rst.clk125 = '1') then
	 allregs.dout.clk125 <= reglist_clk125_default;
      elsif rising_edge(allnets.clk125) then
	 allregs.dout.clk125 <= allregs.din.clk125;
      end if;
   end process;

   process (inputs, allouts, allregs, allnets, allcmps)


      procedure SubReset
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_t;
	    variable ro: in reglist_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_t;
	    variable n : inout netlist_t
	    ) is
      begin
	 n.clk125  := i.clkin_125; --Internal Clock Domains.
	 n.clk40   := i.clkin_40;
	 --
	 -- main reset (clock domain clkin_50,cpu_resetn)
	 -- level 1 (max priority: deasserted first)
	 --
	 
	 n.SyncRST.inputs.clk(1) := i.clkin_50;
	 n.SyncRST.inputs.clr(1) := not(i.cpu_resetn);
	 n.rst.main := n.SyncRST.outputs.rst(1);
	 
	 --
	 -- clock domain clk125    
	 -- level 2 
	 --
	 n.SyncRST.inputs.clk(2) := n.clk125;   
	 n.SyncRST.inputs.clr(2) := n.rst.main;
	 n.rst.clk125 := n.SyncRST.outputs.rst(2);

	 --
	 -- clock domain rxclock
	 -- level 3 
	 --
	 n.SyncRST.inputs.clk(3) := '0';   
	 n.SyncRST.inputs.clr(3) := '1';
	 --n.rst.clk50 := n.SyncRST.outputs.rst(3);
	 --
	 -- clock domain clk40
	 -- level 4
	 --
	 n.SyncRST.inputs.clk(4) := n.clk40;
	 n.SyncRST.inputs.clr(4) := n.rst.clk125;
	 n.rst.clk40 := n.SyncRst.outputs.rst(4);
	 
	
	 n.SyncRST.inputs.clk(5) := i.clkin_50;
	 n.SyncRST.inputs.clr(5) := n.rst.clk40;
	 n.rst.clk50 := n.SyncRst.outputs.rst(5);

	 --
	 -- Unused SyncRst section 
	 --
	 n.SyncRst.inputs.clk(6) := '0';
	 n.SyncRst.inputs.clr(6) := '1';

	 --
	 -- Unused SyncRst section 
	 --
	 n.SyncRst.inputs.clk(7) := '0';
	 n.SyncRst.inputs.clr(7) := '1';

	 --
	 -- Unused SyncRst section 
	 --
	 n.SyncRst.inputs.clk(8) := '0';
	 n.SyncRst.inputs.clr(8) := '1';

	 o.rst40 := n.rst.clk40;
	 o.rst50 := n.rst.clk50;
	 o.rst125:= n.rst.clk125;

      end procedure;


--
-- SubMain combinatorial procedure (edit)
--
-- all clock domains
--
      procedure SubMain
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_t;
	    variable ro: in reglist_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_t;
	    variable n : inout netlist_t
	    ) is
      begin

	 r.clk50.div2 := not(ro.clk50.div2);
	 r.clk125.hwaddress := i.USER_DIPSW(7 downto 0);
	 r.clk125.primitive_In_Mep      := i.primitive_In_Mep(15 downto 0);
	 
	 r.clk40.internal_timestamp     := i.internal_timestamp;
	 r.clk125.internal_timestamp    := i.internal_timestamp125;
	 
	 r.clk40.BURST40                := i.BURST;
	 r.clk125.BURST125              := i.BURST;
	 r.clk125.primitive_In_Mep125   := i.primitive_In_Mep(15 downto 0);
	 
	 
	 n.SENDRAM.inputs.address_a     := (others=>'0'); 
	 n.SENDRAM.inputs.data_a        := (others=>'0');
	 n.SENDRAM.inputs.address_b     := (others=>'0'); 
	 n.SENDRAM.inputs.data_b        := (others=>'0');
	 n.SENDRAM.inputs.wren_a        := '0';
	 n.SENDRAM.inputs.wren_b        := '0';
	 n.SENDRAM.inputs.rden_a        := '0';
	 n.SENDRAM.inputs.rden_b        := '0';
	 n.SENDRAM.inputs.clock_a       := n.clk125;
	 n.SENDRAM.inputs.clock_b       := n.clk125; 
	 
	 n.FIFODETECTORFARM.inputs.data := (others=>'0');
	 n.FIFODETECTORFARM.inputs.aclr := '0';
	 n.FIFODETECTORFARM.inputs.rdclk:= n.clk125;
	 n.FIFODETECTORFARM.inputs.wrclk:= n.clk40;
	 n.FIFODETECTORFARM.inputs.rdreq:= '0';
	 n.FIFODETECTORFARM.inputs.wrreq:= '0';
	 
	 
	 
	 
	 n.FIFOPACKETS.inputs.data     :=(others=>'0');
	 n.FIFOPACKETS.inputs.aclr     :='0';
	 n.FIFOPACKETS.inputs.rdclk    :=n.clk125;
	 n.FIFOPACKETS.inputs.wrclk    :=n.clk125;
	 n.FIFOPACKETS.inputs.rdreq    :='0';
	 n.FIFOPACKETS.inputs.wrreq    :='0';
	 
	 n.LATENCYRAM.inputs.address_a :=(others=>'0'); 
	 n.LATENCYRAM.inputs.data_a    :=(others=>'0');
	 n.LATENCYRAM.inputs.address_b :=(others=>'0'); 
	 n.LATENCYRAM.inputs.data_b    :=(others=>'0');
	 n.LATENCYRAM.inputs.wren_a    :='0';
	 n.LATENCYRAM.inputs.wren_b    :='0';
	 n.LATENCYRAM.inputs.rden_a    :='0';
	 n.LATENCYRAM.inputs.rden_b    :='0';
	 n.LATENCYRAM.inputs.clock_a   :=n.clk125;
	 n.LATENCYRAM.inputs.clock_b   :=n.clk40; 
	 
	 o.SENDFIFOFULL                := ro.clk125.SENDFIFOFULL;
	 o.MEPNum                      := ro.clk125.MEPNum;
	 o.number_of_primitives        := ro.clk125.number_of_primitives;

	 o.LTU0                        := ro.clk40.LTU0;
	 o.LTU1                        := ro.clk40.LTU1;
	 o.LTU2                        := ro.clk40.LTU2;
	 o.LTU3                        := ro.clk40.LTU3;
	 o.LTU4                        := ro.clk40.LTU4;
	 o.LTU5                        := ro.clk40.LTU5;
 	 o.LTU_TRIGGER                 := ro.clk40.LTU_TRIGGER;
	 
	 o.number_of_CHOKE             := ro.clk125.number_of_CHOKE;
	 o.number_of_ERROR             := ro.clk125.number_of_ERROR;
	 
	 o.counterLTU                  := ro.clk40.counterLTU;
	 o.periodicrandomtriggercounter:= ro.clk125.periodicrandomtriggercounter;
	 o.randomtriggercounter        := ro.clk125.randomtriggercounter;

	 o.Fixed_Latency_o             := ro.clk40.fixed_latency;
	 
	 
 	 r.clk40.activate_synch        := i.activate_synch     ;
	 r.clk40.fixed_latency         := i.Fixed_Latency      ;
	 r.clk125.fixed_latency        := i.Fixed_Latency      ;
	 r.clk40.activate_clock20MHz   := i.activate_clock20MHz;
	 r.clk40.synch_signal          := i.synch_signal       ;  

	 r.clk125.calib_direction      := i.calib_direction;
	 r.clk125.calib_latency        := i.calib_latency;
	 r.clk125.activate_primitives  := i.activate_primitives;

	 r.clk125.random_triggerword    := i.random_triggerword   ;
	 r.clk125.periodic_triggerword0 := i.periodic_triggerword0 ;
	 r.clk125.periodic_triggerword1 := i.periodic_triggerword1 ;

	 r.clk125.triggerword        	:= i.triggerword          ; 	
	 r.clk125.triggerword_calib     := i.triggerword_calib    ;  

	 ----------------------------------------------------------
	 r.clk125.timestamp_physics     := i.timestamp_physics  ;  
	 r.clk125.timestamp_calib       := i.timestamp_calib ;

         r.clk125.finetime_physics0     := i.finetime_physics0   ;
         r.clk125.finetime_physics1     := i.finetime_physics1   ;
         r.clk125.finetime_physics2     := i.finetime_physics2   ;
	 
	 r.clk125.finetime_physics_ref  := i.finetime_physics_ref;  
	 r.clk125.finetime_calib        := i.finetime_calib     ;  

	 r.clk125.primitiveID_t0        := i.primitiveID_t0      ;  
	 r.clk125.primitiveID_t1        := i.primitiveID_t1      ;  
	 r.clk125.primitiveID_t2        := i.primitiveID_t2      ;  
	 
	 r.clk125.primitiveID_c         := i.primitiveID_c      ;  
	 
	 r.clk125.calibration_nim       := i.calibration_nim    ;  
	 r.clk125.trigger_signal        := i.trigger_signal     ; 	
	 r.clk125.periodic_signal0      := i.periodic_signal0    ;
	 r.clk125.periodic_signal1      := i.periodic_signal1    ;

	 r.clk125.random_signal         := i.random_signal      ; 
	 r.clk125.control_signal        := i.control_signal     ;
	 r.clk125.calib_signal          := i.calib_signal       ;  
	 r.clk125.CHOKE_signal          := i.CHOKE_signal       ;  
	 r.clk125.ERROR_signal          := i.ERROR_signal       ;  
	 r.clk40.CHOKE_OFF              := i.CHOKE_OFF          ;  
	 r.clk40.CHOKE_ON               := i.CHOKE_ON           ;  
	 r.clk40.ERROR_ON               := i.ERROR_ON           ;  
	 r.clk40.ERROR_OFF              := i.ERROR_OFF          ;  
	 
	 r.clk125.CHOKE_OFF             := i.CHOKE_OFF          ;  
	 r.clk125.CHOKE_ON              := i.CHOKE_ON           ;  
	 r.clk125.ERROR_ON              := i.ERROR_ON           ;  
	 r.clk125.ERROR_OFF             := i.ERROR_OFF          ;  
	 
	 r.clk125.chokeOn_ev125         := ro.clk40.chokeOn_ev40 ;
	 r.clk125.chokeOff_ev125        := ro.clk40.chokeOff_ev40;
	 r.clk125.errorOn_ev125         := ro.clk40.errorOn_ev40 ;
	 r.clk125.errorOff_ev125        := ro.clk40.errorOff_ev40;
	 
	 
	 r.clk125.countertriggers       := i.countertriggers    ; --NO CUT (75 ns or less) 
	 r.clk125.number_of_trigger     := i.number_of_trigger  ; --(>75 ns)
	 r.clk125.control_trigger_counter:= i.control_trigger_counter;
	 
	 for index in 0 to nmask-1 loop
	    if i.enable_mask(index)='0' then
	       r.clk125.downscaling(index)  := (others=>'0')           ;
	       r.clk125.enable_mask(index):='0';
	    elsif i.enable_mask(index)='1' then
	       r.clk125.downscaling(index) := SLV(UINT(i.downscaling(index))+1,32);
	       r.clk125.enable_mask(index):='1';
	    end if;
	 end loop;

	 for index in 0 to ethlink_NODES -2 loop
	    n.FIFODELAY(index).inputs.data           :=(others=>'0');
	    n.FIFODELAY(index).inputs.aclr           :='0';
	    n.FIFODELAY(index).inputs.rdclk          :=n.clk125;
	    n.FIFODELAY(index).inputs.wrclk          :=n.clk125;
	    n.FIFODELAY(index).inputs.rdreq          :='0';
	    n.FIFODELAY(index).inputs.wrreq          :='0';
	 end loop; 

	 r.clk125.fifODELAY_set           :=i.delay_set;
	 r.clk125.maximum_delay_detector :=i.maximum_delay_detector;
	 
	 r.clk125.detector_in_run(0)            :='1';
	 r.clk125.detector_in_run(1)            :='1';
	 r.clk125.detector_in_run(2)            :='0';
	 r.clk125.detector_in_run(3)            :='0';
	 r.clk125.detector_in_run(4)            :='0';
	 r.clk125.detector_in_run(5)            :='0';
	 r.clk125.detector_in_run(6)            :='0';
	 
	 for index in 0 to 6 loop
	    if ro.clk125.detector_in_run(index) ='0' then
	       r.clk125.readdetector(index) :='1';
	    end if;
	 end loop;
	 
	 r.clk125.mask_0                         :=SLV(0,16);
	 r.clk125.mask_1                         :=SLV(1,16);
	 r.clk125.mask_2                         :=SLV(2,16);
	 r.clk125.mask_3                         :=SLV(3,16);
	 r.clk125.mask_4                         :=SLV(4,16);
	 r.clk125.mask_5                         :=SLV(5,16);
	 r.clk125.mask_6                         :=SLV(6,16);
	 r.clk125.mask_7                         :=SLV(7,16);
	 r.clk125.mask                           := i.mask;
	 r.clk125.dontcare                       := i.dontcare;
	 r.clk125.triggerflag                     := i.triggerflag;
	 r.clk125.reference_detector             := i.reference_detector;
	 r.clk125.fixed_Latency125               := i.fixed_Latency;
	 r.clk125.write_ev125                    := ro.clk40.write_ev40 ;
	 r.clk125.ecounter125                    := ro.clk40.ecounter40 ;
	 r.clk125.sob_ev125                      := ro.clk40.sob_ev40;
	 r.clk125.eob_ev125                      := ro.clk40.eob_ev40;  

      end procedure;  
--
-- Rxdata procedure (edit)
--SGMII
-- clock domain: 125MHz
--
      procedure SubReceive
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_clk125_t;
	    variable ro: in reglist_clk125_t;
	    variable o : out outputs_t;
	    variable r : inout reglist_clk125_t;
	    variable n : inout netlist_t
	    
	    ) is
	 variable  CHODline  : line;   --line number declaration  
	 variable  MUVline   : line;   --line number declaration  

      begin
	 
	 FOR index IN 0 to SGMII_NODES-2 LOOP
	    
	    n.FIFODELAY(index).inputs.data           :=(others=>'0');
	    n.FIFODELAY(index).inputs.aclr           :='0';
	    n.FIFODELAY(index).inputs.wrclk          :=n.clk125;
	    n.FIFODELAY(index).inputs.wrreq          :='0';
	    o.rdMAC(index) :='0';		 

	    case ro.FSMReceive(index) is

	       when S0 =>
		  r.FSMReceive(index) := S1;
		  
	       when S1 =>
		  
		  r.rena(index):='1';
		  r.FSMReceive(index) := S2;

	       when S2 => --verify MAC status and enable reception
		  
		  if i.MACReady(index) ='1' then
		     o.rdMAC(index) :='1';
		     r.headermode(index) :='1';
		     r.FSMReceive(index) := S3_1;
		  else
		     r.FSMReceive(index) := S2;
		  end if;
		  
		  
	       when S3_1 =>
		  o.rdMAC(index) :='1';
		  r.data0(index) := i.MACoutput(index); --byte 0
		  r.FSMReceive(index):=S3_2;
		  

	       when S3_2 =>
		  o.rdMAC(index) :='1';
		  r.data1(index) := i.MACoutput(index);
		  r.FSMReceive(index):=S3_3;
		  
	       when S3_3 =>
		  o.rdMAC(index) :='1';
		  r.data2(index) := i.MACoutput(index);
		  r.FSMReceive(index):=S3_4;
		  
		  
	       when S3_4 =>
		  o.rdMAC(index) :='1';
		  r.data3(index) := i.MACoutput(index);
		  r.FSMReceive(index):=S3_5;
		  
		  
	       when S3_5 =>
		  o.rdMAC(index) :='1';
		  r.data4(index) := i.MACoutput(index);
		  r.FSMReceive(index):=S3_6;
		  
	       when S3_6 =>
		  o.rdMAC(index) :='1';
		  r.data5(index) := i.MACoutput(index);
		  r.FSMReceive(index):=S3_7;
		  
	       when S3_7 =>
		  o.rdMAC(index) :='1';
		  r.data6(index) := i.MACoutput(index);
		  r.FSMReceive(index):=S3_8;
		  
	       when S3_8 =>
		  r.data7(index) := i.MACoutput(index);
		  if ro.headermode(index) ='1' then
		     r.FSMReceive(index) := S5_header;
		  else
		     r.FSMReceive(index) := S5_write_0;
		  end if;
		  
	       when S5_header=> --registers for saving header data
		  if ro.activate_primitives ='1' then
		     n.FIFODELAY(index).inputs.data(63):='1';
		     n.FIFODELAY(index).inputs.data(62 downto 56):=ro.data7(index)(6 downto 0);
		     n.FIFODELAY(index).inputs.data(55 downto 48):=ro.data6(index);
		     n.FIFODELAY(index).inputs.data(47 downto 40):=ro.data5(index);
		     n.FIFODELAY(index).inputs.data(39 downto 32):=ro.data4(index);
		     n.FIFODELAY(index).inputs.data(31 downto 24):=ro.data3(index);
		     n.FIFODELAY(index).inputs.data(23 downto 16):=ro.data2(index);
		     n.FIFODELAY(index).inputs.data(15 downto 8) :=ro.data1(index);
		     n.FIFODELAY(index).inputs.data(7 downto 0)  :=ro.data0(index);
		     n.FIFODELAY(index).inputs.wrreq :='1'; --ho scritto l'header
		     r.FSMReceive(index) := S3_loop;
		  else
		     r.FSMReceive(index) := S3_loop;
		  end if;--activate_primitives


	       when S3_loop =>
		  if i.reoframe(index)='0' then
		     r.headermode(index) :='0';
		     o.rdMAC(index) :='1';
		     r.FSMReceive(index) := S3_1;
		  else 
		     r.FSMReceive(index) := S4; 
		  end if;
		  
		  
	       when S5_write_0 =>-----------------LI SCRIVO 1 ----------------
		  r.number_of_primitives(index) := SLV(UINT(ro.number_of_primitives(index))+1,32);

		  if ro.activate_primitives ='1' then
		     
		     n.FIFODELAY(index).inputs.data(63):='0';
		     
		     if ro.data3(index)/="00000000" or ro.data2(index)/="00000000" then --primitive ID /=0
			
			n.FIFODELAY(index).inputs.data(62 downto 56):=ro.data7(index)(6 downto 0);
			n.FIFODELAY(index).inputs.data(55 downto 48):=ro.data6(index);
			n.FIFODELAY(index).inputs.data(47 downto 40):=ro.data5(index);
			n.FIFODELAY(index).inputs.data(39 downto 32):=ro.data4(index);
			n.FIFODELAY(index).inputs.data(31 downto 24):=ro.data3(index);
			n.FIFODELAY(index).inputs.data(23 downto 16):=ro.data2(index);
			n.FIFODELAY(index).inputs.data(15 downto 8) :=ro.data1(index);
			n.FIFODELAY(index).inputs.data(7 downto 0)  :=ro.data0(index);
			n.FIFODELAY(index).inputs.wrreq :='1'; --ho scritto i dati	
			r.FSMReceive(index) := S3_loop;
		     else
			r.FSMReceive(index) := S3_loop;
		     end if;
		  else
		     r.FSMReceive(index) := S3_loop;
		  end if;	
		  
	       when S4 =>
		  if ro.activate_primitives ='1' then
		     n.FIFODELAY(index).inputs.data :=("0111111111111111111111111111111111111111111111111111111111111111");
		     n.FIFODELAY(index).inputs.wrreq :='1'; --ho scritto l'EOF
		  end if;
		  r.FSMReceive(index) := S1;
	    end case;
	 END LOOP;
      end procedure;
      
      procedure SubReadFIFODelay
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_clk125_t;
	    variable ro: in reglist_clk125_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_clk125_t;
	    variable n : inout netlist_t
	    
	    ) is
      begin

	 o.MTPNUMREF                     :=(others=>"00000000");	 
	 o.packet_received     := '0';

	 for index in 0 to ethlink_NODES-2 loop

	    
	    o.received_signal(index)                 := '0';
	    o.timestamp(index)     	             := "00000000000000000000000000000000";	
	    o.finetime(index)      		     := "00000000";
	    o.primitiveID(index)   		     := "0000000000000000";										
	    o.reserved(index)        		     := "00000000";

	    n.FIFODELAY(index).inputs.aclr           :='0';
	    n.FIFODELAY(index).inputs.rdclk          :=n.clk125;
	    n.FIFODELAY(index).inputs.rdreq          :='0';
	    
	    case ro.FSMDelay(index) is
	       
	       when skipdata => 

		  if ro.BURST125 ='1' then
		     
		     if (ro.fifODELAY(index) = ro.fifODELAY_set(index)) then -- skippa un certo numero di pacchetti
			r.FSMDelay(index) := waitfinish;		
		     else
			
			if n.FIFODELAY(index).outputs.rdempty ='0' then            --se voglio skippare n pacchetti, incremento fifodelay 
			   n.FIFODELAY(index).inputs.rdreq :='1'; 		         -- dentro questo IF. in questo modo imposto il ritardo come
			   r.FSMDelay(index) := waitcontrol;
			else
			   r.FSMDelay(index) := skipdata;		 
			end if; --end rdempty 		
		     end if;	
		  else
		     r.FSMDelay(index) := skipdata; --NON IN RUN
		  end if;	--end run



	       when waitcontrol =>
		  if ro.BURST125 ='1' then
		     if n.FIFODelay(index).outputs.q = "0111111111111111111111111111111111111111111111111111111111111111" then  
			r.fifODELAY(index) := SLV(UINT(ro.fifodelay(index))+1,32);-- numero di pacchetti
		     end if;	  
		     r.FSMDelay(index) := skipdata;
		  else
		     r.FSMDelay(index) := skipdata;
		  end if;



	       when waitfinish =>
		  if ro.BURST125 ='1' then
		     if (ro.fifODELAY(0) = ro.fifODELAY_set(0))
			and (ro.fifODELAY(1) = ro.fifODELAY_set(1))
			and (ro.fifODELAY(2) = ro.fifODELAY_set(2))
		     then
			r.FSMDelay(index) := waitdata;
		     else
			r.FSMDelay(index) := waitfinish;
		     end if;
		     
		  else
		     r.FSMDelay(index) := skipdata;
		  end if;

	       when waitdata =>
		  if ro.BURST125 ='1' then
		     if (ro.fifODELAY(0) = ro.fifODELAY_set(0))
			and (ro.fifODELAY(1) = ro.fifODELAY_set(1))
			and (ro.fifODELAY(2) = ro.fifODELAY_set(2))
		     then
			
			if n.FIFODELAY(0).outputs.rdempty ='0' then
			   if UINT(ro.start_latency) = 200  then
			      r.start_latency:=(others=>'0');
			      n.FIFODELAY(index).inputs.rdreq :='1';
			      r.FSMDelay(index) := readdata;
			   else
			      r.start_latency :=SLV(UINT(ro.start_latency)+1,32);
			      r.FSMDelay(index) := waitdata;
			   end if;  
			   
			   
			else
			   r.FSMDelay(index) := waitdata;
			end if;
			
		     else
			r.FSMDelay(index) := waitdata;
		     end if;
		     
		  else
		     r.FSMDelay(index) := skipdata; --end run	
		  end if;

	       when endofframe =>
		  if n.FIFODelay(index).outputs.q = "0111111111111111111111111111111111111111111111111111111111111111" or n.FIFODelay(index).outputs.q=SLV(0,65)--end of packet 
		  then
		     r.FSMDelay(index) :=waitoutput;
		  else  
		     if n.FIFODELAY(index).outputs.rdempty ='0' then
			n.FIFODELAY(index).inputs.rdreq :='1';  
			r.FSMDelay(index) := readdata;
		     else
			r.FSMDelay(index) := endofframe;  
		     end if;

		  end if;
		  
	 	  
		  
	        when readdata =>
		  
		  if ro.BURST125 ='1' then
		     
		     if n.FIFODelay(index).outputs.q(63) = '1' then --header
			r.MTPNUMREF(index)                        := n.FIFODELAY(index).outputs.q(55 downto 48);
			o.received_signal(index)               :='0';	
			r.FSMDelay(index) := endofframe;
			
		     elsif n.FIFODelay(index).outputs.q = "0111111111111111111111111111111111111111111111111111111111111111" then --end of packet
			r.FSMDelay(index) :=endofframe;
			o.received_signal(index)               :='0';	
		     elsif n.FIFODelay(index).outputs.q = "0000000000000000000000000000000000000000000000000000000000000000" then
			r.FSMDelay(index) :=endofframe;
			o.received_signal(index)               :='0';	
			
		     else	
			
			r.FSMDelay(index) := endofframe;
			
		
			if (to_integer(signed(ro.internal_timestamp(30 downto 0)))-(to_integer(signed(n.FIFODELAY(index).outputs.q(62 downto 32))))) > to_integer(signed(ro.fixed_latency125))-768 then
			   o.received_signal(index)     :='0';
			 
			   r.MTPNUMREF(index)           := SLV(UINT(ro.MTPNUMREF(index))-1,8);
			else	
			 
			   o.received_signal(index)      :='1';	
			   
			   o.timestamp(index)     	:= '0' &
			                                   n.FIFODELAY(index).outputs.q(62 downto 56) &
							   n.FIFODELAY(index).outputs.q(55 downto 48) &
							   n.FIFODELAY(index).outputs.q(47 downto 40) &
							   n.FIFODELAY(index).outputs.q(39 downto 32) ;
			   
			   o.finetime(index)        := n.FIFODELAY(index).outputs.q(7 downto 0);
			   o.primitiveID(index)     := n.FIFODELAY(index).outputs.q(31 downto 24) &
						       n.FIFODELAY(index).outputs.q(23 downto 16);
			   o.reserved(index)        := n.FIFODELAY(index).outputs.q(15 downto 8);  					   
			end if; --controllo timestamp 
		     end if;--controllo dati
		  else
		     r.FSMDelay(index) := skipdata;
		  end if;	 


		  
	       when waitoutput =>    
		  if (n.FIFODelay(0).outputs.q =  "0111111111111111111111111111111111111111111111111111111111111111" or n.FIFODelay(0).outputs.q=SLV(0,64))
		     and (n.FIFODelay(1).outputs.q = "0111111111111111111111111111111111111111111111111111111111111111" or n.FIFODelay(1).outputs.q=SLV(0,64))
		     and (n.FIFODelay(2).outputs.q = "0111111111111111111111111111111111111111111111111111111111111111" or n.FIFODelay(2).outputs.q=SLV(0,64))    


		  then
		     o.packet_received :='1';     
		     o.MTPNUMREF := ro.MTPNUMREF;
		     r.FSMDelay(index) := waitdata;
		  else
		     o.packet_received :='0';
		     r.FSMDelay(index) := waitoutput;
		  end if;
	    end case;

	 end loop;

      end procedure;
      

--
-- Rxdata procedure (edit)
--
-- clock domain: rxclock
--
      
      procedure SubSendBuffer
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_clk125_t;
	    variable ro: in reglist_clk125_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_clk125_t;
	    variable n : inout netlist_t
	    ) is

	 
      begin
	 ----Default ---------------------- 
	 n.LATENCYRAM.inputs.clock_a   :=n.clk125;
	 n.LATENCYRAM.inputs.wren_a    :='0';	
	 n.LATENCYRAM.inputs.rden_a    :='0';	
	 
	 n.SENDRAM.inputs.clock_a      :=n.clk125;
	 n.SENDRAM.inputs.rden_a       :='0';
	 n.SENDRAM.inputs.wren_a       :='0';
	 
	 
	 
	 --Burst Check
	 if ro.BURST125 ='1' then

	    r.internal_timestamp_eob        := ro.internal_timestamp;
	    r.sendEOB:='1';
	    
	    if ro.sendSOB = '1' then
	       r.sendSOB                       := '0'                  ;
	       r.sob_data125(191 downto 160)   := ro.internal_timestamp;
	       r.sob_data125(159 downto 152)   := (others=>'0')        ;
	       r.sob_data125(151 downto 146)   := "100010"             ; --TW
	       r.sob_data125(145 downto  34)   := (others=>'0')        ;			
	       r.sob_data125(33 downto 18)     := ("0000000000100010") ; -- event triggerword
	       r.sob_data125(17 downto 10)     := "00100010"           ; --data type		
	    end if; --end sob
	    
	    
	    if ro.CHOKE_ON= '1' and ro.CHOKE_OFF ='0' and ro.sendCHOKE_ON = '1' then
	       
	       
	       r.sendCHOKE_ON                               := '0'                  ;
	       r.sendCHOKE_OFF                              := '1'                  ;
	       r.chokeOn_data125(191 downto 160)            := ro.internal_timestamp;
	       r.chokeOn_data125(159 downto 152)            := (others=>'0')        ; 
	       r.chokeOn_data125(151 downto 146)            := "100100"             ; --TW
	       r.chokeOn_data125(145 downto  34)            := (others=>'0')        ;			
	       r.chokeOn_data125(33 downto 18)              := SLV(ro.CHOKE_signal,16); -- event triggerword
	       r.chokeOn_data125(17 downto 10)              := X"A1"                ; --data type		
	       
	    end if; --end choke on

	    if ro.CHOKE_OFF = '1' and ro.CHOKE_ON = '0' and ro.sendCHOKE_OFF ='1' then
	       
	       r.sendCHOKE_OFF   := '0';
	       r.sendCHOKE_ON    := '1';
	       r.chokeOff_data125(191 downto 160)   := ro.internal_timestamp  ;
	       r.chokeOff_data125(159 downto 152)   := (others=>'0')          ;
	       r.chokeOff_data125(151 downto 146)   := "100101"               ; --TW
	       r.chokeOff_data125(145 downto  34)   := (others=>'0')          ;			
	       r.chokeOff_data125(33 downto 18)     := SLV(ro.CHOKE_signal,16); -- event triggerword
	       r.chokeOff_data125(17 downto 10)     := X"A0"                  ; --data type		
	    end if; --end choke off
	    
	    if ro.ERROR_ON = '1' and ro.ERROR_OFF ='0' and ro.sendERROR_ON   = '1' then
	       
	       
	       r.sendERROR_ON    := '0';
	       r.sendERROR_OFF   := '1';				
	       r.errorOn_data125(191 downto 160)   := ro.internal_timestamp ;
	       r.errorOn_data125(159 downto 152)   := (others=>'0')          ;
	       r.errorOn_data125(151 downto 146)   := "100110"               ; --TW
	       r.errorOn_data125(145 downto  34)   := (others=>'0')          ;			
	       r.errorOn_data125(33 downto 18)     := SLV(ro.ERROR_signal,16); -- event triggerword
	       r.errorOn_data125(17 downto 10)     := X"C1"                  ; --data type		
	    end if; --end error on


	    if ro.ERROR_OFF = '1' and ro.ERROR_ON ='0' and ro.sendERROR_OFF   = '1' then

	       r.sendERROR_OFF   := '0';
	       r.sendERROR_ON    := '1';
	       r.errorOff_data125(191 downto 160)   := ro.internal_timestamp   ;
	       r.errorOff_data125(159 downto 152)   := (others=>'0')           ;
	       r.errorOff_data125(151 downto 146)   := "100111"                ; --TW
	       r.errorOff_data125(145 downto  34)   := (others=>'0')           ;			
	       r.errorOff_data125(33 downto 18)     := SLV(ro.ERROR_signal,16) ; -- event triggerword
	       r.errorOff_data125(17 downto 10)     := X"C0"                   ; --data type		
	    end if; --end sob

	    
	 else --Se sono fuori dal Burst...
	    if ro.sendEOB ='1' then			
	       r.sendEOB    :='0';
	       r.sendSOB    :='1';	
	       r.eob_data125(191 downto 160)   := ro.internal_timestamp_eob;
	       r.eob_data125(159 downto 152)   := (others=>'0');
	       r.eob_data125(151 downto 146)   := "100011"     ; --TW
	       r.eob_data125(145 downto 130)   := (others=>'0');
	       r.eob_data125(129 downto 114)   := (others=>'0');
	       r.eob_data125(113 downto 98)    := (others=>'0');
	       r.eob_data125(97 downto  82)    := (others=>'0');
	       r.eob_data125(81 downto  66)    := (others=>'0');
	       r.eob_data125(65 downto  50)    := (others=>'0');
	       r.eob_data125(49 downto  34)    := (others=>'0');         		
	       r.eob_data125(33 downto 18)     := (others=>'0');
	       r.eob_data125(17 downto 10)     := "00100011"     ; --data type
	    end if;
	    r.addressfarm   :=(others=>'0');
	    r.sendCHOKE_OFF :='0';
	    r.sendERROR_OFF :='0';
	    r.sendCHOKE_ON  :='1';
	    r.sendERROR_ON  :='1';
	    r.errorOff_data125 := (others=>'0');
	    r.errorOn_data125  := (others=>'0');
	    r.chokeOn_data125  := (others=>'0');
	    r.chokeOff_data125 := (others=>'0');
	    
	 end if; --end controllo sul Burst

	 case ro.FSMoutputdata is

	    when Idle => 

--VAI LEGGI L'INDIRIZZO E VAI ALLO STATO SUCCESSIVO( avendo memorizzato i valori )

	       if ro.trigger_signal ='1' and ro.control_signal ='0' then
		  n.LATENCYRAM.inputs.rden_a     := '1'                              ;
		  n.LATENCYRAM.inputs.address_a  :=ro.timestamp_physics(14 downto 0) ;
		  r.FSMoutputdata                :=SetFarmAddress                    ;
		  r.tmptimestamp                 :=ro.timestamp_physics              ;
		  r.tmptriggerword               :=ro.triggerword                    ;
		  r.tmpdatatype                  :=X"01"                             ; --data type physics
		  r.tmpprimitiveID0               :=ro.primitiveID_t0                ; --location N
		  r.tmpprimitiveID1               :=ro.primitiveID_t1                ; --location N-1
		  r.tmpprimitiveID2               :=ro.primitiveID_t2                ; --location N+1
		  r.tmptriggerflag                :=ro.triggerflag                   ;
		  r.tmpfinetime_ref               :=ro.finetime_physics_ref          ;
		  r.tmpfinetime0                  :=ro.finetime_physics0             ; --location N
		  r.tmpfinetime1                  :=ro.finetime_physics1             ; --location N-1
		  r.tmpfinetime2                  :=ro.finetime_physics2             ; --location N+1
		  
	       elsif ro.control_signal ='1' and ro.trigger_signal ='0' then
		  n.LATENCYRAM.inputs.rden_a      := '1'                             ;
		  n.LATENCYRAM.inputs.address_a  := ro.timestamp_physics(14 downto 0);
		  r.FSMoutputdata                :=SetFarmAddress                    ;
		  r.tmptimestamp                 :=ro.timestamp_physics              ;
		  r.tmptriggerword               :=ro.triggerword                    ;
		  r.tmpdatatype                  :=X"10"                             ; --data type physics
		  r.tmpprimitiveID0              :=ro.primitiveID_t0                 ; --location N
		  r.tmpprimitiveID1              :=ro.primitiveID_t1                 ; --location N-1
		  r.tmpprimitiveID2              :=ro.primitiveID_t2                 ; --location N+1
		  r.tmptriggerflag               :=(others=>'0')                     ;
		  r.tmpfinetime_ref              :=ro.finetime_physics_ref           ;
		  r.tmpfinetime0                 :=ro.finetime_physics0              ; --location N
		  r.tmpfinetime1                 :=ro.finetime_physics1              ; --location N-1
		  r.tmpfinetime2                 :=ro.finetime_physics2              ; --location N+1
		  
	       elsif ro.control_signal ='1' and ro.trigger_signal ='1' then
		  n.LATENCYRAM.inputs.rden_a      := '1'                               ;
		  n.LATENCYRAM.inputs.address_a   := ro.timestamp_physics(14 downto 0) ;
		  r.FSMoutputdata                 :=SetFarmAddress                     ;
		  r.tmptimestamp                  :=ro.timestamp_physics               ;
		  r.tmptriggerword                :=ro.triggerword                     ;
		  r.tmpdatatype                   :=X"11"                              ; --data type
											 --physics
											 --AND control
		  r.tmpprimitiveID0               :=ro.primitiveID_t0                  ; --location N
		  r.tmpprimitiveID1               :=ro.primitiveID_t1                  ; --location N-1
		  r.tmpprimitiveID2               :=ro.primitiveID_t2                  ; --location N+1
  		  r.tmptriggerflag                :=ro.triggerflag                     ;
		  r.tmpfinetime_ref               :=ro.finetime_physics_ref            ;
		  r.tmpfinetime0                  :=ro.finetime_physics0               ; --location N
		  r.tmpfinetime1                  :=ro.finetime_physics1               ; --location N-1
		  r.tmpfinetime2                  :=ro.finetime_physics2               ; --location N+1
		  

	       elsif ro.periodic_signal0 ='1' or ro.periodic_signal1 ='1'  then 
		  n.LATENCYRAM.inputs.rden_a     := '1'                                ;
		  n.LATENCYRAM.inputs.address_a  :=ro.internal_timestamp(14 downto 0)  ;
		  r.FSMoutputdata                :=SetFarmAddress                      ;
		  
	
		  
		  r.tmptimestamp                 :=ro.internal_timestamp; 
		  r.tmptriggerword               :=ro.periodic_triggerword0 or ro.periodic_triggerword1;
		  r.tmpdatatype                  :=X"02"               ; --data type periodics
		  r.tmpprimitiveID0               :=ro.primitiveID_t0  ;
		  r.tmpprimitiveID1               :=ro.primitiveID_t1  ;
		  r.tmpprimitiveID2               :=ro.primitiveID_t2  ;
		  r.tmptriggerflag                :=(others=>'0')      ;
		  r.tmpfinetime_ref              :=(others=>'0')       ;
		  r.tmpfinetime0                 :=(others=>"00000000");
		  r.tmpfinetime1                 :=(others=>"00000000");
		  r.tmpfinetime2                 :=(others=>"00000000");
		  
		  
	       elsif ro.calib_signal /="0000000" and ro.trigger_signal ='0' then 
		  n.LATENCYRAM.inputs.rden_a     :='1'                            ;
		  r.FSMoutputdata                :=SetFarmAddress                 ;
		  r.calibration_trigger  := SLV(UINT(ro.calibration_trigger)+1,32);
		  r.tmptriggerword       :=ro.triggerword_calib                   ;
		  r.tmpdatatype          :=X"04"                                  ; --data type calibration 
		  r.tmpprimitiveID0      :=ro.primitiveID_c                       ;
		  r.tmpprimitiveID1      :=(others=>"0000000000000000")           ;
		  r.tmpprimitiveID2      :=(others=>"0000000000000000")           ;
		  r.tmptriggerflag       :=SLV(ro.calib_signal,16)                ; --event word
		  r.tmpfinetime0         := ro.finetime_calib                     ;
		  r.tmpfinetime1         :=(others=>"00000000")                   ;
		  r.tmpfinetime2         :=(others=>"00000000")                   ;
		  
		  if ro.calib_signal    ="0000001" then
		     r.tmptimestamp         :=ro.timestamp_calib(0); 
		     r.tmpfinetime_ref      := ro.finetime_calib(0);
		     n.LATENCYRAM.inputs.address_a := ro.timestamp_calib(0)(14 downto 0);
		  elsif ro.calib_signal ="0000010" then             
		     r.tmptimestamp         :=ro.timestamp_calib(1)                               ; 
		     r.tmpfinetime_ref      := ro.finetime_calib(1)                               ;
		     n.LATENCYRAM.inputs.address_a := ro.timestamp_calib(1)(14 downto 0)          ;
		  elsif ro.calib_signal ="0000100" then             
		     r.tmptimestamp         :=ro.timestamp_calib(2)                               ; 
		     r.tmpfinetime_ref      := ro.finetime_calib(2)                               ;
		     n.LATENCYRAM.inputs.address_a := ro.timestamp_calib(2)(14 downto 0)          ;
		  elsif ro.calib_signal ="0001000" then             
		     r.tmptimestamp         :=ro.timestamp_calib(3)                               ; 
		     r.tmpfinetime_ref      := ro.finetime_calib(3)                               ;
		     n.LATENCYRAM.inputs.address_a := ro.timestamp_calib(3)(14 downto 0)          ;
		  elsif ro.calib_signal ="0010000" then             
		     r.tmptimestamp         :=ro.timestamp_calib(4)                               ; 
		     r.tmpfinetime_ref      := ro.finetime_calib(4)                               ;
		     n.LATENCYRAM.inputs.address_a := ro.timestamp_calib(4)(14 downto 0)          ;
		  elsif ro.calib_signal ="0100000" then             
		     r.tmptimestamp         :=ro.timestamp_calib(5)                               ; 
		     r.tmpfinetime_ref      := ro.finetime_calib(5)                               ;
		     n.LATENCYRAM.inputs.address_a := ro.timestamp_calib(5)(14 downto 0)          ;
		  elsif ro.calib_signal ="1000000" then             
		     r.tmptimestamp         :=ro.timestamp_calib(6)                               ; 
		     r.tmpfinetime_ref      := ro.finetime_calib(6)                               ;
		     n.LATENCYRAM.inputs.address_a := ro.timestamp_calib(6)(14 downto 0)          ;
		  else             
		     r.tmptimestamp         :=(others=>'0')                                       ;
		     r.tmpfinetime0          :=(others=>"00000000")                               ;
		  end if; --fine calibrazione
		  
		  
	       elsif ro.calibration_nim ='1' then	
		  r.FSMoutputdata                :=SetFarmAddress                                    ;
		  r.tmpfinetime0                 := (others=>"00000000")                             ;
		  r.tmpfinetime1                 := (others=>"00000000")                             ;
		  r.tmpfinetime2                 := (others=>"00000000")                             ;

		  r.tmpfinetime_ref             := (others=>'0')                                     ;
		  r.tmpdatatype                 := X"08"                                             ;
		  r.tmptriggerflag               := (others=>'0')                                     ;
		  r.tmptriggerword              := ro.triggerword_calib                              ; --TW
		  
		  if ro.calib_latency =X"00000000" then
		     r.tmptimestamp := ro.internal_timestamp                                          ; --bit meno significativi timestamp
		  else
		     
		     if ro.calib_direction = '0' then
			r.tmptimestamp :=SLV(UINT(ro.internal_timestamp)+UINT(ro.calib_latency),32)   ;
			n.LATENCYRAM.inputs.address_a := SLV(UINT(ro.internal_timestamp(14 downto 0))+UINT(ro.calib_latency),15); --bit meno significativi timestamp
			n.LATENCYRAM.inputs.rden_a    :='1';
		     else
			r.tmptimestamp :=SLV(UINT(ro.internal_timestamp)-UINT(ro.calib_latency),32);
			n.LATENCYRAM.inputs.address_a := SLV(UINT(ro.internal_timestamp(14 downto 0))-UINT(ro.calib_latency),15); --bit meno significativi timestamp
			n.LATENCYRAM.inputs.rden_a    :='1';
		     end if; --calib direction
		  end if; -- calib latency
		  
	       else

		  r.FSMoutputdata := Idle;
		  
	       end if; --end check data type	
	       
	       
	    when SetFarmAddress =>
	       	--I read latency ram, to see if it is not empty.
	       --if not empty, it means that there were two different types of
	       --triggers in the same timestamp.
	       --then I carry the same address of the send ram and replace the word already written
	       --with the new words doing the OR of Triggerword, TriggerFlag and Event type.
	       --if the location of latency ram was empty, it continues as if nothing had happened.
	       
	       if n.LATENCYRAM.outputs.q_a /= "00000000000000000" then  	
		  n.SENDRAM.inputs.address_a                := n.LATENCYRAM.outputs.q_a(15 downto 6); -- leggo la ram
		  n.SENDRAM.inputs.rden_a                   := '1' 	 ;
		  n.LATENCYRAM.inputs.address_a             := ro.tmptimestamp(14 downto 0)                ; --bit meno significativi timestamp
		  n.LATENCYRAM.inputs.data_a(5 downto 0)    := ro.tmptriggerword OR n.LATENCYRAM.outputs.q_a(5 downto 0);
		  n.LATENCYRAM.inputs.data_a(16 downto 6)   := n.LATENCYRAM.outputs.q_a(16 downto 6)       ;		      
		  n.LATENCYRAM.inputs.wren_a                :='1';	
		  
	       else
		  n.SENDRAM.inputs.address_a                := std_logic_vector(ro.addressfarm(9 downto 0));
		  n.LATENCYRAM.inputs.address_a             := ro.tmptimestamp(14 downto 0)                ; --bit meno significativi timestamp
		  n.LATENCYRAM.inputs.data_a(5 downto 0)    := ro.tmptriggerword;
		  n.LATENCYRAM.inputs.data_a(16 downto 6)   := std_logic_vector(ro.addressfarm(10 downto 0));		      
		  n.LATENCYRAM.inputs.wren_a                :='1'; 				 
	       end if;
	       
	       r.FSMoutputdata := WriteEvent;
	       
	    when WriteEvent =>
	       
	       if n.LATENCYRAM.outputs.q_a /= "00000000000000000" and ro.tmpdatatype= X"10" then
		  --if control trigger, I have to maintain the trigger infos of
		  --physics, without overwrite the ram
		  r.addressfarm := ro.addressfarm; 			
		  n.SENDRAM.inputs.address_a    := n.LATENCYRAM.outputs.q_a(15 downto 6); -- leggo la ram

		  n.SENDRAM.inputs.data_a(151 downto 146)   := ro.tmptriggerword OR n.SENDRAM.outputs.q_a(151 downto 146);
		  n.SENDRAM.inputs.data_a(33 downto 18)     := ro.tmptriggerflag OR n.SENDRAM.outputs.q_a(33 downto 18);					
		  n.SENDRAM.inputs.data_a(17 downto 10)     := ro.tmpdatatype    OR n.SENDRAM.outputs.q_a(17 downto 10);--n.SENDRAM.outputs.q_a(17 downto 12); --data type

		  n.SENDRAM.inputs.data_a(575 downto 152)   :=  n.SENDRAM.outputs.q_a(575 downto 152);
		  n.SENDRAM.inputs.data_a(145 downto 34)    :=  n.SENDRAM.outputs.q_a(145 downto 34);
		  n.SENDRAM.inputs.data_a(9 downto 0)       :=  n.SENDRAM.outputs.q_a(9 downto 0);
		  
		  
		  n.SENDRAM.inputs.wren_a                   :='1';		
		  
	       elsif n.LATENCYRAM.outputs.q_a /= "00000000000000000" and ro.tmpdatatype(0)= '1' then
		  --if it is a physiscs trigger, I have to overwrite infos. So,
		  --if I have control OR periodics OR random in the same
		  --timestamp of physics, the winner is physics.
		  --PROBLEM: if I have 2 different physics trigger in the same
		  --timestamp, but in different time slot due to a granularity
		  --lower than 25 ns, what i have to do ??? which trigger
		  --should be sent? the first one and the second discarded?
		  r.addressfarm := ro.addressfarm; 			
		  n.SENDRAM.inputs.address_a    := n.LATENCYRAM.outputs.q_a(15 downto 6); -- leggo la ram
		  n.SENDRAM.inputs.data_a(151 downto 146)   := ro.tmptriggerword OR n.SENDRAM.outputs.q_a(151 downto 146);
		  n.SENDRAM.inputs.data_a(33 downto 18)     := ro.tmptriggerflag OR n.SENDRAM.outputs.q_a(33 downto 18);					
		  n.SENDRAM.inputs.data_a(17 downto 10)     := ro.tmpdatatype    OR n.SENDRAM.outputs.q_a(17 downto 10);--n.SENDRAM.outputs.q_a(17 downto 12); --data type
		  n.SENDRAM.inputs.data_a(7 downto 0)       := ro.tmpprimitiveID2(0)(15 downto 8)      ; --location N +1
	          n.SENDRAM.inputs.data_a(575 downto 568)   := ro.tmpprimitiveID2(0)(7 downto 0)       ; --location N +1
		  
		  n.SENDRAM.inputs.data_a(567 downto 552)     := ro.tmpprimitiveID2(1)                    ; --location N +1
		  n.SENDRAM.inputs.data_a(551 downto 536)     := ro.tmpprimitiveID2(2)                    ; --location N +1
		  n.SENDRAM.inputs.data_a(535 downto  520)    := ro.tmpprimitiveID2(3)                    ; --location N +1
		  n.SENDRAM.inputs.data_a(519 downto  504)    := ro.tmpprimitiveID2(4)                    ; --location N +1
		  n.SENDRAM.inputs.data_a(503 downto  488)    := ro.tmpprimitiveID2(5)                    ; --location N +1
		  n.SENDRAM.inputs.data_a(487 downto  472)    := ro.tmpprimitiveID2(6)                    ; --location N +1


		  n.SENDRAM.inputs.data_a(471 downto 456)     := ro.tmpprimitiveID1(0)                    ; --location N -1
		  n.SENDRAM.inputs.data_a(455 downto 440)     := ro.tmpprimitiveID1(1)                    ; --location N -1
		  n.SENDRAM.inputs.data_a(439 downto 424)     := ro.tmpprimitiveID1(2)                    ; --location N -1
		  n.SENDRAM.inputs.data_a(423 downto  408)    := ro.tmpprimitiveID1(3)                    ; --location N -1
		  n.SENDRAM.inputs.data_a(407 downto  392)    := ro.tmpprimitiveID1(4)                    ; --location N -1
		  n.SENDRAM.inputs.data_a(391 downto  376)    := ro.tmpprimitiveID1(5)                    ; --location N -1
		  n.SENDRAM.inputs.data_a(375 downto  360)    := ro.tmpprimitiveID1(6)                    ; --location N -1

		  
		  n.SENDRAM.inputs.data_a(359 downto 352)   := ro.tmpfinetime2(6)                          ;--location N +1
		  n.SENDRAM.inputs.data_a(351 downto 344)   := ro.tmpfinetime2(5)                          ;--location N +1
		  n.SENDRAM.inputs.data_a(343 downto 336)   := ro.tmpfinetime2(4)                          ;--location N +1
		  n.SENDRAM.inputs.data_a(335 downto 328)   := ro.tmpfinetime2(3)                          ;--location N +1
		  n.SENDRAM.inputs.data_a(327 downto 320)   := ro.tmpfinetime2(2)                          ;--location N +1
		  n.SENDRAM.inputs.data_a(319 downto 312)   := ro.tmpfinetime2(1)                          ;--location N +1
		  n.SENDRAM.inputs.data_a(311 downto 304)   := ro.tmpfinetime2(0)                          ;--location N +1
		  
		  
		  n.SENDRAM.inputs.data_a(303 downto 296)   := ro.tmpfinetime1(6)                          ;--location N -1
		  n.SENDRAM.inputs.data_a(295 downto 288)   := ro.tmpfinetime1(5)                          ;--location N -1
		  n.SENDRAM.inputs.data_a(287 downto 280)   := ro.tmpfinetime1(4)                          ;--location N -1
		  n.SENDRAM.inputs.data_a(279 downto 272)   := ro.tmpfinetime1(3)                          ;--location N -1
		  n.SENDRAM.inputs.data_a(271 downto 264)   := ro.tmpfinetime1(2)                          ;--location N -1
		  n.SENDRAM.inputs.data_a(263 downto 256)   := ro.tmpfinetime1(1)                          ;--location N -1
		  n.SENDRAM.inputs.data_a(255 downto 248)   := ro.tmpfinetime1(0)                          ;--location N -1
		  
		  
		  n.SENDRAM.inputs.data_a(247 downto 240)   := ro.tmpfinetime0(6)                          ; --location N
		  n.SENDRAM.inputs.data_a(239 downto 232)   := ro.tmpfinetime0(5)                          ; --location N
		  n.SENDRAM.inputs.data_a(231 downto 224)   := ro.tmpfinetime0(4)                          ; --location N
		  n.SENDRAM.inputs.data_a(223 downto 216)   := ro.tmpfinetime0(3)                          ; --location N
		  n.SENDRAM.inputs.data_a(215 downto 208)   := ro.tmpfinetime0(2)                          ; --location N
		  n.SENDRAM.inputs.data_a(207 downto 200)   := ro.tmpfinetime0(1)                          ; --location N
		  n.SENDRAM.inputs.data_a(199 downto 192)   := ro.tmpfinetime0(0)                          ; --location N
		  
		  n.SENDRAM.inputs.data_a(191 downto 160)   := ro.tmptimestamp                             ;
		  n.SENDRAM.inputs.data_a(159 downto 152)   := ro.tmpfinetime_ref                          ;
		  
		  n.SENDRAM.inputs.data_a(145 downto 130)   := ro.tmpprimitiveID0(0)                       ; --location N
		  n.SENDRAM.inputs.data_a(129 downto 114)   := ro.tmpprimitiveID0(1)                       ; --location N
		  n.SENDRAM.inputs.data_a(113 downto 98)    := ro.tmpprimitiveID0(2)                       ; --location N
		  n.SENDRAM.inputs.data_a(97 downto  82)    := ro.tmpprimitiveID0(3)                       ; --location N
		  n.SENDRAM.inputs.data_a(81 downto  66)    := ro.tmpprimitiveID0(4)                       ; --location N
		  n.SENDRAM.inputs.data_a(65 downto  50)    := ro.tmpprimitiveID0(5)                       ; --location N
		  n.SENDRAM.inputs.data_a(49 downto  34)    := ro.tmpprimitiveID0(6)                       ; --location N

		  n.SENDRAM.inputs.wren_a                   :='1'                                         ;		
		  
		  
	       else
		  r.addressfarm				  := ro.addressfarm + 1;
		  n.SENDRAM.inputs.data_a(151 downto 146) := ro.tmptriggerword;
		  n.SENDRAM.inputs.data_a(33 downto 18)	  := ro.tmptriggerflag;
		  n.SENDRAM.inputs.data_a(17 downto 10)	  := ro.tmpdatatype;  --n.SENDRAM.outputs.q_a(17 downto 12); --data type
		  n.SENDRAM.inputs.address_a		  := std_logic_vector(ro.addressfarm(9 downto 0));

		  n.SENDRAM.inputs.wren_a := '1';
	       
	       
		  n.SENDRAM.inputs.data_a(7 downto 0)     := ro.tmpprimitiveID2(0)(15 downto 8);  --location N +1
		  n.SENDRAM.inputs.data_a(575 downto 568) := ro.tmpprimitiveID2(0)(7 downto 0);  --location N +1

		  n.SENDRAM.inputs.data_a(567 downto 552) := ro.tmpprimitiveID2(1);  --location N +1
	          n.SENDRAM.inputs.data_a(551 downto 536) := ro.tmpprimitiveID2(2);  --location N +1
	          n.SENDRAM.inputs.data_a(535 downto 520) := ro.tmpprimitiveID2(3);  --location N +1
	          n.SENDRAM.inputs.data_a(519 downto 504) := ro.tmpprimitiveID2(4);  --location N +1
	          n.SENDRAM.inputs.data_a(503 downto 488) := ro.tmpprimitiveID2(5);  --location N +1
	          n.SENDRAM.inputs.data_a(487 downto 472) := ro.tmpprimitiveID2(6);  --location N +1
	          
	          
	          n.SENDRAM.inputs.data_a(471 downto 456) := ro.tmpprimitiveID1(0);  --location N -1
	          n.SENDRAM.inputs.data_a(455 downto 440) := ro.tmpprimitiveID1(1);  --location N -1
	          n.SENDRAM.inputs.data_a(439 downto 424) := ro.tmpprimitiveID1(2);  --location N -1
	          n.SENDRAM.inputs.data_a(423 downto 408) := ro.tmpprimitiveID1(3);  --location N -1
	          n.SENDRAM.inputs.data_a(407 downto 392) := ro.tmpprimitiveID1(4);  --location N -1
	          n.SENDRAM.inputs.data_a(391 downto 376) := ro.tmpprimitiveID1(5);  --location N -1
	          n.SENDRAM.inputs.data_a(375 downto 360) := ro.tmpprimitiveID1(6);  --location N -1
	          
	          
	          n.SENDRAM.inputs.data_a(359 downto 352) := ro.tmpfinetime2(6);  --location N +1
	          n.SENDRAM.inputs.data_a(351 downto 344) := ro.tmpfinetime2(5);  --location N +1
	          n.SENDRAM.inputs.data_a(343 downto 336) := ro.tmpfinetime2(4);  --location N +1
	          n.SENDRAM.inputs.data_a(335 downto 328) := ro.tmpfinetime2(3);  --location N +1
	          n.SENDRAM.inputs.data_a(327 downto 320) := ro.tmpfinetime2(2);  --location N +1
	          n.SENDRAM.inputs.data_a(319 downto 312) := ro.tmpfinetime2(1);  --location N +1
	          n.SENDRAM.inputs.data_a(311 downto 304) := ro.tmpfinetime2(0);  --location N +1
	          
	          
	          n.SENDRAM.inputs.data_a(303 downto 296) := ro.tmpfinetime1(6);  --location N -1
	          n.SENDRAM.inputs.data_a(295 downto 288) := ro.tmpfinetime1(5);  --location N -1
	          n.SENDRAM.inputs.data_a(287 downto 280) := ro.tmpfinetime1(4);  --location N -1
	          n.SENDRAM.inputs.data_a(279 downto 272) := ro.tmpfinetime1(3);  --location N -1
	          n.SENDRAM.inputs.data_a(271 downto 264) := ro.tmpfinetime1(2);  --location N -1
	          n.SENDRAM.inputs.data_a(263 downto 256) := ro.tmpfinetime1(1);  --location N -1
	          n.SENDRAM.inputs.data_a(255 downto 248) := ro.tmpfinetime1(0);  --location N -1
	          
	          
	          n.SENDRAM.inputs.data_a(247 downto 240) := ro.tmpfinetime0(6);  --location N
	          n.SENDRAM.inputs.data_a(239 downto 232) := ro.tmpfinetime0(5);  --location N
	          n.SENDRAM.inputs.data_a(231 downto 224) := ro.tmpfinetime0(4);  --location N
	          n.SENDRAM.inputs.data_a(223 downto 216) := ro.tmpfinetime0(3);  --location N
	          n.SENDRAM.inputs.data_a(215 downto 208) := ro.tmpfinetime0(2);  --location N
	          n.SENDRAM.inputs.data_a(207 downto 200) := ro.tmpfinetime0(1);  --location N
	          n.SENDRAM.inputs.data_a(199 downto 192) := ro.tmpfinetime0(0);  --location N
	          
	          n.SENDRAM.inputs.data_a(191 downto 160) := ro.tmptimestamp;
	          n.SENDRAM.inputs.data_a(159 downto 152) := ro.tmpfinetime_ref;
	          
	          n.SENDRAM.inputs.data_a(145 downto 130) := ro.tmpprimitiveID0(0);  --location N
	          n.SENDRAM.inputs.data_a(129 downto 114) := ro.tmpprimitiveID0(1);  --location N
	          n.SENDRAM.inputs.data_a(113 downto 98)  := ro.tmpprimitiveID0(2);  --location N
	          n.SENDRAM.inputs.data_a(97 downto 82)   := ro.tmpprimitiveID0(3);  --location N
	          n.SENDRAM.inputs.data_a(81 downto 66)   := ro.tmpprimitiveID0(4);  --location N
	          n.SENDRAM.inputs.data_a(65 downto 50)   := ro.tmpprimitiveID0(5);  --location N
	          n.SENDRAM.inputs.data_a(49 downto 34)   := ro.tmpprimitiveID0(6);  --location N
	    end if;

	       
	       
	       r.tmpfinetime0      := (others =>"00000000");
	       r.tmpfinetime1      := (others =>"00000000");
	       r.tmpfinetime2      := (others =>"00000000");
	       r.tmptimestamp      := (others =>'0');
	       r.tmpfinetime_ref   := (others =>'0');
	       r.tmptriggerword    := (others =>'0');
	       r.tmpprimitiveID0   := (others =>"0000000000000000");
	       r.tmpprimitiveID1   := (others =>"0000000000000000");
	       r.tmpprimitiveID2   := (others =>"0000000000000000");
	       r.tmptriggerflag    := (others =>'0'); 
	       r.tmpdatatype       := (others =>'0');

	       r.FSMoutputdata     := Idle;
	       
	 end case;	
      end procedure;	  



------------------------------------------------------------------------------------------------------------- 

      procedure SubGeneratePackets
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_clk125_t;
	    variable ro: in reglist_clk125_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_clk125_t;
	    variable n : inout netlist_t
	    ) is
      begin
	 n.SENDRAM.inputs.clock_b         :=n.clk125;      
	 n.SENDRAM.inputs.rden_b          :='0';
	 n.SENDRAM.inputs.wren_b          :='0';
	 
	 n.FIFOPACKETS.inputs.aclr        :='0';
	 n.FIFOPACKETS.inputs.wrclk       :=n.clk125;      
	 n.FIFOPACKETS.inputs.wrreq       :='0';
	 n.FIFOPACKETS.inputs.data        :=(OTHERS=>'0');
	 
	 n.FIFODETECTORFARM.inputs.rdclk  :=n.clk125;
	 n.FIFODETECTORFARM.inputs.rdreq  :='0';
	 n.FIFODETECTORFARM.inputs.aclr   :='0';
	 
	 -------------------Event header-------------------------------------------------------------------------
	 -- 1) SOB
	 
	 case ro.FSMPackets is
	    
	    when idle =>
	       
	       if ro.sob_ev125 ='1'  then      -- SOB (il segnale dura 25 ns)
		  r.sob_data_send :='1';
		  r.FSMPackets := deadtime_1;
		  
	       elsif ro.write_ev125 ='1' then --DATA			  		
		  r.data_send :='1';
		  r.FSMPackets := deadtime_1;
		  
	       elsif ro.chokeOn_ev125 ='1' then 
		  r.chokeOn_data_send :='1';
		  r.FSMPackets := deadtime_1;
		  
	       elsif ro.chokeOff_ev125 ='1' then
		  r.chokeOff_data_send :='1';
		  r.FSMPackets := deadtime_1;
		  
	       elsif ro.errorOn_ev125 ='1' then
		  r.errorOn_data_send :='1';
		  r.FSMPackets := deadtime_1;
		  
	       elsif ro.errorOff_ev125 ='1' then
		  r.errorOff_data_send :='1';
		  r.FSMPackets := deadtime_1;
		  
	       else
		  r.FSMPackets := Idle;
	       end if;
	       


	    when deadtime_1 => --+8ns
	       
	       
	       if ro.errorOff_data_send  ='1'  OR
		  ro.errorOn_data_send  ='1'  OR
		  ro.chokeOff_data_send ='1'  OR
		  ro.chokeOn_data_send  ='1'  OR
		  ro.sob_data_send      ='1'   then
		  
		  r.FSMPackets := SetFarmAddress;
		  
	       elsif n.FIFODETECTORFARM.outputs.rdempty ='0' then --DATA
		  n.FIFODETECTORFARM.inputs.rdreq :='1';
		  r.FSMPackets := SetFarmAddress;
	       else
		  r.FSMPackets := deadtime_1;
	       end if;	   



	    when SetFarmAddress=>
	       
	       if ro.errorOff_data_send   ='1'  OR
		  ro.errorOn_data_send  ='1'  OR
		  ro.chokeOff_data_send ='1'  OR
		  ro.chokeOn_data_send  ='1'  OR
		  ro.sob_data_send      ='1'   then        
		  r.FSMPackets := deadtime_2;
		  
	       else
		  
		  n.SENDRAM.inputs.address_b := n.FIFODETECTORFARM.outputs.q(9 downto 0);
		  n.SENDRAM.inputs.rden_b :='1';
		  r.FSMPackets := deadtime_2;

	       end if;

	    when deadtime_2 => --+8ns
	       
	       r.FSMPackets := WriteEvent;


	    when WriteEvent =>
	       
	       --SOB
	       
	       if ro.sob_data_send = '1' then --SOB
		  r.old_tw :=	ro.sob_data125(151 downto 146);--TW HW 
		  r.old_timestamp:=ro.sob_data125(191 downto 160);
		  r.sob_data_send :='0';

		  if n.FIFOPACKETS.outputs.wrfull='0' then 			

		     --Data_3
		     n.FIFOPACKETS.inputs.data( 703 downto  256) := (others=>'0');                                               
		     --Data_2
		     n.FIFOPACKETS.inputs.data( 255 downto  248)  := ro.sob_data125(33 downto 26)   ; --EVENT                                    
		     n.FIFOPACKETS.inputs.data( 247 downto  240)  := ro.sob_data125(25 downto 18)   ; --TW                           
		     n.FIFOPACKETS.inputs.data( 239 downto  232)  := SLV(ro.old_tw,8)               ;                                   
		     n.FIFOPACKETS.inputs.data( 231 downto  224)  := SLV(ro.sob_data125(151 downto 146),8);--TW HW                       
		     n.FIFOPACKETS.inputs.data( 223 downto  216)  := ro.old_timestamp(31 downto 24)  ;                           
		     n.FIFOPACKETS.inputs.data( 215 downto  208)  := ro.old_timestamp(23 downto 16)  ;                   
		     n.FIFOPACKETS.inputs.data( 207 downto  200)  := ro.old_timestamp(15 downto 8)   ;                      
		     n.FIFOPACKETS.inputs.data(199  downto  192)  := ro.old_timestamp(7 downto 0)    ;                       
		     
		     --Data_1
		     n.FIFOPACKETS.inputs.data(191  downto  184) := ro.sob_data125(49 downto 42);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 183 downto  176) := ro.sob_data125(41 downto 34);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 175 downto  168) := ro.sob_data125(65 downto 58);  --prim ID F 
		     n.FIFOPACKETS.inputs.data( 167 downto  160) := ro.sob_data125(57 downto 50);  --prim ID F
		     n.FIFOPACKETS.inputs.data( 159 downto  152) := ro.sob_data125(81 downto 74);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 151 downto  144) := ro.sob_data125(73 downto 66);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 143 downto   136):= ro.sob_data125(97 downto 90);  --prim ID D
		     n.FIFOPACKETS.inputs.data( 135 downto  128) := ro.sob_data125(89 downto 82);  --prim ID D
		     
		     --Data_0
		     n.FIFOPACKETS.inputs.data(127  downto  120)  := ro.sob_data125(113 downto 106);   --prim ID C
		     n.FIFOPACKETS.inputs.data( 119 downto  112)  := ro.sob_data125(105 downto 98) ;   --prim ID C
		     n.FIFOPACKETS.inputs.data( 111 downto  104)  := ro.sob_data125(129 downto 122);   --prim ID B	
		     n.FIFOPACKETS.inputs.data( 103 downto  96)   := ro.sob_data125(121 downto 114);   --prim ID B
		     n.FIFOPACKETS.inputs.data( 95 downto  88)    := ro.sob_data125(145 downto 138);   --prim ID A	
		     n.FIFOPACKETS.inputs.data( 87 downto  80)    := ro.sob_data125(137 downto 130);   --prim ID A
		     n.FIFOPACKETS.inputs.data( 79 downto   72)   := ro.sob_data125(17 downto 10)  ;   --Data type
		     n.FIFOPACKETS.inputs.data( 71 downto  64)    := ro.sob_data125(159 downto 152);   --FINETIME
		     
		     
		     -- EVENT HEADER
		     n.FIFOPACKETS.inputs.data( 63 downto  56)  := ro.sob_data125(191 downto 184);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 55 downto  48)  := ro.sob_data125(183 downto 176);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 47 downto  40)  := ro.sob_data125(175 downto 168);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 39 downto  32)  := ro.sob_data125(167 downto 160);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 31 downto  24)  := "00000000";--EVENT FLAG
		     n.FIFOPACKETS.inputs.data( 23 downto  16)  := ro.MEPeventNum(7 downto 0);
		     n.FIFOPACKETS.inputs.data( 15 downto  8)   := SLV(0,8); --Length of event
		     n.FIFOPACKETS.inputs.data( 7 downto  0)    := SLV(96,8);--Length of event	
		     n.FIFOPACKETS.inputs.wrreq := '1';
		     r.MEPeventWritten:=SLV(UINT(ro.MEPEventWritten)+1,8); --mi dice che ho scritto un nuovo evento nella fifo
		  end if;
		  
		  --CHOKE ON
		  
		  
	       elsif ro.chokeOn_data_send ='1' then
		  r.old_tw :=	ro.chokeOn_data125(151 downto 146);--TW HW 
		  r.old_timestamp:=ro.chokeOn_data125(191 downto 160);
		  r.chokeOn_data_send  :='0';
		  
		  if n.FIFOPACKETS.outputs.wrfull='0' then 			
		     
		     --Data_3
		     n.FIFOPACKETS.inputs.data( 703 downto  256) := (others=>'0');                                               
		     --Data_2
		     n.FIFOPACKETS.inputs.data( 255 downto  248)  := ro.chokeOn_data125(33 downto 26)   ; --EVENT                                    
		     n.FIFOPACKETS.inputs.data( 247 downto  240)  := ro.chokeOn_data125(25 downto 18)   ; --TW                           
		     n.FIFOPACKETS.inputs.data( 239 downto  232)  := SLV(ro.old_tw,8)               ;                                   
		     n.FIFOPACKETS.inputs.data( 231 downto  224)  := SLV(ro.chokeOn_data125(151 downto 146),8);--TW HW                       
		     n.FIFOPACKETS.inputs.data( 223 downto  216)  :=ro.old_timestamp(31 downto 24)  ;                           
		     n.FIFOPACKETS.inputs.data( 215 downto  208)  :=ro.old_timestamp(23 downto 16)  ;                   
		     n.FIFOPACKETS.inputs.data( 207 downto  200)  :=ro.old_timestamp(15 downto 8)   ;                      
		     n.FIFOPACKETS.inputs.data(199  downto  192)  :=ro.old_timestamp(7 downto 0)    ;                       
		     
		     --Data_1
		     n.FIFOPACKETS.inputs.data(191  downto  184) := ro.chokeOn_data125(49 downto 42);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 183 downto  176) := ro.chokeOn_data125(41 downto 34);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 175 downto  168) := ro.chokeOn_data125(65 downto 58);  --prim ID F 
		     n.FIFOPACKETS.inputs.data( 167 downto  160) := ro.chokeOn_data125(57 downto 50);  --prim ID F
		     n.FIFOPACKETS.inputs.data( 159 downto  152) := ro.chokeOn_data125(81 downto 74);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 151 downto  144) := ro.chokeOn_data125(73 downto 66);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 143 downto   136):= ro.chokeOn_data125(97 downto 90);  --prim ID D
		     n.FIFOPACKETS.inputs.data( 135 downto  128) := ro.chokeOn_data125(89 downto 82);  --prim ID D
		     
		     --Data_0
		     n.FIFOPACKETS.inputs.data(127  downto  120)  := ro.chokeOn_data125(113 downto 106);   --prim ID C
		     n.FIFOPACKETS.inputs.data( 119 downto  112)  := ro.chokeOn_data125(105 downto 98) ;   --prim ID C
		     n.FIFOPACKETS.inputs.data( 111 downto  104)  := ro.chokeOn_data125(129 downto 122);   --prim ID B	
		     n.FIFOPACKETS.inputs.data( 103 downto  96)   := ro.chokeOn_data125(121 downto 114);   --prim ID B
		     n.FIFOPACKETS.inputs.data( 95 downto  88)    := ro.chokeOn_data125(145 downto 138);   --prim ID A	
		     n.FIFOPACKETS.inputs.data( 87 downto  80)    := ro.chokeOn_data125(137 downto 130);   --prim ID A
		     n.FIFOPACKETS.inputs.data( 79 downto   72)   := ro.chokeOn_data125(17 downto 10)  ;   --Data type
		     n.FIFOPACKETS.inputs.data( 71 downto  64)    := ro.chokeOn_data125(159 downto 152);   --FINETIME
		     
		     
		     -- EVENT HEADER
		     n.FIFOPACKETS.inputs.data( 63 downto  56)  := ro.chokeOn_data125(191 downto 184);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 55 downto  48)  := ro.chokeOn_data125(183 downto 176);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 47 downto  40)  := ro.chokeOn_data125(175 downto 168);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 39 downto  32)  := ro.chokeOn_data125(167 downto 160);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 31 downto  24)  := "00000000";--EVENT FLAG
		     n.FIFOPACKETS.inputs.data( 23 downto  16)  := ro.MEPeventNum(7 downto 0);
		     n.FIFOPACKETS.inputs.data( 15 downto  8)   := SLV(0,8); --Length of event
		     n.FIFOPACKETS.inputs.data( 7 downto  0)    := SLV(96,8);--Length of event	
		     n.FIFOPACKETS.inputs.wrreq := '1';
		     r.MEPeventWritten:=SLV(UINT(ro.MEPEventWritten)+1,8); --mi dice che ho scritto un nuovo evento nella fifo
		  end if;
		  
		  -- CHOKE OFF
		  
	       elsif ro.chokeOff_data_send ='1' then
		  r.old_tw :=	ro.chokeOff_data125(151 downto 146);--TW HW 
		  r.old_timestamp:=ro.chokeOff_data125(191 downto 160);
		  r.chokeOff_data_send :='0';
		  

		  if n.FIFOPACKETS.outputs.wrfull='0' then 			
		     
		     --Data_3
		     n.FIFOPACKETS.inputs.data( 703 downto  256) := (others=>'0');                                               
		     --Data_2
		     n.FIFOPACKETS.inputs.data( 255 downto  248)  := ro.chokeOff_data125(33 downto 26)   ; --EVENT                                    
		     n.FIFOPACKETS.inputs.data( 247 downto  240)  := ro.chokeOff_data125(25 downto 18)   ; --TW                           
		     n.FIFOPACKETS.inputs.data( 239 downto  232)  := SLV(ro.old_tw,8)               ;                                   
		     n.FIFOPACKETS.inputs.data( 231 downto  224)  := SLV(ro.chokeOff_data125(151 downto 146),8);--TW HW                       
		     n.FIFOPACKETS.inputs.data( 223 downto  216)  :=ro.old_timestamp(31 downto 24)  ;                           
		     n.FIFOPACKETS.inputs.data( 215 downto  208)  :=ro.old_timestamp(23 downto 16)  ;                   
		     n.FIFOPACKETS.inputs.data( 207 downto  200)  :=ro.old_timestamp(15 downto 8)   ;                      
		     n.FIFOPACKETS.inputs.data(199  downto  192)  :=ro.old_timestamp(7 downto 0)    ;                       
		     
		     --Data_1
		     n.FIFOPACKETS.inputs.data(191  downto  184) := ro.chokeOff_data125(49 downto 42);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 183 downto  176) := ro.chokeOff_data125(41 downto 34);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 175 downto  168) := ro.chokeOff_data125(65 downto 58);  --prim ID F 
		     n.FIFOPACKETS.inputs.data( 167 downto  160) := ro.chokeOff_data125(57 downto 50);  --prim ID F
		     n.FIFOPACKETS.inputs.data( 159 downto  152) := ro.chokeOff_data125(81 downto 74);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 151 downto  144) := ro.chokeOff_data125(73 downto 66);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 143 downto   136):= ro.chokeOff_data125(97 downto 90);  --prim ID D
		     n.FIFOPACKETS.inputs.data( 135 downto  128) := ro.chokeOff_data125(89 downto 82);  --prim ID D
		     
		     --Data_0
		     n.FIFOPACKETS.inputs.data(127  downto  120)  := ro.chokeOff_data125(113 downto 106);   --prim ID C
		     n.FIFOPACKETS.inputs.data( 119 downto  112)  := ro.chokeOff_data125(105 downto 98) ;   --prim ID C
		     n.FIFOPACKETS.inputs.data( 111 downto  104)  := ro.chokeOff_data125(129 downto 122);   --prim ID B	
		     n.FIFOPACKETS.inputs.data( 103 downto  96)   := ro.chokeOff_data125(121 downto 114);   --prim ID B
		     n.FIFOPACKETS.inputs.data( 95 downto  88)    := ro.chokeOff_data125(145 downto 138);   --prim ID A	
		     n.FIFOPACKETS.inputs.data( 87 downto  80)    := ro.chokeOff_data125(137 downto 130);   --prim ID A
		     n.FIFOPACKETS.inputs.data( 79 downto   72)   := ro.chokeOff_data125(17 downto 10)  ;   --Data type
		     n.FIFOPACKETS.inputs.data( 71 downto  64)    := ro.chokeOff_data125(159 downto 152);   --FINETIME
		     
		     
		     -- EVENT HEADER
		     n.FIFOPACKETS.inputs.data( 63 downto  56)  := ro.chokeOff_data125(191 downto 184);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 55 downto  48)  := ro.chokeOff_data125(183 downto 176);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 47 downto  40)  := ro.chokeOff_data125(175 downto 168);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 39 downto  32)  := ro.chokeOff_data125(167 downto 160);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 31 downto  24)  := "00000000";--EVENT FLAG
		     n.FIFOPACKETS.inputs.data( 23 downto  16)  := ro.MEPeventNum(7 downto 0);
		     n.FIFOPACKETS.inputs.data( 15 downto  8)   := SLV(0,8); --Length of event
		     n.FIFOPACKETS.inputs.data( 7 downto  0)    := SLV(96,8);--Length of event	
		     n.FIFOPACKETS.inputs.wrreq := '1';
		     r.MEPeventWritten:=SLV(UINT(ro.MEPEventWritten)+1,8); --mi dice che ho scritto un nuovo evento nella fifo
		     
		  end if;
		  
		  
		  -- ERROR ON
		  
	       elsif ro.errorOn_data_send ='1' then
		  r.old_tw :=	ro.errorOn_data125(151 downto 146);--TW HW 
		  r.old_timestamp:=ro.errorOn_data125(191 downto 160);
		  r.errorOn_data_send  :='0';
		  
		  if n.FIFOPACKETS.outputs.wrfull='0' then 			
		     
		     --Data_3
		     n.FIFOPACKETS.inputs.data( 703 downto  256) := (others=>'0');                                               
		     --Data_2
		     n.FIFOPACKETS.inputs.data( 255 downto  248)  := ro.errorOn_data125(33 downto 26)   ; --EVENT                                    
		     n.FIFOPACKETS.inputs.data( 247 downto  240)  := ro.errorOn_data125(25 downto 18)   ; --TW                           
		     n.FIFOPACKETS.inputs.data( 239 downto  232)  := SLV(ro.old_tw,8)               ;                                   
		     n.FIFOPACKETS.inputs.data( 231 downto  224)  := SLV(ro.errorOn_data125(151 downto 146),8);--TW HW                       
		     n.FIFOPACKETS.inputs.data( 223 downto  216)  :=ro.old_timestamp(31 downto 24)  ;                           
		     n.FIFOPACKETS.inputs.data( 215 downto  208)  :=ro.old_timestamp(23 downto 16)  ;                   
		     n.FIFOPACKETS.inputs.data( 207 downto  200)  :=ro.old_timestamp(15 downto 8)   ;                      
		     n.FIFOPACKETS.inputs.data(199  downto  192)  :=ro.old_timestamp(7 downto 0)    ;                       
		     
		     --Data_1
		     n.FIFOPACKETS.inputs.data(191  downto  184) := ro.errorOn_data125(49 downto 42);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 183 downto  176) := ro.errorOn_data125(41 downto 34);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 175 downto  168) := ro.errorOn_data125(65 downto 58);  --prim ID F 
		     n.FIFOPACKETS.inputs.data( 167 downto  160) := ro.errorOn_data125(57 downto 50);  --prim ID F
		     n.FIFOPACKETS.inputs.data( 159 downto  152) := ro.errorOn_data125(81 downto 74);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 151 downto  144) := ro.errorOn_data125(73 downto 66);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 143 downto   136):= ro.errorOn_data125(97 downto 90);  --prim ID D
		     n.FIFOPACKETS.inputs.data( 135 downto  128) := ro.errorOn_data125(89 downto 82);  --prim ID D
		     
		     --Data_0
		     n.FIFOPACKETS.inputs.data(127  downto  120)  := ro.errorOn_data125(113 downto 106);   --prim ID C
		     n.FIFOPACKETS.inputs.data( 119 downto  112)  := ro.errorOn_data125(105 downto 98) ;   --prim ID C
		     n.FIFOPACKETS.inputs.data( 111 downto  104)  := ro.errorOn_data125(129 downto 122);   --prim ID B	
		     n.FIFOPACKETS.inputs.data( 103 downto  96)   := ro.errorOn_data125(121 downto 114);   --prim ID B
		     n.FIFOPACKETS.inputs.data( 95 downto  88)    := ro.errorOn_data125(145 downto 138);   --prim ID A	
		     n.FIFOPACKETS.inputs.data( 87 downto  80)    := ro.errorOn_data125(137 downto 130);   --prim ID A
		     n.FIFOPACKETS.inputs.data( 79 downto   72)   := ro.errorOn_data125(17 downto 10)  ;   --Data type
		     n.FIFOPACKETS.inputs.data( 71 downto  64)    := ro.errorOn_data125(159 downto 152);   --FINETIME
		     
		     
		     -- EVENT HEADER
		     n.FIFOPACKETS.inputs.data( 63 downto  56)  := ro.errorOn_data125(191 downto 184);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 55 downto  48)  := ro.errorOn_data125(183 downto 176);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 47 downto  40)  := ro.errorOn_data125(175 downto 168);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 39 downto  32)  := ro.errorOn_data125(167 downto 160);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 31 downto  24)  := "00000000";--EVENT FLAG
		     n.FIFOPACKETS.inputs.data( 23 downto  16)  := ro.MEPeventNum(7 downto 0);
		     n.FIFOPACKETS.inputs.data( 15 downto  8)   := SLV(0,8); --Length of event
		     n.FIFOPACKETS.inputs.data( 7 downto  0)    := SLV(96,8);--Length of event	
		     n.FIFOPACKETS.inputs.wrreq := '1';
		     r.MEPeventWritten:=SLV(UINT(ro.MEPEventWritten)+1,8); --mi dice che ho scritto un nuovo evento nella fifo
		  end if;
		  
		  --ERROR OFF
		  
	       elsif ro.errorOff_data_send ='1' then
		  r.old_tw :=	ro.errorOff_data125(151 downto 146);--TW HW 
		  r.old_timestamp:=ro.errorOff_data125(191 downto 160);
		  r.errorOff_data_send :='0';

		  if  n.FIFOPACKETS.outputs.wrfull='0' then 			
		     
		     --Data_3
		     n.FIFOPACKETS.inputs.data( 703 downto  256) := (others=>'0');                                               
		     --Data_2
		     n.FIFOPACKETS.inputs.data( 255 downto  248)  := ro.errorOff_data125(33 downto 26)   ; --EVENT                                    
		     n.FIFOPACKETS.inputs.data( 247 downto  240)  := ro.errorOff_data125(25 downto 18)   ; --TW                           
		     n.FIFOPACKETS.inputs.data( 239 downto  232)  := SLV(ro.old_tw,8)               ;                                   
		     n.FIFOPACKETS.inputs.data( 231 downto  224)  := SLV(ro.errorOff_data125(151 downto 146),8);--TW HW                       
		     n.FIFOPACKETS.inputs.data( 223 downto  216)  :=ro.old_timestamp(31 downto 24)  ;                           
		     n.FIFOPACKETS.inputs.data( 215 downto  208)  :=ro.old_timestamp(23 downto 16)  ;                   
		     n.FIFOPACKETS.inputs.data( 207 downto  200)  :=ro.old_timestamp(15 downto 8)   ;                      
		     n.FIFOPACKETS.inputs.data(199  downto  192)  :=ro.old_timestamp(7 downto 0)    ;                       
		     
		     --Data_1
		     n.FIFOPACKETS.inputs.data(191  downto  184) := ro.errorOff_data125(49 downto 42);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 183 downto  176) := ro.errorOff_data125(41 downto 34);  --prim ID G
		     n.FIFOPACKETS.inputs.data( 175 downto  168) := ro.errorOff_data125(65 downto 58);  --prim ID F 
		     n.FIFOPACKETS.inputs.data( 167 downto  160) := ro.errorOff_data125(57 downto 50);  --prim ID F
		     n.FIFOPACKETS.inputs.data( 159 downto  152) := ro.errorOff_data125(81 downto 74);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 151 downto  144) := ro.errorOff_data125(73 downto 66);  --prim ID E 
		     n.FIFOPACKETS.inputs.data( 143 downto   136):= ro.errorOff_data125(97 downto 90);  --prim ID D
		     n.FIFOPACKETS.inputs.data( 135 downto  128) := ro.errorOff_data125(89 downto 82);  --prim ID D
		     
		     --Data_0
		     n.FIFOPACKETS.inputs.data(127  downto  120)  := ro.errorOff_data125(113 downto 106);   --prim ID C
		     n.FIFOPACKETS.inputs.data( 119 downto  112)  := ro.errorOff_data125(105 downto 98) ;   --prim ID C
		     n.FIFOPACKETS.inputs.data( 111 downto  104)  := ro.errorOff_data125(129 downto 122);   --prim ID B	
		     n.FIFOPACKETS.inputs.data( 103 downto  96)   := ro.errorOff_data125(121 downto 114);   --prim ID B
		     n.FIFOPACKETS.inputs.data( 95 downto  88)    := ro.errorOff_data125(145 downto 138);   --prim ID A	
		     n.FIFOPACKETS.inputs.data( 87 downto  80)    := ro.errorOff_data125(137 downto 130);   --prim ID A
		     n.FIFOPACKETS.inputs.data( 79 downto   72)   := ro.errorOff_data125(17 downto 10)  ;   --Data type
		     n.FIFOPACKETS.inputs.data( 71 downto  64)    := ro.errorOff_data125(159 downto 152);   --FINETIME
		     
		     
		     -- EVENT HEADER
		     n.FIFOPACKETS.inputs.data( 63 downto  56)  := ro.errorOff_data125(191 downto 184);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 55 downto  48)  := ro.errorOff_data125(183 downto 176);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 47 downto  40)  := ro.errorOff_data125(175 downto 168);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 39 downto  32)  := ro.errorOff_data125(167 downto 160);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 31 downto  24)  := "00000000";--EVENT FLAG
		     n.FIFOPACKETS.inputs.data( 23 downto  16)  := ro.MEPeventNum(7 downto 0);
		     n.FIFOPACKETS.inputs.data( 15 downto  8)   := SLV(0,8); --Length of event
		     n.FIFOPACKETS.inputs.data( 7 downto  0)    := SLV(96,8);--Length of event	
		     n.FIFOPACKETS.inputs.wrreq := '1';
		     r.MEPeventWritten:=SLV(UINT(ro.MEPEventWritten)+1,8); --mi dice che ho scritto un nuovo evento nella fifo
		  end if;
		  
		  
		  -- 2) DATA--------------------------------------
		  
	       elsif ro.data_send = '1' then 		--DATA		
		  
		  r.old_tw :=	n.SENDRAM.outputs.q_b(151 downto 146);-- TW
		  r.old_timestamp:=n.SENDRAM.outputs.q_b(191 downto 160);      
		  r.data_send :='0';
		  
		  if  n.FIFOPACKETS.outputs.wrfull ='0' then 

		     --Data_9
		     
		     n.FIFOPACKETS.inputs.data( 703 downto  656)  := (others=>'0');
		     n.FIFOPACKETS.inputs.data( 655 downto  640)  := n.SENDRAM.outputs.q_b(487 downto  472)       ; --primitive 2 G
		     
		     --Data_8
		     
		     n.FIFOPACKETS.inputs.data( 639 downto  624)  := n.SENDRAM.outputs.q_b(503 downto  488)       ; --primitive 2 F  
		     n.FIFOPACKETS.inputs.data( 623 downto  608)  := n.SENDRAM.outputs.q_b(519 downto  504)      ; --primitive 2 E  
		     n.FIFOPACKETS.inputs.data( 607 downto  592)  := n.SENDRAM.outputs.q_b(535 downto  520)       ; --primitive 2 D  
		     n.FIFOPACKETS.inputs.data( 591 downto  576)  := n.SENDRAM.outputs.q_b(551 downto 536)       ; --primitive 2 C  


		     --Data_7
		     
		     n.FIFOPACKETS.inputs.data( 575 downto  560)  := n.SENDRAM.outputs.q_b(567 downto 552)       ; --primitive 2 B  
		     n.FIFOPACKETS.inputs.data( 559 downto  544)  :=  n.SENDRAM.outputs.q_b(7 downto 0) & n.SENDRAM.outputs.q_b(575 downto 568)      ; --primitive 2 A  
		     n.FIFOPACKETS.inputs.data( 543 downto  528)  := n.SENDRAM.outputs.q_b(375 downto  360)       ; --primitive 1 G  
		     n.FIFOPACKETS.inputs.data( 527 downto  512)  := n.SENDRAM.outputs.q_b(391 downto  376)       ; --primitive 1 F  


--Data_6
		     n.FIFOPACKETS.inputs.data( 511 downto  496)  := n.SENDRAM.outputs.q_b(407 downto  392)       ; --primitive 1 E  
		     n.FIFOPACKETS.inputs.data( 495 downto  480)  := n.SENDRAM.outputs.q_b(423 downto  408)      ; --primitive 1 D  
		     n.FIFOPACKETS.inputs.data( 479 downto  464)  := n.SENDRAM.outputs.q_b(439 downto 424)       ; --primitive 1 C  
		     n.FIFOPACKETS.inputs.data( 463 downto  448)  := n.SENDRAM.outputs.q_b(455 downto 440)       ; --primitive 1 B  
		     
		     --Data_5
		     n.FIFOPACKETS.inputs.data( 447 downto  432)  := n.SENDRAM.outputs.q_b(471 downto 456)       ; --primitive 1 A  
		     n.FIFOPACKETS.inputs.data( 431 downto  424)  := (others=>'0')                               ;-- reserved           
		     n.FIFOPACKETS.inputs.data( 423 downto  416)  := n.SENDRAM.outputs.q_b(359 downto 352)       ; --FT G 2        	                                                   
		     n.FIFOPACKETS.inputs.data( 415 downto  408)  := n.SENDRAM.outputs.q_b(351 downto 344)       ; --FT F 2   
		     n.FIFOPACKETS.inputs.data( 407 downto  400)  := n.SENDRAM.outputs.q_b(343 downto 336)       ; --FT E 2
		     n.FIFOPACKETS.inputs.data( 399 downto  392)  := n.SENDRAM.outputs.q_b(335 downto 328)       ; --FT D 2
		     n.FIFOPACKETS.inputs.data(391  downto  384)  := n.SENDRAM.outputs.q_b(327 downto 320)       ; --FT C 2
		     
		     

		     --Data_4
		     n.FIFOPACKETS.inputs.data( 383 downto  376)  := n.SENDRAM.outputs.q_b(319 downto 312)       ; --FT B 2                                                    
		     n.FIFOPACKETS.inputs.data( 375 downto  368)  := n.SENDRAM.outputs.q_b(311 downto 304)       ; --FT A 2   
		     n.FIFOPACKETS.inputs.data( 367 downto  360)  := n.SENDRAM.outputs.q_b(303 downto 296)       ; --FT G 1              
		     n.FIFOPACKETS.inputs.data( 359 downto  352)  := n.SENDRAM.outputs.q_b(295 downto 288)       ; --FT F 1        	                                                   
		     n.FIFOPACKETS.inputs.data( 351 downto  344)  := n.SENDRAM.outputs.q_b(287 downto 280)       ; --FT E 1   
		     n.FIFOPACKETS.inputs.data( 343 downto  336)  := n.SENDRAM.outputs.q_b(279 downto 272)       ; --FT D 1
		     n.FIFOPACKETS.inputs.data( 335 downto  328)  := n.SENDRAM.outputs.q_b(271 downto 264)       ; --FT C 1
		     n.FIFOPACKETS.inputs.data(327  downto  320)  := n.SENDRAM.outputs.q_b(263 downto 256)       ; --FT B 1


		     --Data_3
		     n.FIFOPACKETS.inputs.data( 319 downto  312)  := n.SENDRAM.outputs.q_b(255 downto 248)       ; --FT A 1                   
		     n.FIFOPACKETS.inputs.data( 311 downto  304)  := n.SENDRAM.outputs.q_b(247 downto 240)       ; --FT G 0   
		     n.FIFOPACKETS.inputs.data( 303 downto  296)  := n.SENDRAM.outputs.q_b(239 downto 232)       ; --FT F 0              
		     n.FIFOPACKETS.inputs.data( 295 downto  288)  := n.SENDRAM.outputs.q_b(231 downto 224)       ; --FT E 0        	                                                   
		     n.FIFOPACKETS.inputs.data( 287 downto  280)  := n.SENDRAM.outputs.q_b(223 downto 216)       ; --FT D 0   
		     n.FIFOPACKETS.inputs.data( 279 downto  272)  := n.SENDRAM.outputs.q_b(215 downto 208)       ; --FT C 0
		     n.FIFOPACKETS.inputs.data( 271 downto  264)  := n.SENDRAM.outputs.q_b(207 downto 200)       ; --FT B 0
		     n.FIFOPACKETS.inputs.data(263  downto  256)  := n.SENDRAM.outputs.q_b(199 downto 192)       ; --FT A 0
		     
		     --Data_2
		     n.FIFOPACKETS.inputs.data( 255 downto  248)  := n.SENDRAM.outputs.q_b(33 downto 26)         ; --EV TW
		     n.FIFOPACKETS.inputs.data( 247 downto  240)  := n.SENDRAM.outputs.q_b(25 downto 18)         ; --EV TW 
		     n.FIFOPACKETS.inputs.data( 239 downto  232)  := SLV(ro.old_tw,8)                           ;       
		     n.FIFOPACKETS.inputs.data( 231 downto  224)  := SLV(n.SENDRAM.outputs.q_b(151 downto 146),8);-- TW                                                      
		     n.FIFOPACKETS.inputs.data( 223 downto  216)  := ro.old_timestamp(31 downto 24)             ;       
		     n.FIFOPACKETS.inputs.data( 215 downto  208)  := ro.old_timestamp(23 downto 16)             ; 
		     n.FIFOPACKETS.inputs.data( 207 downto  200)  := ro.old_timestamp(15 downto 8)              ; 
		     n.FIFOPACKETS.inputs.data(199  downto  192)  := ro.old_timestamp(7 downto 0)               ; 
		     
		     --DATA_1
		     n.FIFOPACKETS.inputs.data(191  downto  184) := n.SENDRAM.outputs.q_b(49 downto 42);  --prim ID G 	
		     n.FIFOPACKETS.inputs.data( 183 downto  176) := n.SENDRAM.outputs.q_b(41 downto 34);  --prim ID G		
		     n.FIFOPACKETS.inputs.data( 175 downto  168) := n.SENDRAM.outputs.q_b(65 downto 58);  --prim ID F
		     n.FIFOPACKETS.inputs.data( 167 downto  160) := n.SENDRAM.outputs.q_b(57 downto 50);  --prim ID F		
		     n.FIFOPACKETS.inputs.data( 159 downto  152) := n.SENDRAM.outputs.q_b(81 downto 74);  --prim ID E      
		     n.FIFOPACKETS.inputs.data( 151 downto  144) := n.SENDRAM.outputs.q_b(73 downto 66);  --prim ID E
		     n.FIFOPACKETS.inputs.data( 143 downto   136):= n.SENDRAM.outputs.q_b(97 downto 90);  --prim ID D			
		     n.FIFOPACKETS.inputs.data( 135 downto  128) := n.SENDRAM.outputs.q_b(89 downto 82);  --prim ID D
		     
		     --Data_0
		     n.FIFOPACKETS.inputs.data(127  downto  120)   := n.SENDRAM.outputs.q_b(113 downto 106);   --prim ID C
		     n.FIFOPACKETS.inputs.data( 119 downto  112)   := n.SENDRAM.outputs.q_b(105 downto 98);    --prim ID C 
		     n.FIFOPACKETS.inputs.data( 111 downto  104)   := n.SENDRAM.outputs.q_b(129 downto 122);   --prim ID B
		     n.FIFOPACKETS.inputs.data( 103 downto  96)    := n.SENDRAM.outputs.q_b(121 downto 114);   --prim ID B
		     n.FIFOPACKETS.inputs.data( 95 downto  88)     := n.SENDRAM.outputs.q_b(145 downto 138);   --prim ID A
		     n.FIFOPACKETS.inputs.data( 87 downto  80)     := n.SENDRAM.outputs.q_b(137 downto 130);   --prim ID A
		     n.FIFOPACKETS.inputs.data( 79 downto   72)    := n.SENDRAM.outputs.q_b(17 downto 10)  ;   --Data type
		     n.FIFOPACKETS.inputs.data( 71 downto  64)     := n.SENDRAM.outputs.q_b(159 downto 152);   --FINETIME

-- EVENT HEADER

		     n.FIFOPACKETS.inputs.data( 63 downto  56)  := n.SENDRAM.outputs.q_b(191 downto 184);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 55 downto  48)  := n.SENDRAM.outputs.q_b(183 downto 176);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 47 downto  40)  := n.SENDRAM.outputs.q_b(175 downto 168);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 39 downto  32)  := n.SENDRAM.outputs.q_b(167 downto 160);   --timestamp;
		     n.FIFOPACKETS.inputs.data( 31 downto  24)  := "00000000";--EVENT FLAG
		     n.FIFOPACKETS.inputs.data( 23 downto  16)  := (others =>'0'); --reserved x me
		     n.FIFOPACKETS.inputs.data( 15 downto  8)   := SLV(0,8); --Length of event
		     n.FIFOPACKETS.inputs.data( 7 downto  0)    := SLV(96,8);--Length of event
		     n.FIFOPACKETS.inputs.wrreq := '1';
		     r.MEPeventWritten:=SLV(UINT(ro.MEPEventWritten)+1,8); --mi dice che ho scritto un nuovo evento nella fifo

		  else
		     
		     r.SENDFIFOFULL :=	ro.SENDFIFOFULL OR SLV(65536,32); 
		  end if;
	       end if; --dati o sob
	       
	       r.FSMPackets := deadtime_0;

	       
	    when deadtime_0 => --+8ns
	       r.FSMPackets := Idle;
	       if ro.MEPeventWritten = ro.primitive_In_Mep125(7 downto 0) then
		  r.MEPeventWritten:=(others =>'0');
	       end if;		
	       
	 end case;
      end procedure;



      procedure SubSendDetectors
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_clk40_t;
	    variable ro: in reglist_clk40_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_clk40_t;
	    variable n : inout netlist_t
	    ) is
      begin
	 
	 n.LATENCYRAM.inputs.clock_b :=n.clk40;
	 n.LATENCYRAM.inputs.address_b   :=std_logic_vector(ro.latencyreadaddressb(14 downto 0));
	 n.LATENCYRAM.inputs.rden_b      :='0';
	 n.LATENCYRAM.inputs.wren_b      :='0';
	 
	 n.FIFODETECTORFARM.inputs.wrreq :='0';
	 n.FIFODETECTORFARM.inputs.data  :=(others=>'0');
	 n.FIFODETECTORFARM.inputs.wrclk :=n.clk40;
	 n.FIFODETECTORFARM.inputs.aclr  :='0';
	 
	 r.LTU0            := '0';
	 r.LTU1            := '0';
	 r.LTU2            := '0';
	 r.LTU3            := '0';
	 r.LTU4            := '0';
	 r.LTU5            := '0';
	 r.LTU_TRIGGER     := '0';
	 
	 r.write_ev40      :='0';
	 r.sob_ev40        :='0';
	 r.chokeOn_ev40    :='0';
	 r.chokeOff_ev40   :='0';
	 r.errorOn_ev40    :='0';
	 r.errorOff_ev40   :='0';
	 
	 case ro.FSMSendDetectors is
	    when S0_0 =>
	       r.firstdata :='0';
	       r.latencycounter:=0;
	       r.FSMSendDetectors :=S0;
	    when S0 =>
	       if ro.BURST40='1'  then --SOB
		  r.LTU0             := '0';
		  r.LTU1             := '1';
		  r.LTU2             := '0';
		  r.LTU3             := '0';
		  r.LTU4             := '0';
		  r.LTU5             := '1';
		  r.LTU_TRIGGER      := '1';
		  r.TriggerStop      := '1';      --per non inviare di nuovo l'eob mentre resetto la ram
		  r.FSMSendDetectors := S1;  --Vado in stato di latenza
		  r.sob_ev40         := '1'; --invia i dati di sob anche alla farm
		  r.ecounter40       := SLV(UINT(ro.ecounter40)+1,16);
		  
		  r.counterLTU       := SLV(1,32);
		  r.counterCHOKE     := SLV(0,32);
		  r.counterERROR     := SLV(0,32);
		  
		  r.sentCHOKE_OFF    := '1';
		  r.sentERROR_OFF    := '1';
		  r.sentCHOKE_ON     := '0';
		  r.sentERROR_ON     := '0';
		  
	       else --if out of RUN125
		  r.oldlatencyreadaddressb  := (others =>'0');
		  r.latencyreadaddressb     := (others =>'0');
		  r.latencycounter          :=0;
		  r.FSMSendDetectors        :=S0;
	       end if;
	       
	    when S1 => --Latency 
	       
	       if ro.activate_synch ='1' then
		  r.FSMSendDetectors        := S2;
	       else
		  if ro.latencycounter < UINT(ro.Fixed_Latency) - 3 then 
		     r.latencycounter            := ro.latencycounter+1;	   	
		     r.latencyreadaddressb       := (others =>'0');
		     r.oldlatencyreadaddressb    := (others =>'0');
		     r.FSMSendDetectors          := S1;
		  else --Fine Latency
		     n.LATENCYRAM.inputs.rden_b  :='1';
		     r.latencycounter            := 0;
		     r.FSMSendDetectors          := S2;
		  end if; --if latency
	       end if; -- if synchronization
	       
	    when S2 =>		  
	       --------------------------------------------------------------
	       r.latencyreadaddressb    := ro.latencyreadaddressb+1;		-- incremented clock by clock (25 ns)
	       --------------------------------------------------------------
	       n.LATENCYRAM.inputs.rden_b    := '1';
	       n.LATENCYRAM.inputs.address_b := std_logic_vector(ro.latencyreadaddressb(14 downto 0));
	       n.LATENCYRAM.inputs.wren_b    := '1'; --Because it is a TDP ram, RDW done calling old data --PERMETTE DI RESETTARE
	       n.LATENCYRAM.inputs.data_b    := (others =>'0');
	       --------------------------------------------------------------
	       
	       if ro.BURST40 ='1' then --Se sono in RUN	
		  
		  if ro.activate_synch ='1' then
		     if ro.synch_signal = '1' then
			r.counterLTU  := SLV(UINT(ro.counterLTU)+1,32);
			r.LTU0 := '0';
			r.LTU1 := '0';
			r.LTU2 := '0';
			r.LTU3 := '0';
			r.LTU4 := '0';
			r.LTU5 := '1';
			r.LTU_TRIGGER :='1';					
		     end if; --synch signal		
		     
		  else	-- non ÃƒÆ’Ã‚Â¨ abilitato il synch	
		     
		     if ro.CHOKE_ON = '1' and ro.sentCHOKE_ON ='0' then
			---------CHOKE-------------------
			report "CHOKE SENT!";
			r.counterLTU  := SLV(UINT(ro.counterLTU)+1,32);
			r.LTU0           := '0';
			r.LTU1           := '0';
			r.LTU2           := '1';
			r.LTU3           := '0';
			r.LTU4           := '0';
			r.LTU5           := '1';
			r.LTU_TRIGGER    := '1';		
			r.sentCHOKE_ON   := '1';
			r.sentCHOKE_OFF  := '0';
			r.chokeOn_ev40   := '1';
			r.counterCHOKE   := SLV(UINT(ro.counterCHOKE) +1,32);
		   	r.write_ev40     := '0'; --scrivi sul mac_sgmii

		     elsif ro.ERROR_ON = '1' and ro.sentERROR_ON ='0' then
			---------ERROR-------------------
			report "ERROR SENT!";
			r.LTU0           := '0';
			r.LTU1           := '1';
			r.LTU2           := '1';
			r.LTU3           := '0';
			r.LTU4           := '0';
			r.LTU5           := '1';
			r.LTU_TRIGGER    := '1';		
			r.sentERROR_ON   := '1';
			r.sentERROR_OFF  := '0';
			r.errorOn_ev40   := '1';
			r.counterERROR   := SLV(UINT(ro.counterERROR) +1,32);
			r.write_ev40     := '0'; --scrivi sul mac_sgmii

		     elsif ro.CHOKE_OFF = '1' and ro.sentCHOKE_OFF ='0' then
			---------CHOKE-------------------
			report "CHOKE OFF!";
			r.counterLTU  := SLV(UINT(ro.counterLTU)+1,32);
			r.LTU0           := '1';
			r.LTU1           := '0';
			r.LTU2           := '1';
			r.LTU3           := '0';
			r.LTU4           := '0';
			r.LTU5           := '1';
			r.LTU_TRIGGER    := '1';			
			r.sentCHOKE_ON   := '0';
			r.sentCHOKE_OFF  := '1';
			r.chokeOff_ev40  := '1';
			r.write_ev40     := '0'; --scrivi sul mac_sgmii

		     elsif ro.ERROR_OFF = '1' and ro.sentERROR_OFF ='0' then
			---------ERROR-------------------
			report "ERROR OFF!";
			r.counterLTU     := SLV(UINT(ro.counterLTU)+1,32);
			r.LTU0           := '1';
			r.LTU1           := '0';
			r.LTU2           := '1';
			r.LTU3           := '0';
			r.LTU4           := '0';
			r.LTU5           := '1';
			r.LTU_TRIGGER    := '1';			
			r.sentERROR_ON   := '0';
			r.sentERROR_OFF  := '1';
			r.errorOff_ev40  := '1';
			r.write_ev40     := '0'; --scrivi sul mac_sgmii

		     elsif n.LATENCYRAM.outputs.q_b(5 downto 0) /="000000" and ro.ERROR_OFF = '1' and ro.CHOKE_OFF ='1' and ro.ERROR_ON ='0' and ro.CHOKE_ON ='0' then --SOLO PRIM	
			
			if (abs(ro.latencyreadaddressb - ro.oldlatencyreadaddressb)) > 4  then --controllo timestamp
			   
			   if n.FIFODETECTORFARM.outputs.wrfull ='0' then
			      
			      r.oldlatencyreadaddressb    := ro.latencyreadaddressb - 1;
			      
			      r.counterLTU  := SLV(UINT(ro.counterLTU)+1,32);
			      ----------PHYSICAL OR CALIB TRIGGERS-------------------
			      r.LTU0 := n.LATENCYRAM.outputs.q_b(0);
			      r.LTU1 := n.LATENCYRAM.outputs.q_b(1);
			      r.LTU2 := n.LATENCYRAM.outputs.q_b(2);
			      r.LTU3 := n.LATENCYRAM.outputs.q_b(3);
			      r.LTU4 := n.LATENCYRAM.outputs.q_b(4);
			      r.LTU5 := n.LATENCYRAM.outputs.q_b(5);
			      r.LTU_TRIGGER :='1';		
			      
					--!!!!!!!!!DA METTERE UN CONTROLLO!!!!!!!!!!
			      
			      n.FIFODETECTORFARM.inputs.data := n.LATENCYRAM.outputs.q_b(16 downto 6);
			      n.FIFODETECTORFARM.inputs.wrreq :='1';
			      
			      r.ecounter40    := SLV(UINT(ro.ecounter40)+1,16); --conta quanti dati stai scrivendo sul MAC
			      r.write_ev40    := '1'; --scrivi sul mac_sgmii
			      
			   end if; --controllo fifo piena. se e' piena non spedire ne' alla farm ne' ai detector
			   
			end if; --controllo timestamp
			
		     end if;--fine controllo latency ram	
		     
		  end if; --fine controllo synch			     	 
		  
		  r.FSMSendDetectors :=S2; --SONO ANCORA NEL RUN
		  
	       elsif	ro.BURST40 ='0' then--arriva l'EOB
		  --IF CHOKE_OFF	
		  r.counterLTU  := SLV(UINT(ro.counterLTU)+1,32);
		  r.LTU0        := '1';
		  r.LTU1        := '1';
		  r.LTU2        := '0';
		  r.LTU3        := '0';
		  r.LTU4        := '0';
		  r.LTU5        := '1';
		  r.LTU_TRIGGER := '1'; 
		  r.eob_ev40    := '1'; -- manda l'eob
		  r.FSMSendDetectors :=S3; 
	       end if;
	       
	       r.firstdata :='0';
	       
	    when S3=>--DOPO EOB
	       ------------------------------------------------------
	       r.latencyreadaddressb := ro.latencyreadaddressb+1;		 
	       --------------------------------------------------------------
	       n.LATENCYRAM.inputs.rden_b    :='0';
	       n.LATENCYRAM.inputs.address_b := std_logic_vector(ro.latencyreadaddressb(14 downto 0));
	       n.LATENCYRAM.inputs.wren_b    :='1'; --Because it is a TDP ram, RDW done calling old data --PERMETTE DI RESETTARE
	       n.LATENCYRAM.inputs.data_b    :=(others =>'0');
	       
	       n.SENDRAM.inputs.address_b := std_logic_vector(ro.latencyreadaddressb(9 downto 0));
	       n.SENDRAM.inputs.data_b    :=(others =>'0');
	       
	       --------------------------------------------------------------
	       if ro.latencycounter /= UINT(ro.Fixed_Latency)+ 40000 then --Rimani qui per il tempo della latenza per resettare la ram
		  r.latencycounter := ro.latencycounter+1;
		  r.FSMSendDetectors :=S3;
	       else
		  r.eob_ev40    := '0'; -- manda l'eob
		  r.latencyreadaddressb := (others=>'0');	
		  r.oldlatencyreadaddressb := (others=>'0');	
		  r.latencycounter :=0;
		  r.FSMSendDetectors :=S0;
		  r.ecounter40    := (others=>'0');
		  n.FIFODETECTORFARM.inputs.aclr :='1';
		  r.write_ev40 :='0';
	       end if;
	 end case;
      end procedure; 

      
      procedure Clock20MHz
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_clk40_t;
	    variable ro: in reglist_Clk40_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_Clk40_t;
	    variable n : inout netlist_t
	    ) is
      begin

	 if ro.activate_clock20MHz = '1' then
	    if  n.clk40='1' then
	       if ro.clockDiv =1 then
		  r.clockDiv:=0;
		  r.div2:=not(n.clk40);
	       else
		  r.clockDiv:=ro.clockDiv+1;
		  r.div2 := n.clk40;
	       end if;
	    end if;
	    o.LTU3 := ro.div2;
	 end if;
      end procedure;


      procedure ResetCounters
	 (
	    variable i : in inputs_t;
	    variable ri: in reglist_clk125_t;
	    variable ro: in reglist_Clk125_t;
	    variable o : inout outputs_t;
	    variable r : inout reglist_Clk125_t;
	    variable n : inout netlist_t	
	    ) is
      begin
	 case ro.FSMResetCounters is
	    when S0=>
	       if ro.BURST125 ='1' then
		  r.number_of_primitives         :=(others=>"00000000000000000000000000000000");
		  r.SENDFIFOFULL                 :=SLV(0,32);
		  r.calibration_trigger          :=SLV(0,32);
		  r.number_of_CHOKE              :=(others=>'0');
		  r.number_of_ERROR              :=(others=>'0');
		  r.old_timestamp                :=(others=>'0');
		  r.old_tw                       :=(others=>'0');
		  r.MEPeventNum                  :=(others=>'0');
		  r.MEPNum                       :=(others=>'0');
		  r.periodicrandomtriggercounter :=(others=>'0');
		  r.randomtriggercounter         :=(others=>'0');
		  r.MTPNUMREF                    :=(others=>"00000000");  
		  r.FSMDelay                     := (others=>skipdata);
		  r.fifODelay                    :=(others=>"00000000000000000000000000000000");
		  r.FSMResetCounters             :=S1;
		  r.FSMSend                      := Idle;--SPEDISCE E CREA UN PACCHETTO NUOVO
		  r.sent                         :='1';
		  r.MEPeventWritten              :=(others =>'0');
		  r.MEPeventRead                 :=(others =>'0');
		  r.eob_data_send                :='0';	
		  r.addressfarm                  :=(others=>'0');
		  r.timer                        := 0;
	       else
		  r.FSMResetCounters             :=S0;
	       end if;

	    when s1=>
	       if ro.BURST125 ='0' then
		  if ro.timer = 10000 then
		     n.FIFODETECTORFARM.inputs.aclr :='1';
		     n.FIFOPACKETS.inputs.aclr :='1';
		     n.FIFODelay(0).inputs.aclr :='1';
		     n.FIFODelay(1).inputs.aclr :='1';
		     n.FIFODelay(2).inputs.aclr :='1';
		     n.FIFODelay(3).inputs.aclr :='1';
		     n.FIFODelay(4).inputs.aclr :='1';
		     n.FIFODelay(5).inputs.aclr :='1';
		     n.FIFODelay(6).inputs.aclr :='1';
		     r.start_latency :=(others =>'0');

		     
		     r.FSMResetCounters          :=S0;
		     
		  else
		     r.sob_data_send             :='0';	
		     r.start_latency             :=(others=>'0');	    
		     r.timer := ro.timer+1;
		     r.FSMResetCounters :=S1;
		     r.sob_data125               :=(others=>'0');
		     r.sendSOB                   :='1';
		  end if;
		  
	       else
		  r.FSMResetCounters :=S1;
	       end if;
	 end case;
      end procedure;





      variable i : inputs_t;
      variable ri: reglist_t;
      variable ro: reglist_t;
      variable o : outputs_t;
      variable r : reglist_t;
      variable n : netlist_t;
   begin
      -- read only variables
      i := inputs;
      ri := allregs.din;
      ro := allregs.dout;
      -- read/write variables
      o := allouts;
      r := allregs.dout;
      n := allnets;
      -- components outputs
      n.SyncRST.outputs := allcmps.SyncRST.outputs;
      n.SENDRAM.outputs   :=allcmps.SENDRAM.outputs ; -- Fifo in which triggerwords are stored before sending them back
      n.FIFOPACKETS.outputs  :=allcmps.FIFOPACKETS.outputs; -- Fifo in which triggerwords are stored before sending them back
      n.FIFODETECTORFARM.outputs :=allcmps.FIFODETECTORFARM.outputs;
      for index in 0 to ethlink_NODES-2 loop
	 n.FIFODelay(index).outputs   :=allcmps.FIFODelay(index).outputs;	 
      end loop;
      
      n.LATENCYRAM.outputs     :=allcmps.LATENCYRAM.outputs; 
      
      --
      -- all procedures call (edit)
      --
      
      SubReset(i, ri, ro, o, r, n);
      SubMain(i, ri, ro, o, r, n);
      
      
      -- clock domain: clk50
      SubReceive(i, ri.clk125, ro.clk125, o, r.clk125, n);
      SubReadFIFODelay(i, ri.clk125, ro.clk125, o, r.clk125, n);
      SubSendBuffer(i,ri.clk125, ro.clk125, o, r.clk125, n);
      SubSendDETECTORS(i,ri.clk40, ro.clk40, o, r.clk40, n);
      SubGeneratePackets(i,ri.clk125, ro.clk125, o, r.clk125, n);
      Clock20MHz(i, ri.clk40, ro.clk40, o, r.clk40, n);
      ResetCounters(i,ri.clk125, ro.clk125, o, r.clk125, n);


      -- allouts/regs/nets updates
      
      allouts <= o;
      allregs.din <= r;
      allnets <= n;

   end process;

--**************************************************************
--**************************************************************
   outputs <= allouts;

end rtl;

